package com.ruoyi.utils;

import cn.hutool.crypto.symmetric.AES;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.properties.EnvProperties;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class AesUtil {

    private static final EnvProperties envProperties = SpringContextHolder.getBean(EnvProperties.class);

    public static String encrypt(String value) {
        log.debug("value before encrypt: {}", value);
        String valueAfterEncrypt = new AES("CBC", "PKCS7Padding", envProperties.getAppEncryptKey().getBytes(), envProperties.getAppEncryptIv().getBytes()).encryptHex(value);
        log.debug("value after encrypt: {}", valueAfterEncrypt);
        return valueAfterEncrypt;
    }

    public static String decrypt(String value) {
        log.debug("value before decrypt: {}", value);
        String valueAfterDecrypt = new AES("CBC", "PKCS7Padding", envProperties.getAppEncryptKey().getBytes(), envProperties.getAppEncryptIv().getBytes()).decryptStr(value);
        log.debug("value after decrypt: {}", valueAfterDecrypt);
        return valueAfterDecrypt;
    }

    public static void decryptParams(Map<String, Object> params) {
        for (String key : params.keySet()) {
            Object obj = params.get(key);
            if (obj != null) {
                if (obj instanceof String) {
                    String value = (String) obj;
                    if (StringUtils.isNotBlank(value)) {
                        params.put(key, AesUtil.decrypt(value));
                    }
                }
            }
        }
    }
}
