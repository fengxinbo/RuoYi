package com.ruoyi.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Data
@Component
@PropertySource(value = "classpath:/properties/env-${spring.profiles.active}.properties")
public class EnvProperties {

    @Value("${app.elavatine.encrypt.key}")
    private String appEncryptKey;

    @Value("${app.elavatine.encrypt.iv}")
    private String appEncryptIv;
}
