package com.ruoyi.web.controller.app;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.app.Sword;
import com.ruoyi.system.service.app.ISwordService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/app/sword")
public class SwordController extends BaseController {

    private String prefix = "app/sword";

    @Autowired
    private ISwordService swordService;

    @GetMapping()
    @RequiresPermissions("app:sword:view")
    public String sword() {
        return prefix + "/sword";
    }

    @ResponseBody
    @PostMapping("/list")
    @RequiresPermissions("app:sword:list")
    public TableDataInfo list(Sword sword) {
        startPage();
        List<Sword> swordList = swordService.selectSword(sword);
        return getDataTable(swordList);
    }

    @Log(title = "敏感词管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("app:sword:edit")
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(Sword sword) {
        sword.setUpdatedby(getLoginName());
        sword.setEtime(LocalDateTime.now());
        return toAjax(swordService.changeState(sword));
    }

    @Log(title = "敏感词刷新", businessType = BusinessType.UPDATE)
    @RequiresPermissions("app:sword:edit")
    @PostMapping("/refresh")
    @ResponseBody
    public AjaxResult refresh() {
        return toAjax(swordService.refresh());
    }

    @RequiresPermissions("app:sword:remove")
    @Log(title = "删除敏感词", businessType = BusinessType.DELETE)
    @PostMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") String id) {
        try {
            return toAjax(swordService.deleteSword(id));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    @RequiresPermissions("app:sword:add")
    @Log(title = "新增敏感词", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated Sword sword) {
        sword.setCreatedby(getLoginName());
        sword.setUpdatedby(getLoginName());
        return toAjax(swordService.insertSword(sword));
    }

}
