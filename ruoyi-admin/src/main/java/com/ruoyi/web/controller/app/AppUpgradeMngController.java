package com.ruoyi.web.controller.app;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.app.JsonValidator;
import com.ruoyi.system.domain.app.AppUpgradeMng;
import com.ruoyi.system.service.app.IAppUpgradeMngService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Slf4j
@Controller
@RequestMapping("/app/upgrade")
public class AppUpgradeMngController extends BaseController {

    private String prefix = "app/upgrade";

    @Autowired
    private IAppUpgradeMngService appUpgradeMngService;

    @RequiresPermissions("app:upgrade:view")
    @GetMapping()
    public String upgrade() {
        return prefix + "/info";
    }

    @RequiresPermissions("app:upgrade:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list() {
        startPage();
        return getDataTable(appUpgradeMngService.selectAppUpgradeMngList());
    }

    @RequiresPermissions("app:upgrade:edit")
    @GetMapping("/edit/{phoneType}")
    public String edit(@PathVariable("phoneType") Integer phoneType, ModelMap mmap) {
        mmap.put("appUpgradeMng", appUpgradeMngService.selectAppUpgradeMngById(phoneType));
        return prefix + "/edit";
    }

    @RequiresPermissions("app:upgrade:edit")
    @Log(title = "编辑APP版本更新信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated AppUpgradeMng appUpgradeMng) {
        if (StringUtils.isBlank(appUpgradeMng.getReleaseLog()))
            appUpgradeMng.setReleaseLog("[]");
        if (!JsonValidator.isValidJsonString(appUpgradeMng.getReleaseLog()))
            return error("编辑失败，更新日志必须为有效JSON字符串");
        appUpgradeMng.setUpdatedBy(getLoginName());
        appUpgradeMng.setEtime(LocalDateTime.now());
        return toAjax(appUpgradeMngService.updateAppUpgradeMng(appUpgradeMng));
    }
}
