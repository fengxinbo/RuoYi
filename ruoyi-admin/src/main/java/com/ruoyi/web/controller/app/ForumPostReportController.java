package com.ruoyi.web.controller.app;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.system.domain.app.*;
import com.ruoyi.system.service.app.ForumPostCommentReplyService;
import com.ruoyi.system.service.app.ForumPostCommentService;
import com.ruoyi.system.service.app.ForumPostReportService;
import com.ruoyi.system.service.app.ForumPostService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Controller
@RequestMapping("/app/forum/post/report")
public class ForumPostReportController extends BaseController {

    @Autowired
    private ForumPostService appForumPostService;

    @Autowired
    private ForumPostCommentService appForumPostCommentService;

    @Autowired
    private ForumPostCommentReplyService appForumPostCommentReplyService;

    @Autowired
    private ForumPostReportService appForumPostReportService;

    private String prefix = "app/forum/post/report";

    @GetMapping()
    @RequiresPermissions("app:forum_post_report:view")
    public String forumPostReport() {
        return prefix + "/report";
    }

    @ResponseBody
    @PostMapping("/list")
    public TableDataInfo list(ForumPostReport forumPostReport) {
        startPage();
        List<ForumPostReport> forumPostReportList = appForumPostReportService.list(forumPostReport);
        return getDataTable(forumPostReportList);
    }

    @Log(title = "标记被举报对象状态为已处理", businessType = BusinessType.UPDATE)
    @PostMapping("/ignore")
    @ResponseBody
    public AjaxResult ignore(String id) {
        return toAjax(appForumPostReportService.ignore(id));
    }

    @RequiresPermissions("app:forum_post_report:edit")
    @GetMapping("/process/{id}")
    public String process(@PathVariable("id") String id, ModelMap mmap) {
        ForumPostReport report = appForumPostReportService.selectById(id);
        if (report == null)
            throw new ServiceException("举报记录不存在");

        Integer bizType = report.getBizType();
        String bizId = report.getBizId();
        String bizTypeStr = "未知";
        String violations = "未知";
        if (bizType == 1) {
            bizTypeStr = "帖子";
            ForumPost post = appForumPostService.selectById(bizId);
            if (post == null)
                throw new ServiceException("被举报帖子不存在");
            violations = post.getTitle();
        }
        if (bizType == 2) {
            bizTypeStr = "评论";
            ForumPostComment comment = appForumPostCommentService.selectById(bizId);
            if (comment == null)
                throw new ServiceException("被举报评论不存在");
            violations = comment.getContent();
        }
        if (bizType == 3) {
            bizTypeStr = "回复";
            ForumPostCommentReply reply = appForumPostCommentReplyService.selectById(bizId);
            if (reply == null)
                throw new ServiceException("被举报回复不存在");
            violations = reply.getContent();
        }

        ForumPostReportProcess process = new ForumPostReportProcess();
        process.setId(id);
        process.setBizId(bizId);
        process.setBizType(bizType);
        process.setBizTypeStr(bizTypeStr);
        process.setContentPart1(String.format("经核实，你举报的%s“%s”存在违规，该条%s已被隐藏", bizTypeStr, violations, bizTypeStr));
        process.setContentPart2("，并对违规用户进行了警告");
        process.setContentPart3("。感谢你对Elavatine社区氛围做出的贡献。");
        mmap.put("forumPostReportProcess", process);
        return prefix + "/process";
    }

    @RequiresPermissions("app:forum_post_report:edit")
    @Log(title = "处理被举报对象", businessType = BusinessType.UPDATE)
    @PostMapping("/process")
    @ResponseBody
    public AjaxResult process(@Validated ForumPostReportProcess process) {
        return toAjax(appForumPostReportService.process(process));
    }

}