package com.ruoyi.web.controller.app;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.app.ForumTutorial;
import com.ruoyi.system.domain.app.ForumTutorialMenu1;
import com.ruoyi.system.domain.app.ForumTutorialMenu2;
import com.ruoyi.system.domain.app.Material;
import com.ruoyi.system.service.app.ForumTutorialMenu1Service;
import com.ruoyi.system.service.app.ForumTutorialMenu2Service;
import com.ruoyi.system.service.app.ForumTutorialService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Slf4j
@Controller
@RequestMapping("/app/forum/tutorial")
public class ForumTutorialController extends BaseController {

    private String prefix = "app/forum/tutorial";

    @Autowired
    private ForumTutorialMenu1Service forumTutorialMenu1Service;

    @Autowired
    private ForumTutorialMenu2Service forumTutorialMenu2Service;

    @Autowired
    private ForumTutorialService forumTutorialService;

    @GetMapping()
    @RequiresPermissions("app:tutorial:view")
    public String main() {
        return prefix + "/main";
    }

    @ResponseBody
    @PostMapping("/menu1/list")
    public TableDataInfo menu1List() {
        startPage();
        return getDataTable(forumTutorialMenu1Service.list());
    }

    @GetMapping("/menu1/add")
    public String menu1Add() {
        return prefix + "/menu1/add";
    }

    @RequiresPermissions("app:tutorial:add")
    @Log(title = "教程菜单", businessType = BusinessType.INSERT)
    @PostMapping("/menu1/add")
    @ResponseBody
    public AjaxResult menu1AddSave(@Validated ForumTutorialMenu1 menu1) {
        if (StringUtils.isNotBlank(menu1.getMaterialStr()))
            menu1.setMaterial(JSON.parseObject(menu1.getMaterialStr(), Material.class));
        menu1.setCreatedBy(getLoginName());
        menu1.setUpdatedBy(getLoginName());
        return toAjax(forumTutorialMenu1Service.add(menu1));
    }

    @RequiresPermissions("app:tutorial:edit")
    @GetMapping("/menu1/edit/{id}")
    public String menu1Edit(@PathVariable("id") String id, ModelMap mmap) {
        ForumTutorialMenu1 menu1 = forumTutorialMenu1Service.selectById(id);
        if (menu1.getMaterial() != null)
            menu1.setMaterialStr(JSON.toJSONString(menu1.getMaterial()));
        mmap.put("menu1", menu1);
        return prefix + "/menu1/edit";
    }

    @RequiresPermissions("app:tutorial:edit")
    @Log(title = "教程菜单", businessType = BusinessType.UPDATE)
    @PostMapping("/menu1/edit")
    @ResponseBody
    public AjaxResult menu1EditSave(@Validated ForumTutorialMenu1 menu1) {
        if (StringUtils.isNotBlank(menu1.getMaterialStr()))
            menu1.setMaterial(JSON.parseObject(menu1.getMaterialStr(), Material.class));
        menu1.setUpdatedBy(getLoginName());
        return toAjax(forumTutorialMenu1Service.update(menu1));
    }

    @RequiresPermissions("app:tutorial:remove")
    @Log(title = "教程菜单", businessType = BusinessType.DELETE)
    @PostMapping("/menu1/remove/{id}")
    @ResponseBody
    public AjaxResult menu1Remove(@PathVariable("id") String id) {
        return toAjax(forumTutorialMenu1Service.delete(id));
    }

    @ResponseBody
    @PostMapping("/menu2/list")
    public TableDataInfo menu2List(ForumTutorialMenu1 menu1) {
        startPage();
        return getDataTable(forumTutorialMenu2Service.list(menu1.getId()));
    }

    @GetMapping("/menu2/add/{id}")
    public String menu2Add(@PathVariable("id") String id, ModelMap mmap) {
        mmap.put("parentId", id);
        return prefix + "/menu2/add";
    }

    @RequiresPermissions("app:tutorial:add")
    @Log(title = "教程栏目", businessType = BusinessType.INSERT)
    @PostMapping("/menu2/add")
    @ResponseBody
    public AjaxResult menu2AddSave(@Validated ForumTutorialMenu2 menu2) {
        menu2.setCreatedBy(getLoginName());
        menu2.setUpdatedBy(getLoginName());
        return toAjax(forumTutorialMenu2Service.add(menu2));
    }

    @RequiresPermissions("app:tutorial:edit")
    @GetMapping("/menu2/edit/{id}")
    public String menu2Edit(@PathVariable("id") String id, ModelMap mmap) {
        mmap.put("menu2", forumTutorialMenu2Service.selectById(id));
        return prefix + "/menu2/edit";
    }

    @RequiresPermissions("app:tutorial:edit")
    @Log(title = "教程栏目", businessType = BusinessType.UPDATE)
    @PostMapping("/menu2/edit")
    @ResponseBody
    public AjaxResult menu2EditSave(@Validated ForumTutorialMenu2 menu2) {
        menu2.setUpdatedBy(getLoginName());
        return toAjax(forumTutorialMenu2Service.update(menu2));
    }

    @RequiresPermissions("app:tutorial:remove")
    @Log(title = "教程菜单", businessType = BusinessType.DELETE)
    @PostMapping("/menu2/remove")
    @ResponseBody
    public AjaxResult menu2Remove(String id) {
        return toAjax(forumTutorialMenu2Service.delete(id));
    }

    @ResponseBody
    @PostMapping("/list")
    public TableDataInfo list(ForumTutorialMenu2 menu2) {
        startPage();
        return getDataTable(forumTutorialService.list(menu2.getId()));
    }

    @GetMapping("/add/{id}")
    public String add(@PathVariable("id") String id, ModelMap mmap) {
        mmap.put("parentId", id);
        return prefix + "/add";
    }

    @RequiresPermissions("app:tutorial:add")
    @Log(title = "教程", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated ForumTutorial tutorial) {
        if (StringUtils.isNotBlank(tutorial.getMaterialStr()))
            tutorial.setMaterial(JSON.parseObject(tutorial.getMaterialStr(), Material.class));
        tutorial.setCreatedBy(getLoginName());
        tutorial.setUpdatedBy(getLoginName());
        return toAjax(forumTutorialService.add(tutorial));
    }

    @RequiresPermissions("app:tutorial:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        ForumTutorial tutorial = forumTutorialService.selectById(id);
        if (tutorial.getMaterial() != null)
            tutorial.setMaterialStr(JSON.toJSONString(tutorial.getMaterial()));
        mmap.put("tutorial", tutorial);
        return prefix + "/edit";
    }

    @RequiresPermissions("app:tutorial:edit")
    @Log(title = "教程栏目", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult edit(@Validated ForumTutorial tutorial) {
        tutorial.setMaterial(JSON.parseObject(tutorial.getMaterialStr(), Material.class));
        tutorial.setUpdatedBy(getLoginName());
        tutorial.setEtime(LocalDateTime.now());
        return toAjax(forumTutorialService.update(tutorial));
    }

    @RequiresPermissions("app:tutorial:remove")
    @Log(title = "教程", businessType = BusinessType.DELETE)
    @PostMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") String id) {
        return toAjax(forumTutorialService.delete(id));
    }

}