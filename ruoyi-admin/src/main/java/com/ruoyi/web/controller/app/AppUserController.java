package com.ruoyi.web.controller.app;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.app.AppUser;
import com.ruoyi.system.domain.app.UserOperLog;
import com.ruoyi.system.domain.app.UserSuggestion;
import com.ruoyi.system.service.app.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("/app/user")
public class AppUserController extends BaseController {

    private String prefix = "app/user";

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private IUserOperLogService userOperLogService;

    @Autowired
    private IAppUserOnlineService appUserOnlineService;

    @Autowired
    private IAppUserDeviceInfoService appUserDeviceInfoService;

    @Autowired
    private IFoodsMissedService foodsMissedService;

    @RequiresPermissions("app:user:view")
    @GetMapping()
    public String user() {
        return prefix + "/user";
    }

    @GetMapping("/suggestion")
    @RequiresPermissions("app:suggestion:view")
    public String suggestion() {
        return prefix + "/suggestion";
    }

    @RequiresPermissions("app:user:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(AppUser user) {
        startPage();
        return getDataTable(appUserService.selectAppUserList(user));
    }

    @GetMapping("/dummy/add")
    public String add() {
        return prefix + "/dummy/add";
    }

    /**
     * 新增保存用户
     */
    @RequiresPermissions("app:user:add")
    @Log(title = "APP用户管理", businessType = BusinessType.INSERT)
    @PostMapping("/dummy/add")
    @ResponseBody
    public AjaxResult addSave(@Validated AppUser user) {
        return toAjax(appUserService.insertDummyUser(user));
    }

    @RequiresPermissions("app:user:edit")
    @GetMapping("/dummy/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        mmap.put("appUser", appUserService.selectDummyUserByUid(id));
        return prefix + "/dummy/edit";
    }

    @RequiresPermissions("app:user:edit")
    @Log(title = "APP用户管理", businessType = BusinessType.UPDATE)
    @PostMapping("/dummy/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated AppUser appUser) {
        return toAjax(appUserService.updateDummyUser(appUser));
    }

    @RequiresPermissions("app:user:remove")
    @Log(title = "APP用户管理", businessType = BusinessType.DELETE)
    @PostMapping("/dummy/remove/{id}")
    @ResponseBody
    public AjaxResult deleteDummyUserByUid(@PathVariable("id") String id) {
        return toAjax(appUserService.deleteDummyUserByUid(id));
    }

    @PostMapping("/suggestion/list")
    @ResponseBody
    public TableDataInfo suggestionList(UserSuggestion userSuggestion) {
        startPage();
        return getDataTable(appUserService.selectUserSuggestion(userSuggestion));
    }

    @PostMapping("/suggestion/showPics")
    @ResponseBody
    public JSONObject showSuggestionPics(@RequestParam String id) {
        return appUserService.showSuggestionPics(id);
    }

    @Log(title = "处理用户建议", businessType = BusinessType.UPDATE)
    @PostMapping("/suggestion/process")
    @ResponseBody
    public AjaxResult process(String id) {
        return toAjax(appUserService.processSuggestion(id));
    }

    /**
     * 回复用户的建议
     */
    @RequiresPermissions("app:suggestion:reply")
    @GetMapping("/suggestion/reply/{id}")
    public String reply(@PathVariable("id") String id, ModelMap mmap) {
        mmap.put("suggestion", appUserService.selectUserSuggestionById(id));
        return prefix + "/reply";
    }

    /**
     * 回复用户的建议
     */
    @Log(title = "回复用户", businessType = BusinessType.INSERT)
    @RequiresPermissions("app:suggestion:reply")
    @PostMapping("/suggestion/reply")
    @ResponseBody
    public AjaxResult reply(@Validated UserSuggestion suggestion) {
        suggestion.setCreatedby("admin");
        return toAjax(appUserService.replySuggestion(suggestion));
    }

    /**
     * 删除admin给用户发送的消息
     */
    @RequiresPermissions("app:suggestion:remove")
    @Log(title = "删除admin的消息", businessType = BusinessType.DELETE)
    @PostMapping("/suggestion/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") String id) {
        try {
            return toAjax(appUserService.deleteMsgById(id));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    /**
     * APP用户状态修改
     */
    @Log(title = "APP用户管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("app:user:edit")
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(AppUser user) {
        return toAjax(appUserService.changeStatus(user));
    }

    @RequiresPermissions("app:user:t1switch")
    @Log(title = "T1切换", businessType = BusinessType.OTHER)
    @PostMapping("/t1switch")
    @ResponseBody
    public AjaxResult t1switch(String uid, Integer ulevel) {
        return toAjax(appUserService.t1switch(uid, ulevel));
    }

    @GetMapping("/operlog")
    @RequiresPermissions("app:operlog:view")
    public String operLog() {
        return prefix + "/operlog";
    }

    @RequiresPermissions("app:operlog:list")
    @PostMapping("/operlog/list")
    @ResponseBody
    public TableDataInfo operLogList(UserOperLog userOperLog) {
        startPage();
        return getDataTable(userOperLogService.selectUserOperLog(userOperLog));
    }

    @ResponseBody
    @PostMapping("/dnu")
    public String getIndexDNU() {
        return appUserService.getIndexDNU();
    }

    @ResponseBody
    @PostMapping("/dau")
    public String getIndexDAU() {
        return appUserService.getIndexDAU();
    }

    @ResponseBody
    @PostMapping("/ol")
    public String getIndexOL() {
        return appUserOnlineService.getIndexOL();
    }

    @ResponseBody
    @PostMapping("/gender")
    public String getGenderPie() {
        return appUserService.getGenderPie();
    }

    @ResponseBody
    @PostMapping("/age")
    public String getIndexAge() {
        return appUserService.getIndexAge();
    }

    @ResponseBody
    @PostMapping("/missed")
    public String getIndexMissedFoods() {
        return appUserService.getIndexMissedFoods();
    }

    @ResponseBody
    @PostMapping("/missed/resolve")
    public String resolveMissedFood(String fname) {
        foodsMissedService.resolveMissedFood(fname);
        return appUserService.getIndexMissedFoods();
    }

    @ResponseBody
    @PostMapping("/manufacturer")
    public String getManufacturerPie() {
        return appUserDeviceInfoService.getPhoneBrandPie();
    }

    /**
     * 导出数据
     */
    @PostMapping("/operlog/exportData")
    @ResponseBody
    public AjaxResult exportSelected(String uid) {
        List<UserOperLog> list = userOperLogService.selectUserOperLog(uid);
        ExcelUtil<UserOperLog> util = new ExcelUtil<>(UserOperLog.class);
        return util.exportExcel(list, String.format("用户%s操作记录%s", uid, LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))));
    }

    @Log(title = "APP用户管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("app:user:edit")
    @PostMapping("/changeVisibleScope")
    @ResponseBody
    public AjaxResult changeVisibleScope(AppUser user) {
        return toAjax(appUserService.changeVisibleScope(user));
    }

    @Log(title = "APP用户管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("app:user:edit")
    @PostMapping("/changeForumAuthorRating")
    @ResponseBody
    public AjaxResult changeForumAuthorRating(AppUser user) {
        return toAjax(appUserService.changeForumAuthorRating(user));
    }

}