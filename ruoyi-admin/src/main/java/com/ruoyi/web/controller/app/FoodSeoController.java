package com.ruoyi.web.controller.app;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.AppConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.app.FoodSeo;
import com.ruoyi.system.domain.app.Foods;
import com.ruoyi.system.service.app.IFoodSeoService;
import com.ruoyi.system.service.app.IFoodService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/app/foods/seo")
public class FoodSeoController extends BaseController {

    private String prefix = "app/foods/seo";

    @Autowired
    private IFoodService foodService;

    @Autowired
    private IFoodSeoService foodSeoService;

    @GetMapping()
    @RequiresPermissions("app:foodseo:view")
    public String foodseo() {
        return prefix + "/seo";
    }

    @ResponseBody
    @PostMapping("/list")
    @RequiresPermissions("app:foodseo:list")
    public TableDataInfo list(FoodSeo foodSeo) {
        startPage();
        List<FoodSeo> foodSeoList = foodSeoService.selectFoodSeo(foodSeo);
        return getDataTable(foodSeoList);
    }

    /**
     * 新增SEO
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        Foods foods = new Foods();
        foods.setVerified(AppConstants.APP_FOODS_NUTRITION_VERIFIED);
        mmap.put("foods", foodService.selectFoods(foods));
        return prefix + "/add";
    }

    /**
     * 新增保存食物SEO
     */
    @RequiresPermissions("app:foodseo:add")
    @Log(title = "新增食物SEO", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated FoodSeo foodSeo) {
        foodSeo.setCreatedby(getLoginName());
        return toAjax(foodSeoService.insertFoodSeo(foodSeo));
    }

    /**
     * 修改SEO
     */
    @RequiresPermissions("app:foodseo:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        mmap.put("foodSeo", foodSeoService.selectFoodSeoById(id));
        return prefix + "/edit";
    }

    /**
     * 编辑保存食物SEO
     */
    @RequiresPermissions("app:foodseo:edit")
    @Log(title = "编辑食物SEO", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated FoodSeo foodSeo) {
        foodSeo.setUpdatedby(getLoginName());
        foodSeo.setEtime(LocalDateTime.now());
        return toAjax(foodSeoService.updateFoodSeo(foodSeo));
    }

    /**
     * 删除食物SEO
     */
    @RequiresPermissions("app:foodseo:remove")
    @Log(title = "删除食物SEO", businessType = BusinessType.DELETE)
    @PostMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") String id) {
        try {
            return toAjax(foodSeoService.deleteFoodSeoById(id));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }
}
