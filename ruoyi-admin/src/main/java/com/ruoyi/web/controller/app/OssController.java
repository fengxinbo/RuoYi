package com.ruoyi.web.controller.app;

import com.alibaba.fastjson.JSONArray;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.system.service.app.IOssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
@RequestMapping("/app/oss")
public class OssController extends BaseController {

    @Autowired
    private IOssService ossService;

    private static final String FORUM_POST_MATERIAL = "forum/post/material/";

    private static final String AVATAR = "avatar/";

    @PostMapping("/uploadAvatar")
    @ResponseBody
    public AjaxResult uploadAvatar(MultipartFile file) {
        try {
            String url = putObjectCore(file, AVATAR);
            AjaxResult ajax = AjaxResult.success();
            ajax.put("url", url);
            return ajax;
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * OSS通用上传请求（单个）
     */
    @PostMapping("/putObject")
    @ResponseBody
    public AjaxResult putObject(MultipartFile file) {
        try {
            String url = putObjectCore(file, FORUM_POST_MATERIAL);
            AjaxResult ajax = AjaxResult.success();
            ajax.put("url", url);
            ajax.put("fileName", FileUtils.getName(url));
            return ajax;
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * OSS通用上传请求（多个）
     */
    @PostMapping("/putObjects")
    @ResponseBody
    public AjaxResult putObjects(List<MultipartFile> files) {
        try {
            JSONArray urls = new JSONArray();
            for (MultipartFile file : files) {
                String url = putObjectCore(file, FORUM_POST_MATERIAL);
                if (StringUtils.isNotBlank(url))
                    urls.add(url);
            }
            AjaxResult ajax = AjaxResult.success();
            ajax.put("urls", urls);
            return ajax;
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }

    private String putObjectCore(MultipartFile file, String path) {
        return ossService.putObject(file, path);
    }

}
