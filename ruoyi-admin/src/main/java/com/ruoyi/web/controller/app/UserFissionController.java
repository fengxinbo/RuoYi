package com.ruoyi.web.controller.app;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.domain.app.UserInvitationLog;
import com.ruoyi.system.domain.app.UserInvitationLogStatistics;
import com.ruoyi.system.service.app.IUserInvitationLogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping("/app/user/fission")
public class UserFissionController extends BaseController {

    private String prefix = "app/user/fission";

    @Autowired
    private IUserInvitationLogService userInvitationLogService;

    @RequiresPermissions("app:user:fission")
    @GetMapping("")
    public String fission() {
        return prefix + "/fission";
    }

    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo fissionList(UserInvitationLog userInvitationLog) {
        startPage();
        return getDataTable(userInvitationLogService.selectUserInvitationLog(userInvitationLog));
    }

    @RequiresPermissions("app:fission:statistics")
    @GetMapping("/statistics")
    public String statistics() {
        return prefix + "/statistics";
    }

    @PostMapping("/statistics")
    @ResponseBody
    public TableDataInfo statistics(UserInvitationLogStatistics userInvitationLogStatistics) {
        startPage();
        return getDataTable(userInvitationLogService.selectStatistics(userInvitationLogStatistics));
    }

}
