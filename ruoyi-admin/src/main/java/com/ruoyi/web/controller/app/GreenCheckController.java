package com.ruoyi.web.controller.app;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.app.GreenCheck;
import com.ruoyi.system.service.app.IGreenCheckService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/app/green_check")
public class GreenCheckController extends BaseController {

    private String prefix = "app/green_check";

    @Resource
    private IGreenCheckService greenCheckService;

    @GetMapping()
    @RequiresPermissions("app:green_check:view")
    public String greenCheck() {
        return prefix + "/result_list";
    }

    @ResponseBody
    @PostMapping("/list")
    @RequiresPermissions("app:green_check:list")
    public TableDataInfo list(GreenCheck greenCheck) {
        startPage();
        return getDataTable(greenCheckService.selectGreenCheckList(greenCheck));
    }

    @Log(title = "忽略CS告警", businessType = BusinessType.UPDATE)
    @PostMapping("/ignore")
    @ResponseBody
    @RequiresPermissions("app:green_check:edit")
    public AjaxResult ignoreRisk(String id) {
        return toAjax(greenCheckService.ignoreRisk(id));
    }

    @Log(title = "处理CS告警", businessType = BusinessType.UPDATE)
    @PostMapping("/process")
    @ResponseBody
    @RequiresPermissions("app:green_check:edit")
    public AjaxResult process(String id, String ossUrl) {
        return toAjax(greenCheckService.disposalRisk(id, ossUrl));
    }
}
