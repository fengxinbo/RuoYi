package com.ruoyi.web.controller.app;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.app.ForumPostCommentReply;
import com.ruoyi.system.service.app.ForumPostCommentReplyService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@Slf4j
@Controller
@RequestMapping("/app/forum/post/comment/reply")
public class ForumPostCommentReplyController extends BaseController {

    @Autowired
    private ForumPostCommentReplyService appForumPostCommentReplyService;

    @ResponseBody
    @PostMapping("/list")
    public TableDataInfo list(ForumPostCommentReply reply) {
        return getDataTable(appForumPostCommentReplyService.list(new HashMap<String, Object>() {
            {
                put("id", reply.getId());
            }
        }));
    }

    @Log(title = "帖子管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("app:forum_post:edit")
    @PostMapping("/onEditableSave")
    @ResponseBody
    public AjaxResult onEditableSave(ForumPostCommentReply reply) {
        return toAjax(appForumPostCommentReplyService.onEditableSave(reply));
    }

}