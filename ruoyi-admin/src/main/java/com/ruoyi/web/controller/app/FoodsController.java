package com.ruoyi.web.controller.app;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.app.JsonValidator;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.app.Foods;
import com.ruoyi.system.service.app.IFoodService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/app/foods")
public class FoodsController extends BaseController {

    private String prefix = "app/foods";

    @Resource
    private IFoodService foodService;

    @GetMapping()
    @RequiresPermissions("app:foods:view")
    public String foods() {
        return prefix + "/foods";
    }

    @ResponseBody
    @PostMapping("/list")
    @RequiresPermissions("app:foods:list")
    public TableDataInfo list(Foods foods) {
        startPage();
        List<Foods> foodsList = foodService.selectFoods(foods);
        foodsList.forEach(f -> {
            String spec = f.getSpec();
            if (StringUtils.isNotBlank(spec)) {
                JSONArray jsonArray = JSONArray.parseArray(spec);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                    sb.append(jsonObject.getString("specNum")).append(jsonObject.getString("specName"));
                    if (i != jsonArray.size() - 1) {
                        sb.append(",");
                    }
                }
                f.setSpec(sb.toString());
            }
        });
        return getDataTable(foodsList);
    }

    @ResponseBody
    @PostMapping("/checkFoodsNameUnique")
    public boolean checkFoodsNameUnique(Foods foods) {
        return foodService.checkFoodsNameUnique(foods);
    }

    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存食物
     */
    @RequiresPermissions("app:foods:add")
    @Log(title = "食物管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated Foods foods) {
        if (!foodService.checkFoodsNameUnique(foods)) {
            return error("新增食物'" + foods.getFname() + "'失败，食物名称已存在");
        }
        if (StringUtils.isBlank(foods.getSpec()))
            foods.setSpec(new JSONArray() {
                {
                    add(new JSONObject() {
                        {
                            put("specNum", "100");
                            put("specName", "g");
                        }
                    });
                }
            }.toJSONString());
        else if (!JsonValidator.isValidJsonString(foods.getSpec()))
            return error("新增食物'" + foods.getFname() + "'失败，规格必须为有效JSON字符串");
        foods.setCreatedby(getLoginName());
        return toAjax(foodService.insertOrUpdateFoods(foods));
    }

    /**
     * 删除食物
     */
    @RequiresPermissions("app:foods:remove")
    @Log(title = "食物管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove/{fid}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("fid") String fid) {
        try {
            return toAjax(foodService.deleteFoodsByIds(fid));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    /**
     * 修改食物
     */
    @RequiresPermissions("app:foods:edit")
    @GetMapping("/edit/{fid}")
    public String edit(@PathVariable("fid") Integer fid, ModelMap mmap) {
        mmap.put("foods", foodService.selectFoodsById(fid));
        return prefix + "/edit";
    }

    /**
     * 修改保存食物
     */
    @RequiresPermissions("app:foods:edit")
    @Log(title = "食物管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated Foods foods) {
        if (StringUtils.isBlank(foods.getSpec()))
            foods.setSpec(new JSONArray() {
                {
                    add(new JSONObject() {
                        {
                            put("specNum", "100");
                            put("specName", "g");
                        }
                    });
                }
            }.toJSONString());
        else if (!JsonValidator.isValidJsonString(foods.getSpec()))
            return error("新增食物'" + foods.getFname() + "'失败，规格必须为有效JSON字符串");
        foods.setUpdatedby(getLoginName());
        return toAjax(foodService.insertOrUpdateFoods(foods));
    }

    @RequiresPermissions("app:foods:view")
    @GetMapping("/importTemplate")
    @ResponseBody
    public AjaxResult importTemplate() {
        ExcelUtil<Foods> util = new ExcelUtil<>(Foods.class);
        return util.importTemplateExcel("食物数据");
    }

    @Log(title = "食物管理", businessType = BusinessType.IMPORT)
    @RequiresPermissions("app:foods:import")
    @PostMapping("/importData")
    @ResponseBody
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<Foods> util = new ExcelUtil<>(Foods.class);
        List<Foods> foodsList = util.importExcel(file.getInputStream());
        String message = foodService.importFoods(foodsList, updateSupport, getLoginName());
        return AjaxResult.success(message);
    }

    @RequiresPermissions("app:foods:promote")
    @Log(title = "食物提升", businessType = BusinessType.OTHER)
    @PostMapping("/promote")
    @ResponseBody
    public AjaxResult promote(Integer fid) {
        return toAjax(foodService.promoteFoods(foodService.selectFoodsById(fid), getLoginName()));
    }

}
