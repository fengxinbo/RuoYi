package com.ruoyi.web.controller.app;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.domain.app.WechatPayTransferLog;
import com.ruoyi.system.service.app.IWechatPayTransferLogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/app/wechatpaytransferlog")
public class WechatPayTransferLogController extends BaseController {

    private String prefix = "app/wechatpaytransferlog";

    @Autowired
    private IWechatPayTransferLogService wechatPayTransferLogService;

    @GetMapping()
    @RequiresPermissions("app:wechatpaytransferlog:view")
    public String wechatPayTransferLog() {
        return prefix + "/transferlog";
    }

    @ResponseBody
    @PostMapping("/list")
    @RequiresPermissions("app:wechatpaytransferlog:list")
    public TableDataInfo list(WechatPayTransferLog wechatPayTransferLog) {
        startPage();
        List<WechatPayTransferLog> transferLogs = wechatPayTransferLogService.selectTransferLog(wechatPayTransferLog);
        return getDataTable(transferLogs);
    }

}
