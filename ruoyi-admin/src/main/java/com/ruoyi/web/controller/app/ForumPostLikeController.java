package com.ruoyi.web.controller.app;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.system.domain.app.*;
import com.ruoyi.system.service.app.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Controller
@RequestMapping("/app/forum/post/like")
public class ForumPostLikeController extends BaseController {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private ForumPostService forumPostService;

    @Autowired
    private ForumPostLikeService forumPostLikeService;

    @Autowired
    private ForumPostCommentService forumPostCommentService;

    @Autowired
    private ForumPostCommentReplyService forumPostCommentReplyService;

    @GetMapping("/open/{id}")
    public String open(@PathVariable("id") String id, ModelMap mmap) {
        Integer bizType = null;
        ForumPost post = forumPostService.selectById(id);
        if (post != null)
            bizType = 1;
        ForumPostComment comment = forumPostCommentService.selectById(id);
        if (comment != null)
            bizType = 2;
        ForumPostCommentReply reply = forumPostCommentReplyService.selectById(id);
        if (reply != null)
            bizType = 3;
        if (bizType == null)
            throw new ServiceException("bizType参数错误");

        AppUser dummy2 = new AppUser();
        dummy2.setDummy(2);
        List<AppUser> dummy2List = appUserService.selectAppUserList(dummy2);
        mmap.put("bizId", id);
        mmap.put("bizType", bizType);
        mmap.put("dummy2List", dummy2List);
        return "app/forum/post/like";
    }

    @PostMapping("/add")
    @ResponseBody
    public AjaxResult add(ForumPostLike like) {
        return toAjax(forumPostLikeService.add(like));
    }

}