package com.ruoyi.web.controller.app;

import com.alibaba.fastjson.JSONArray;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.system.domain.SysConfig;
import com.ruoyi.system.domain.app.AppUser;
import com.ruoyi.system.domain.app.ForumPost;
import com.ruoyi.system.domain.app.ForumPostComment;
import com.ruoyi.system.domain.app.ForumPostCommentReply;
import com.ruoyi.system.mapper.SysConfigMapper;
import com.ruoyi.system.service.app.AppUserService;
import com.ruoyi.system.service.app.ForumPostCommentReplyService;
import com.ruoyi.system.service.app.ForumPostCommentService;
import com.ruoyi.system.service.app.ForumPostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Controller
@RequestMapping("/app/forum/post/reply")
public class ForumPostReplyController extends BaseController {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private ForumPostService forumPostService;

    @Autowired
    private ForumPostCommentService forumPostCommentService;

    @Autowired
    private ForumPostCommentReplyService forumPostCommentReplyService;

    @Autowired
    private SysConfigMapper sysConfigMapper;

    @GetMapping("/open/{id}")
    public String open(@PathVariable("id") String id, ModelMap mmap) {
        SysConfig sysConfig122 = sysConfigMapper.selectConfigById(122L);
        List<String> locationList = JSONArray.parseArray(sysConfig122.getConfigValue(), String.class);

        AppUser dummy1 = new AppUser();
        dummy1.setDummy(1);
        List<AppUser> dummy1List = appUserService.selectAppUserList(dummy1);
        AppUser dummy2 = new AppUser();
        dummy2.setDummy(2);
        List<AppUser> dummy2List = appUserService.selectAppUserList(dummy2);
        mmap.put("id", id);
        mmap.put("dummy1List", dummy1List);
        mmap.put("dummy2List", dummy2List);
        mmap.put("location", locationList);
        return "app/forum/post/reply";
    }

    @PostMapping("/add")
    @ResponseBody
    public AjaxResult add(ForumPostCommentReply parameter) {
        String id = parameter.getId();
        ForumPost post = forumPostService.selectById(id);
        if (post != null) {
            // 对帖子的评论
            return toAjax(forumPostService.replyPost(post, parameter.getFromUid(), parameter.getLocation(), parameter.getContent(), parameter.getImg()));
        }
        ForumPostComment comment = forumPostCommentService.selectById(id);
        if (comment != null) {
            // 对评论的回复
            return toAjax(forumPostCommentService.replyComment(comment, parameter.getFromUid(), parameter.getLocation(), parameter.getContent(), parameter.getImg()));
        }
        ForumPostCommentReply reply = forumPostCommentReplyService.selectById(id);
        if (reply != null) {
            // 对回复的回复
            return toAjax(forumPostCommentService.replyReply(reply, parameter.getFromUid(), parameter.getLocation(), parameter.getContent(), parameter.getImg()));
        }
        throw new ServiceException("参数错误");
    }

}