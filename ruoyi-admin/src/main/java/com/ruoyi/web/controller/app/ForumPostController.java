package com.ruoyi.web.controller.app;

import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.servlet.ServletUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jthinking.common.util.ip.IPInfo;
import com.jthinking.common.util.ip.IPInfoUtils;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.AppConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysConfig;
import com.ruoyi.system.domain.app.AppUserExt;
import com.ruoyi.system.domain.app.ForumPost;
import com.ruoyi.system.domain.app.ForumPostCoverInfo;
import com.ruoyi.system.mapper.SysConfigMapper;
import com.ruoyi.system.service.app.AppUserService;
import com.ruoyi.system.service.app.ForumPostService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("/app/forum/post")
public class ForumPostController extends BaseController {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private ForumPostService appForumPostService;

    @Autowired
    private SysConfigMapper sysConfigMapper;

    private String prefix = "app/forum/post";

    @GetMapping()
    @RequiresPermissions("app:forum_post:view")
    public String forumPost() {
        return prefix + "/post";
    }

    @ResponseBody
    @PostMapping("/list")
    public TableDataInfo list(ForumPost forumPost) {
        startPage();
        List<ForumPost> forumPostList = appForumPostService.query(forumPost);
        return getDataTable(forumPostList);
    }

    @GetMapping("/add")
    public String add(ModelMap mmap) {
        mmap.put("official", appUserService.selectOfficialUserList());
        return prefix + "/add";
    }

    @RequiresPermissions("app:forum_post:add")
    @Log(title = "论坛发帖", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(HttpServletRequest request, @Validated ForumPost forumPost) {
        if (StringUtils.isNotBlank(forumPost.getCoverInfoStr()))
            forumPost.setCoverInfo(JSON.parseObject(forumPost.getCoverInfoStr(), ForumPostCoverInfo.class));
        if (forumPost.getCoverInfo().getWidth() == null || forumPost.getCoverInfo().getHeight() == null)
            throw new ServiceException("封面尺寸信息错误");
        final TimeInterval timer = new TimeInterval();
        timer.start("1");
        String ip = ServletUtil.getClientIP(request);
        log.debug("ip: {}", ip);
        log.debug("Timer 1 took {} ms", timer.intervalMs("1"));
        if (StrUtil.isNotBlank(ip)) {
            forumPost.setIp(ip);
            timer.start("2");
            IPInfo ipInfo = IPInfoUtils.getIpInfo(ip);
            log.debug("Timer 2 took {} ms", timer.intervalMs("2"));
            if (ipInfo != null) {
                timer.start("3");
                String province = ipInfo.getProvince();
                log.debug("Timer 3 took {} ms", timer.intervalMs("3"));
                if (StrUtil.isNotBlank(province)) {
                    forumPost.setLocation(province);
                }
            }
        }
        forumPost.setMachineAudit(AppConstants.APP_FORUM_POST_CS_MACHINE_AUDIT_PASS);
        forumPost.setAudit(AppConstants.APP_FORUM_POST_CS_AUDIT_PASS);
        String uid = forumPost.getUid();
        String author = getLoginName();
        if (StrUtil.isNotBlank(uid)) {
            author = appUserService.selectUserByUid(uid).getNickname();
            AppUserExt userExt = appUserService.selectUserExtByUid(uid);
            SysConfig sysConfig123 = sysConfigMapper.selectConfigById(123L);
            if (userExt != null && sysConfig123 != null) {
                Integer impressionConstant;
                String configValue = sysConfig123.getConfigValue();
                if (StrUtil.isNotBlank(configValue)) {
                    impressionConstant = Integer.parseInt(configValue);
                    if (impressionConstant != null && impressionConstant > 0) {
                        forumPost.setImpressionThreshold(new BigDecimal(userExt.getForumAuthorRating())
                                .multiply(new BigDecimal(impressionConstant))
                                .setScale(0, userExt.getForumAuthorRating() >= 1.0 ? RoundingMode.CEILING : RoundingMode.FLOOR)
                                .intValue());
                    }
                }
            }
        }
        forumPost.setCreatedBy(author);
        forumPost.setUpdatedBy(author);
        return toAjax(appForumPostService.create(forumPost));
    }

    @RequiresPermissions("app:forum_post:remove")
    @Log(title = "帖子管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") String id) {
        try {
            return toAjax(appForumPostService.delete(id));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    @RequiresPermissions("app:forum_post:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        ForumPost post = appForumPostService.selectById(id);
        mmap.put("forumPost", post);
        return prefix + "/edit";
    }

    @RequiresPermissions("app:forum_post:edit")
    @Log(title = "帖子管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated ForumPost forumPost) {
        forumPost.setUpdatedBy(getLoginName());
        return toAjax(appForumPostService.update(forumPost));
    }

    @PostMapping("/showPostCover")
    @ResponseBody
    public JSONObject showPostCover(@RequestParam String id) {
        return appForumPostService.showPostCover(id);
    }

    @GetMapping("/h")
    @ResponseBody
    public AjaxResult h() {
        return toAjax(1);
    }

    @GetMapping("/dummy/open/{id}")
    public String dummyOpen(@PathVariable("id") String id, ModelMap mmap) {
        mmap.put("uid", id);
        return prefix + "/dummy_add";
    }

    @RequiresPermissions("app:forum_post:add")
    @Log(title = "模拟前端发帖", businessType = BusinessType.INSERT)
    @PostMapping("/dummy/add")
    @ResponseBody
    public AjaxResult dummyAddSave(HttpServletRequest request, @Validated ForumPost forumPost) {
        return toAjax(appForumPostService.createDummy(forumPost));
    }

}