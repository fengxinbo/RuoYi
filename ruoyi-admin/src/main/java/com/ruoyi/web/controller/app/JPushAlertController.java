package com.ruoyi.web.controller.app;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.app.JPushAlert;
import com.ruoyi.system.service.app.IJPushAlertService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Controller
@RequestMapping("/app/jpushalert")
public class JPushAlertController extends BaseController {

    private String prefix = "app/jpushalert";

    @Autowired
    private IJPushAlertService jPushAlertService;

    @GetMapping()
    @RequiresPermissions("app:jpushalert:view")
    public String index() {
        return prefix + "/alert";
    }

    @ResponseBody
    @PostMapping("/list")
    @RequiresPermissions("app:jpushalert:list")
    public TableDataInfo list(JPushAlert jPushAlert) {
        startPage();
        return getDataTable(jPushAlertService.selectJPushAlert(jPushAlert));
    }

    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存
     */
    @RequiresPermissions("app:jpushalert:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated JPushAlert jPushAlert) {
        jPushAlert.setCreatedBy(getLoginName());
        return toAjax(jPushAlertService.insertJPushAlert(jPushAlert));
    }

    /**
     * 修改
     */
    @RequiresPermissions("app:jpushalert:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap mmap) {
        mmap.put("jPushAlert", jPushAlertService.selectJPushAlertById(id));
        return prefix + "/edit";
    }

    /**
     * 编辑保存
     */
    @RequiresPermissions("app:jpushalert:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated JPushAlert jPushAlert) {
        jPushAlert.setUpdatedBy(getLoginName());
        jPushAlert.setEtime(LocalDateTime.now());
        return toAjax(jPushAlertService.updateJPushAlert(jPushAlert));
    }

    /**
     * 删除
     */
    @RequiresPermissions("app:jpushalert:remove")
    @Log(title = "删除推送内容", businessType = BusinessType.DELETE)
    @PostMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") Integer id) {
        try {
            return toAjax(jPushAlertService.deleteJPushAlert(id));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

}
