package com.ruoyi.web.controller.app;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.app.ForumPost;
import com.ruoyi.system.domain.app.ForumPostComment;
import com.ruoyi.system.domain.app.ForumPostCommentReply;
import com.ruoyi.system.service.app.ForumPostCommentReplyService;
import com.ruoyi.system.service.app.ForumPostCommentService;
import com.ruoyi.system.service.app.ForumPostService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@Slf4j
@Controller
@RequestMapping("/app/forum/post/comment")
public class ForumPostCommentController extends BaseController {

    @Autowired
    private ForumPostService forumPostService;

    @Autowired
    private ForumPostCommentService forumPostCommentService;

    @Autowired
    private ForumPostCommentReplyService forumPostCommentReplyService;

    private String prefix = "app/forum/post/comment";

    @RequiresPermissions("app:forum_post_comment:view")
    @GetMapping()
    public String forumPostComment(ModelMap mmap, String id) {
        ForumPost post = forumPostService.selectById(id);
        mmap.put("post", post);
        return prefix + "/comment";
    }

    @ResponseBody
    @PostMapping("/list")
    public TableDataInfo list(String id) {
        startPage();
        return getDataTable(forumPostCommentService.list(new HashMap<String, Object>() {
            {
                put("id", id);
            }
        }));
    }

    @Log(title = "帖子管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("app:forum_post:edit")
    @PostMapping("/onEditableSave")
    @ResponseBody
    public AjaxResult onEditableSave(ForumPostComment comment) {
        return toAjax(forumPostCommentService.onEditableSave(comment));
    }

    @RequiresPermissions("app:forum_post:remove")
    @Log(title = "帖子管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") String id) {
        try {
            ForumPostComment comment = forumPostCommentService.selectById(id);
            if (comment != null)
                return toAjax(forumPostService.deleteComment(id));
            ForumPostCommentReply reply = forumPostCommentReplyService.selectById(id);
            if (reply != null)
                return toAjax(forumPostService.deleteReplyOnAccountDeletion(id));
        } catch (Exception e) {
            return error(e.getMessage());
        }
        return error();
    }

}