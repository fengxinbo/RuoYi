package com.ruoyi.web.controller.app;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.app.AccountDeletionLog;
import com.ruoyi.system.service.app.IAccountDeletionLogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/app/user/accountdeletion")
public class AccountDeletionLogController extends BaseController {

    private String prefix = "app/user";

    @Resource
    private IAccountDeletionLogService accountDeletionLogService;

    @GetMapping()
    @RequiresPermissions("app:accountdeletion:view")
    public String accountDeletionLog() {
        return prefix + "/accountdeletion";
    }

    @ResponseBody
    @PostMapping("/list")
    @RequiresPermissions("app:accountdeletion:list")
    public TableDataInfo list(AccountDeletionLog accountDeletionLog) {
        startPage();
        return getDataTable(accountDeletionLogService.selectAccountDeletionLogList(accountDeletionLog));
    }

    @Log(title = "处理用户账号注销申请", businessType = BusinessType.UPDATE)
    @PostMapping("/process")
    @ResponseBody
    public AjaxResult process(String id) {
        return toAjax(accountDeletionLogService.purgeData(id));
    }
}
