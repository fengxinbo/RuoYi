package com.ruoyi.quartz.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@TableName(value = "t_forum_post", autoResultMap = true)
public class ForumPostQ {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private Integer poster;

    private String uid;

    @TableField(exist = false)
    private String nickname;

    private Integer top;

    private Integer sn;

    @TableField(value = "tag", typeHandler = FastjsonTypeHandler.class)
    private List<String> tag;

    private Integer type;

    private String title;

    private String subtitle;

    @TableField(value = "cover", typeHandler = FastjsonTypeHandler.class)
    private List<String> cover;

    private String content;

    @TableField("content_type")
    private Integer contentType;

    private String material;

    @TableField("visible_scope")
    private Integer visibleScope;

    private Integer commentable;

    @TableField("machine_audit")
    private Integer machineAudit;

    @TableField("manual_audit")
    private Integer manualAudit;

    private Integer audit;

    private String weblink;

    @TableField(value = "poll_json", typeHandler = FastjsonTypeHandler.class)
    private PollJsonQ pollJson;

    @TableField(exist = false)
    private ForumPostPollDataQ forumPostPollDataQ;

    private Integer phase;

    @TableField("created_by")
    private String createdBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @TableField("updated_by")
    private String updatedBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

    @TableField(exist = false)
    private Integer commentCount;

}