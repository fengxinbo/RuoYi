package com.ruoyi.quartz.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_diet_plan_meals_archive")
public class DietPlanMealsArchiveQ {

    @TableId(type = IdType.ASSIGN_UUID)
    private String mid;

    private Integer sn;

    private String pdid;

    private Integer fid;

    private String fname;

    private String remark;

    private Double qty;

    private String spec;

    private Double calories;

    private Double protein;

    private Double carbohydrate;

    private Double fat;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

}
