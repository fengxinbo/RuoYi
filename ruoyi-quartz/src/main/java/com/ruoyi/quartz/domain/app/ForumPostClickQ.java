package com.ruoyi.quartz.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_forum_post_click")
public class ForumPostClickQ extends Model<ForumPostClickQ> {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    @TableField(value = "post_id")
    private String postId;

    private String uid;

    @TableField(value = "biz_type")
    private Integer bizType;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

}