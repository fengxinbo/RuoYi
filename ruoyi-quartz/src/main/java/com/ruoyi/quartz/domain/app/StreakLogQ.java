package com.ruoyi.quartz.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
@TableName("t_streak_log")
public class StreakLogQ {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String uid;

    private Integer checked;

    private Integer reset;

    private Integer streak;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate ctime;
}
