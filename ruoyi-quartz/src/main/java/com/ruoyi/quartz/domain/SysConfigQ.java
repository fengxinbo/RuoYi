package com.ruoyi.quartz.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

@Data
@TableName(value = "sys_config")
public class SysConfigQ extends Model<SysConfigQ> {

    @TableId("config_id")
    private Integer configId;

    @TableField("config_key")
    private String configKey;

    @TableField("config_value")
    private String configValue;

}