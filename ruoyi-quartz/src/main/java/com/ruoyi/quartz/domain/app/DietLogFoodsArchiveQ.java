package com.ruoyi.quartz.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_diet_log_foods_archive")
public class DietLogFoodsArchiveQ {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String pid;

    private Integer sn;

    private Integer fid;

    private String fname;

    private String remark;

    private Double qty;

    private String spec;

    private Double protein;

    private Double carbohydrate;

    private Double fat;

    private Double calories;

    private Integer state;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

}
