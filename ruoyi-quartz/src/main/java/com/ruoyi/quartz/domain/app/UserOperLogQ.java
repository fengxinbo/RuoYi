package com.ruoyi.quartz.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_user_oper_log")
public class UserOperLogQ {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String uid;

    private String clientip;

    private String operpath;

    private String operaction;

    private Integer affectedrows;

    private Long costtime;

    private String inputparams;

    private String outputparams;

    private String remark;

    private LocalDateTime ctime;
}
