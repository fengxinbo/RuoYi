package com.ruoyi.quartz.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_users_ext")
public class AppUserExtQ {

    @TableId(type = IdType.ASSIGN_UUID)
    private String uid;

    @TableField(value = "ios_ver_code")
    private Integer iosVerCode;

    @TableField(value = "ios_ver_name")
    private String iosVerName;

    @TableField(value = "apk_ver_code")
    private Integer apkVerCode;

    @TableField(value = "apk_ver_name")
    private String apkVerName;

    @TableField(value = "ios_jpush_regid")
    private String iosJpushRegid;

    @TableField(value = "apk_jpush_regid")
    private String apkJpushRegid;

    @TableField(value = "app_store_rated")
    private Integer appStoreRated;

    @TableField(value = "survey_button_status")
    private Integer surveyButtonStatus;

    @TableField(value = "forum_author_rating")
    private Double forumAuthorRating;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

}
