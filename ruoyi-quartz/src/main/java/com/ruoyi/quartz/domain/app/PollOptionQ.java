package com.ruoyi.quartz.domain.app;

import lombok.Data;

import java.util.List;

@Data
public class PollOptionQ {

    private Integer sn;

    private String content;

    private List<String> image;

}
