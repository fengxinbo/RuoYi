package com.ruoyi.quartz.domain.app;

import lombok.Data;

import java.util.List;

@Data
public class PollJsonQ {

    private Integer pollType;

    private Integer showResult;

    private List<PollOptionQ> pollOption;

    private Integer optionThreshold;

    private Integer haveImage;

    private String pollTitle;

}
