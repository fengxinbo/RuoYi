package com.ruoyi.quartz.domain.app;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@TableName("t_users")
public class AppUserQ {

    @TableId
    private String uid;

    private String nickname;

    private Integer gender;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate birthday;

    private String phone;

    private String invcode;

    private Integer regfrom;

    private String wxopenid;

    private String wxunionid;

    private String wxaccesstoken;

    private String wxrefreshtoken;

    private String headimgurl;

    private String appleid;

    private String token;

    @TableField(exist = false)
    private Double balance;

    private Integer state;

    private Integer ulevel;

    private Integer idle;

    @TableField(value = "key_user")
    private Integer keyUser;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

}
