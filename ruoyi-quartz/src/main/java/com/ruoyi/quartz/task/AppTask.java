package com.ruoyi.quartz.task;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.quartz.domain.app.AppUserOnlineQ;
import com.ruoyi.quartz.domain.app.ForumPostQ;
import com.ruoyi.quartz.mapper.app.AppUserOnlineQMapper;
import com.ruoyi.quartz.service.app.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Component("appTask")
public class AppTask {

    @Autowired
    private AppUserOnlineQMapper appUserOnlineQMapper;

    @Autowired
    private IUserOperLogQService userOperLogQuartzService;

    @Autowired
    private IDietPlanQService dietPlanQService;

    @Autowired
    private IDietLogQService dietLogQService;

    @Autowired
    private IAppUserDeviceInfoQService appUserDeviceInfoQService;

    @Autowired
    private IFoodsFavoriteQService foodsFavoriteQService;

    @Autowired
    private IAppUserIdleQService appUserIdleQService;

    @Autowired
    private IHealthCheckService healthCheckService;

    @Autowired
    private IGreenCheckQService greenCheckQService;

    @Autowired
    private ForumPostRsService forumPostRsService;

    @Autowired
    private ForumPostQService forumPostQService;

    @Autowired
    private ForumPostLikeQService forumPostLikeQService;

    @Autowired
    private ForumPostPollQService forumPostPollQService;

    public void recordAppUserOnlineCount(Integer minutes) {
        AppUserOnlineQ appUserOnlineQ = new AppUserOnlineQ();
        appUserOnlineQ.setOnline(userOperLogQuartzService.selectOnlineCount(minutes));
        appUserOnlineQMapper.insert(appUserOnlineQ);
    }

    public void deleteUserOnlineHistoryData(Integer days) {
        appUserOnlineQMapper.delete(new LambdaQueryWrapper<AppUserOnlineQ>().le(AppUserOnlineQ::getCtime, LocalDateTime.now().minusDays(days)));
    }

    public void deleteUserOperLogHistoryData(Integer days) {
        userOperLogQuartzService.deleteHistoryData(days);
    }

    public void deletePlan() {
        dietPlanQService.deletePlan();
    }

    public void inactivePlan() {
        dietPlanQService.inactivePlan();
    }

    public void deleteDietLogFoods() {
        dietLogQService.deleteDietLogFoods();
    }

    public void captureAppUserDeviceInfo() {
        appUserDeviceInfoQService.captureAppUserDeviceInfo();
    }

    public void deleteFoodsFavorite() {
        foodsFavoriteQService.truncate();
    }

    public void idleUser(Integer days) {
        appUserIdleQService.idle(days);
    }

    public void healthCheck() {
        healthCheckService.check();
    }

    public void greenCheck() throws Exception {
        greenCheckQService.scan();
    }

    public void updatePostScore() {
        List<ForumPostQ> list = forumPostQService.selectList();
        list.forEach(post -> forumPostRsService.updateScore(post.getId()));
    }

    public void updateOldPostScore() {
        List<ForumPostQ> list = forumPostQService.selectOldPostList();
        list.forEach(post -> forumPostRsService.updateOldPostScore(post.getId()));
    }

    public void dummyLike(Integer bizType, String bizId, Integer likeCount, Long interval) {
        forumPostLikeQService.dummyLike(bizType, bizId, likeCount, interval);
    }

    public void dummyPoll(String postId, Integer pollCount, String jsonStr) {
        forumPostPollQService.dummyPoll(postId, pollCount, jsonStr);
    }
}
