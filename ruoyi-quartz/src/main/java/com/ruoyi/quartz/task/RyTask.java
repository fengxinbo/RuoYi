package com.ruoyi.quartz.task;

import cn.jiguang.sdk.api.PushApi;
import cn.jiguang.sdk.bean.push.PushSendParam;
import cn.jiguang.sdk.bean.push.PushSendResult;
import cn.jiguang.sdk.bean.push.audience.Audience;
import cn.jiguang.sdk.bean.push.message.notification.NotificationMessage;
import cn.jiguang.sdk.enums.platform.Platform;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.quartz.domain.app.*;
import com.ruoyi.quartz.service.app.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 定时任务调度测试
 *
 * @author ruoyi
 */
@Slf4j
@Component("ryTask")
public class RyTask {

    @Autowired
    private PushApi pushApi;

    @Autowired
    private AppUserQService appUserQService;

    @Autowired
    private IAppUserExtQService appUserExtQService;

    @Autowired
    private IUserOperLogQService userOperLogQuartzService;

    @Autowired
    private IJPushStrategyQService jPushStrategyService;

    @Autowired
    private IStreakQService streakQuartzService;

    @Autowired
    private IStreakLogQService streakLogQuartzService;

    public void jpushAlert() {
        List<AppUserExtQ> list = appUserExtQService.selectJPushAppUser();
        list.stream().forEach(appUserQ -> {
            UserOperLogQ userOperLogQ = userOperLogQuartzService.selectLastUserOperLog(appUserQ.getUid());
            if (userOperLogQ != null) {
                JPushAlertQ jPushAlertQ = jPushStrategyService.getAlert(userOperLogQ);
                if (jPushAlertQ != null)
                    push(jPushAlertQ, appUserQ);
            }
        });
    }

    private void push(JPushAlertQ jPushAlertQ, AppUserExtQ appUserExtQ) {
        if (jPushAlertQ != null) {
            if (StringUtils.isNotBlank(appUserExtQ.getIosJpushRegid())) {
                // 苹果
                PushSendParam param = new PushSendParam();
                // 通知内容
                NotificationMessage.IOS iOS = new NotificationMessage.IOS();
                Map<String, String> iOSAlert = new HashMap<>();
                iOSAlert.put("title", jPushAlertQ.getTitle());
                iOSAlert.put("subtitle", jPushAlertQ.getSubtitle());
                iOSAlert.put("body", jPushAlertQ.getBody());
                iOS.setAlert(iOSAlert);

                NotificationMessage notificationMessage = new NotificationMessage();
                notificationMessage.setIos(iOS);
                param.setNotification(notificationMessage);

                // 目标人群
                Audience audience = new Audience();
                audience.setRegistrationIdList(Arrays.asList(appUserExtQ.getIosJpushRegid()));
                // 指定目标
                param.setAudience(audience);
                // 指定平台
                param.setPlatform(Arrays.asList(Platform.ios));
                // options
//                    Options options = new Options();
//                    options.setApnsProduction(false); // false:开发环境 true:生产环境（默认）
//                    param.setOptions(options);
                // 发送
                PushSendResult result = pushApi.send(param);
                log.debug("result:{}", result);
            } else {
                // 安卓
                PushSendParam param = new PushSendParam();
                // 通知内容
                NotificationMessage.Android android = new NotificationMessage.Android();
                android.setAlert(jPushAlertQ.getBody());
                android.setChannelId("124357");// 小米特殊要求，channel_id为必填项

                NotificationMessage notificationMessage = new NotificationMessage();
                notificationMessage.setAndroid(android);
                param.setNotification(notificationMessage);

                // 目标人群
                Audience audience = new Audience();
                audience.setRegistrationIdList(Arrays.asList(appUserExtQ.getApkJpushRegid()));
                // 指定目标
                param.setAudience(audience);
                // 指定平台
                param.setPlatform(Arrays.asList(Platform.android));
                // 发送
                PushSendResult result = pushApi.send(param);
                log.debug("result:{}", result);
            }
        }
    }

    public void unmarkStreak() {
        LocalDate someday = LocalDate.now();
        List<AppUserQ> enabledUsers = appUserQService.selectEnabledAppUser();
        enabledUsers.stream().forEach(user -> {
            String uid = user.getUid();
            AppUserExtQ appUserExtQ = appUserExtQService.selectById(uid);
            StreakLogQ streakLog = streakLogQuartzService.selectByUidCtime(uid, someday);
            if (streakLog == null) {
                // 说明定时调度之前当日用户未登录，当日不是连胜
                // 检查用户前一日的连胜情况
                StreakLogQ yesterday = streakLogQuartzService.selectByUidCtime(uid, someday.minusDays(1));
                if (yesterday != null) {
                    if (yesterday.getChecked() == 1) {
                        // 昨天有连胜，分两种情况：a.昨天的streak<2则重置；b.昨天的streak>=2则进入保护期
                        if (yesterday.getStreak() < 2) {
                            reset(uid, someday);
                        }
                        if (yesterday.getStreak() >= 2) {
                            streakLog = new StreakLogQ();
                            streakLog.setUid(uid);
                            streakLog.setChecked(0);
                            streakLog.setReset(0);
                            streakLog.setStreak(yesterday.getStreak());
                            streakLog.setCtime(someday);
                            streakLogQuartzService.insert(streakLog);
                            StreakQ streak = streakQuartzService.selectById(uid);
                            if (streak != null) {
                                // 更新 streak
                                streak.setLevel(getStreakLevel(streakLog.getStreak()));
                                streak.setGap(getStreakGap(streakLog.getStreak()));
                                streak.setStreak(streakLog.getStreak());
                                streak.setMaxStreak(streakLog.getStreak() > streak.getMaxStreak() ? streakLog.getStreak() : streak.getMaxStreak());
                                streak.setProtectionPeriod(1);
                                streak.setCtime(someday);
                                streakQuartzService.updateById(streak);
                                // 推送保护期通知
                                JPushAlertQ jPushAlertQ = jPushStrategyService.selectById(7);
                                if (jPushAlertQ != null && appUserExtQ != null)
                                    push(jPushAlertQ, appUserExtQ);
                            } else {
                                streak = new StreakQ();
                                streak.setUid(uid);
                                streak.setLevel(getStreakLevel(streakLog.getStreak()));
                                streak.setGap(getStreakGap(streakLog.getStreak()));
                                streak.setStreak(streakLog.getStreak());
                                streak.setMaxStreak(streakLog.getStreak());
                                streak.setProtectionPeriod(0);
                                streak.setCtime(someday);
                                streakQuartzService.insert(streak);
                            }
                        }
                    } else {
                        // 昨天无连胜，连续两天断档，触发重置条件了
                        reset(uid, someday);
                    }
                } else {
                    // 昨天无记录，今天也无记录，连续两天断档，触发重置条件了
                    reset(uid, someday);
                }
            }
        });
    }

    private void reset(String uid, LocalDate someday) {
        StreakLogQ streakLog = new StreakLogQ();
        streakLog.setUid(uid);
        streakLog.setChecked(0);
        streakLog.setReset(1);
        streakLog.setStreak(0);
        streakLog.setCtime(someday);
        streakLogQuartzService.insert(streakLog);
        StreakQ streak = streakQuartzService.selectById(uid);
        if (streak != null) {
            streak.setLevel(getStreakLevel(streakLog.getStreak()));
            streak.setGap(getStreakGap(streakLog.getStreak()));
            streak.setStreak(streakLog.getStreak());
            streak.setMaxStreak(streakLog.getStreak() > streak.getMaxStreak() ? streakLog.getStreak() : streak.getMaxStreak());
            streak.setProtectionPeriod(0);
            streak.setCtime(someday);
            streakQuartzService.updateById(streak);
        } else {
            streak = new StreakQ();
            streak.setUid(uid);
            streak.setLevel(getStreakLevel(streakLog.getStreak()));
            streak.setGap(getStreakGap(streakLog.getStreak()));
            streak.setStreak(streakLog.getStreak());
            streak.setMaxStreak(streakLog.getStreak());
            streak.setProtectionPeriod(0);
            streak.setCtime(someday);
            streakQuartzService.insert(streak);
        }
    }

    private Integer getStreakLevel(Integer streak) {
        Integer level = 1;
        if (streak >= 2 && streak <= 6)
            level = 2;
        if (streak >= 7 && streak <= 13)
            level = 3;
        if (streak >= 14 && streak <= 29)
            level = 4;
        if (streak >= 30 && streak <= 99)
            level = 5;
        if (streak >= 100 && streak <= 364)
            level = 6;
        if (streak >= 365)
            level = 7;
        return level;
    }

    private Integer getStreakGap(Integer streak) {
        Integer gap = 0;
        if (streak >= 0 && streak < 2)
            gap = 2 - streak;
        if (streak >= 2 && streak <= 6)
            gap = 7 - streak;
        if (streak >= 7 && streak <= 13)
            gap = 14 - streak;
        if (streak >= 14 && streak <= 29)
            gap = 30 - streak;
        if (streak >= 30 && streak <= 99)
            gap = 100 - streak;
        if (streak >= 100 && streak <= 364)
            gap = 365 - streak;
        return gap;
    }
}
