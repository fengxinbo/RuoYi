package com.ruoyi.quartz.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.quartz.domain.app.AppUserDeviceInfoQ;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AppUserDeviceInfoQMapper extends BaseMapper<AppUserDeviceInfoQ> {}