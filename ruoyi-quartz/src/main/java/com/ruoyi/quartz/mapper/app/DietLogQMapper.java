package com.ruoyi.quartz.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.quartz.domain.app.DietLogQ;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DietLogQMapper extends BaseMapper<DietLogQ> {

    List<String> selectDeletedDietLog();

}
