package com.ruoyi.quartz.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.quartz.domain.app.UserOperLogQ;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserOperLogQMapper extends BaseMapper<UserOperLogQ> {
    Integer selectOnlineCount(Integer minutes);
}