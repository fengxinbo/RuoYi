package com.ruoyi.quartz.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.quartz.domain.app.ForumPostQ;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ForumPostQMapper extends BaseMapper<ForumPostQ> {}