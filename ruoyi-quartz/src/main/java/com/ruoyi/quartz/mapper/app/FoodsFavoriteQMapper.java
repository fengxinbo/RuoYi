package com.ruoyi.quartz.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.quartz.domain.app.FoodsFavoriteQ;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface FoodsFavoriteQMapper extends BaseMapper<FoodsFavoriteQ> {
    @Update("TRUNCATE TABLE t_foods_favorite")
    void truncate();
}