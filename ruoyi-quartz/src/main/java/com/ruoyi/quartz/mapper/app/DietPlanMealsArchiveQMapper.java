package com.ruoyi.quartz.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.quartz.domain.app.DietPlanMealsArchiveQ;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DietPlanMealsArchiveQMapper extends BaseMapper<DietPlanMealsArchiveQ> {}