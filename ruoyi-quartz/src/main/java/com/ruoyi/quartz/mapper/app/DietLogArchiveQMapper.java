package com.ruoyi.quartz.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.quartz.domain.app.DietLogArchiveQ;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DietLogArchiveQMapper extends BaseMapper<DietLogArchiveQ> {}