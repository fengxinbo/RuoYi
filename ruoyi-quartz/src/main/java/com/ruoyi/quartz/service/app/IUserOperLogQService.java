package com.ruoyi.quartz.service.app;

import com.ruoyi.quartz.domain.app.UserOperLogQ;

import java.util.List;

public interface IUserOperLogQService {

    UserOperLogQ selectLastUserOperLog(String uid);

    Integer selectOnlineCount(Integer num);

    void deleteHistoryData(Integer days);

    List<UserOperLogQ> selectYesterdayUserOperLog();

}
