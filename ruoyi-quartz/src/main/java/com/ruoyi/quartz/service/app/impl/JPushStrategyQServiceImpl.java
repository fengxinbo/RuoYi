package com.ruoyi.quartz.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.quartz.domain.app.JPushAlertQ;
import com.ruoyi.quartz.domain.app.JPushRoundQ;
import com.ruoyi.quartz.domain.app.UserOperLogQ;
import com.ruoyi.quartz.mapper.app.JPushAlertQMapper;
import com.ruoyi.quartz.mapper.app.JPushRoundQMapper;
import com.ruoyi.quartz.service.app.IJPushStrategyQService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
public class JPushStrategyQServiceImpl implements IJPushStrategyQService {

    @Autowired
    private JPushAlertQMapper jPushAlertQMapper;

    @Autowired
    private JPushRoundQMapper jPushRoundQMapper;

    @Override
    public JPushAlertQ getAlert(UserOperLogQ userOperLogQ) {
        JPushAlertQ jPushAlertQ = null;
        long days = Duration.between(userOperLogQ.getCtime(), LocalDateTime.now()).toDays();
        if (days < 2 || days > 8)
            return jPushAlertQ;

        // 判断轮次
        JPushRoundQ round = jPushRoundQMapper.selectById(userOperLogQ.getUid());
        if (round != null) {
            if (days == 8) {
                round.setRound(round.getRound() + 1);
                round.setEtime(LocalDateTime.now());
                jPushRoundQMapper.updateById(round);
            }
            return jPushAlertQ;
        } else {
            if (days == 8) {
                round = new JPushRoundQ();
                round.setUid(userOperLogQ.getUid());
                jPushRoundQMapper.insert(round);
            }
        }

        List<JPushAlertQ> list = jPushAlertQMapper.selectList(new LambdaQueryWrapper<JPushAlertQ>().eq(JPushAlertQ::getSn, days));
        if (!list.isEmpty())
            jPushAlertQ = list.get(0);
        log.debug("推送内容：{}", jPushAlertQ);
        return jPushAlertQ;
    }

    @Override
    public JPushAlertQ selectById(Integer id) {
        return jPushAlertQMapper.selectById(id);
    }
}
