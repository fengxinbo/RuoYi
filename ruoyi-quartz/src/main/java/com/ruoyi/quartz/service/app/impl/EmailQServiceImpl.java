package com.ruoyi.quartz.service.app.impl;

import cn.hutool.extra.mail.MailAccount;
import cn.hutool.extra.mail.MailUtil;
import com.ruoyi.quartz.config.EmailConfigQ;
import com.ruoyi.quartz.service.app.IEmailQService;
import org.springframework.stereotype.Service;

@Service
public class EmailQServiceImpl implements IEmailQService {
    @Override
    public void sendEmail(String subject, String content) {
        MailAccount account = new MailAccount();
        account.setHost(EmailConfigQ.getHost());
        account.setPort(EmailConfigQ.getPort());
        account.setFrom(EmailConfigQ.getFrom());
        account.setUser(EmailConfigQ.getUser());
        account.setPass(EmailConfigQ.getPass());
        account.setAuth(true);
        account.setSslEnable(true);

        // 发送邮件
        MailUtil.send(account, EmailConfigQ.getTo(), subject, content, true);
    }
}
