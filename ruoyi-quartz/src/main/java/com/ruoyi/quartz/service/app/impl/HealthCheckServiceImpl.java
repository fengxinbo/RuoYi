package com.ruoyi.quartz.service.app.impl;

import com.ruoyi.quartz.config.ProfileConfig;
import com.ruoyi.quartz.service.app.IAliyunSmsQService;
import com.ruoyi.quartz.service.app.IHealthCheckService;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HealthCheckServiceImpl implements IHealthCheckService {

    private static final String UID = "6ef091d636073401ee9beba71a84ca646706ace6288c728ae9eafd90576e0bf262a0917674e3034fc2d8df0328a5d793";

    private static final String TOKEN = "f85de2615a4c49704dd176d9414b3a47a46af0e01153dc65ce530ec80cce2759";

    private static final String API_URL_PREFIX = "https://api.leungnutritionsciences.cn:8443";

    private static final List<Map<String, String>> apis = new ArrayList<Map<String, String>>() {
        {
            add(new HashMap<String, String>() {
                {
                    put("method", "post");
                    put("url", "/foods/query_foods_by_id");
                    put("service", "food");
                    put("params", "{\"fid\":\"38278aacf79b25adf1b5b50af1e7b540\"}");
                }
            });
            add(new HashMap<String, String>() {
                {
                    put("method", "post");
                    put("url", "/forum/post/list");
                    put("service", "forum");
                    put("params", "{}");
                }
            });
            add(new HashMap<String, String>() {
                {
                    put("method", "post");
                    put("url", "/plans/meals/list");
                    put("service", "plan");
                    put("params", "{}");
                }
            });
            add(new HashMap<String, String>() {
                {
                    put("method", "post");
                    put("url", "/plans/meals/list");
                    put("service", "plan");
                    put("params", "{}");
                }
            });
            add(new HashMap<String, String>() {
                {
                    put("method", "post");
                    put("url", "/tools/idc/list");
                    put("service", "tool");
                    put("params", "{}");
                }
            });
            add(new HashMap<String, String>() {
                {
                    put("method", "post");
                    put("url", "/users/profile/get");
                    put("service", "user");
                    put("params", "{}");
                }
            });

        }
    };

    @Autowired
    private ProfileConfig profileConfig;

    @Autowired
    private IAliyunSmsQService aliyunSmsQService;

    @Override
    public void check() {
        if (!"test".equalsIgnoreCase(profileConfig.getActiveProfile()))
            return;

        CloseableHttpClient httpClient = HttpClients.createDefault();
        for (Map<String, String> item : apis) {
            String method = item.get("method");
            if ("post".equals(method)) {
                String url = item.get("url");
                HttpPost httpPost = new HttpPost(API_URL_PREFIX + url);
                httpPost.setEntity(new StringEntity(item.get("params"), "UTF-8"));
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setHeader("uid", UID);
                httpPost.setHeader("token", TOKEN);
                try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                    if (response.getStatusLine().getStatusCode() != 200) {
                        aliyunSmsQService.sendSms("18620302730", url, item.get("service"));
                        break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                // get
            }
            try {
                Thread.currentThread().sleep(60000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        try {
            httpClient.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
