package com.ruoyi.quartz.service.app;

public interface IEmailQService {
    void sendEmail(String subject, String content);
}
