package com.ruoyi.quartz.service.app.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.quartz.mapper.app.FoodsFavoriteQMapper;
import com.ruoyi.quartz.service.app.IFoodsFavoriteQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@DataSource(DataSourceType.SLAVE)
public class FoodsFavoriteQServiceImpl implements IFoodsFavoriteQService {

    @Autowired
    private FoodsFavoriteQMapper foodsFavoriteQMapper;

    @Override
    public void truncate() {
        foodsFavoriteQMapper.truncate();
    }
}
