package com.ruoyi.quartz.service.app;

public interface IDietPlanQService {

    void deletePlan();

    void inactivePlan();

}
