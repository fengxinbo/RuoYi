package com.ruoyi.quartz.service.app;

import com.ruoyi.quartz.domain.app.StreakLogQ;

import java.time.LocalDate;

public interface IStreakLogQService {
    StreakLogQ selectByUidCtime(String uid, LocalDate ctime);

    int insert(StreakLogQ streakLogQ);
}
