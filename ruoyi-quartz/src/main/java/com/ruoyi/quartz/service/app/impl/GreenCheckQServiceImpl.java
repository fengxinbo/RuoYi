package com.ruoyi.quartz.service.app.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.green20220302.Client;
import com.aliyun.green20220302.models.*;
import com.aliyun.teaopenapi.models.Config;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.quartz.config.OssConfigQ;
import com.ruoyi.quartz.domain.app.ForumPostCommentQ;
import com.ruoyi.quartz.domain.app.ForumPostCommentReplyQ;
import com.ruoyi.quartz.domain.app.ForumPostQ;
import com.ruoyi.quartz.domain.app.GreenCheckQ;
import com.ruoyi.quartz.mapper.app.*;
import com.ruoyi.quartz.service.app.IEmailQService;
import com.ruoyi.quartz.service.app.IGreenCheckQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class GreenCheckQServiceImpl implements IGreenCheckQService {

    @Autowired
    private GreenCheckQMapper greenCheckQMapper;

    @Autowired
    private IEmailQService emailQService;

    @Autowired
    private AppUserQMapper appUserQMapper;

    @Autowired
    private ForumPostQMapper forumPostQMapper;

    @Autowired
    private ForumPostCommentQMapper forumPostCommentQMapper;

    @Autowired
    private ForumPostCommentReplyQMapper forumPostCommentReplyQMapper;

    private static final String ENDPOINT = "green-cip.cn-shanghai.aliyuncs.com";

    @Override
    public void scan() throws Exception {
        Client client = createClient(OssConfigQ.getAccessKeyId(), OssConfigQ.getAccessKeySecret(), ENDPOINT);
        List<GreenCheckQ> pendingObjects = greenCheckQMapper.selectList(new LambdaQueryWrapper<GreenCheckQ>().eq(GreenCheckQ::getChecked, 0).ge(GreenCheckQ::getDue, LocalDateTime.now()));
        for (GreenCheckQ greenCheckQ : pendingObjects) {
            if (greenCheckQ.getFileType() == 1) {
                // 图片
                DescribeImageModerationResultRequest request = new DescribeImageModerationResultRequest();
                request.setReqId(greenCheckQ.getReqId());
                DescribeImageModerationResultResponse response = client.describeImageModerationResult(request);
                if (response.statusCode == 200) {
                    DescribeImageModerationResultResponseBody body = response.getBody();
                    if (body.getCode() == 200) {
                        DescribeImageModerationResultResponseBody.DescribeImageModerationResultResponseBodyData data = body.getData();
                        if (data != null) {
                            String riskLevel = data.getRiskLevel();
                            String resultJson = JSONObject.toJSONString(data.getResult());
                            updateGreenCheck(greenCheckQ, riskLevel, resultJson);
                            if (greenCheckQ.getRiskLevel() > 0) {
                                String scenario = greenCheckQ.getBizScenario();
                                if ("社区帖子封面".equals(scenario) || "社区帖子图片".equals(scenario) || "社区帖子投票选项图片".equals(scenario)) {
                                    ForumPostQ forumPostQ = forumPostQMapper.selectById(greenCheckQ.getBizId());
                                    if (forumPostQ != null) {
                                        forumPostQ.setVisibleScope(1); // 仅自己可见
                                        forumPostQ.setMachineAudit(0); // 机审不通过
                                        forumPostQ.setAudit(0); // 总审核状态不通过
                                        forumPostQ.setEtime(LocalDateTime.now());
                                        forumPostQMapper.updateById(forumPostQ);
                                    }
                                }
                                if ("社区帖子评论图片".equals(scenario)) {
                                    ForumPostCommentQ forumPostCommentQ = forumPostCommentQMapper.selectById(greenCheckQ.getBizId());
                                    if (forumPostCommentQ != null) {
                                        forumPostCommentQ.setVisibleScope(1); // 仅自己可见
                                        forumPostCommentQMapper.updateById(forumPostCommentQ);
                                    }
                                }
                                if ("社区帖子回复图片".equals(scenario)) {
                                    ForumPostCommentReplyQ forumPostCommentReplyQ = forumPostCommentReplyQMapper.selectById(greenCheckQ.getBizId());
                                    if (forumPostCommentReplyQ != null) {
                                        forumPostCommentReplyQ.setVisibleScope(1); // 仅自己可见
                                        forumPostCommentReplyQMapper.updateById(forumPostCommentReplyQ);
                                    }
                                }
                                emailQService.sendEmail(getEmailSubject(greenCheckQ.getRiskLevel()), getEmailContent(greenCheckQ.getUid(), greenCheckQ.getBizScenario(), greenCheckQ.getOssUrl()));
                            }
                            Thread.currentThread().sleep(1000);
                        }
                    }
                }
            }
            if (greenCheckQ.getFileType() == 2) {
                // 视频
                String taskId = greenCheckQ.getTaskId();
                if (StrUtil.isNotBlank(taskId)) {
                    JSONObject serviceParameters = new JSONObject();
                    serviceParameters.put("taskId", taskId);
                    VideoModerationResultRequest request = new VideoModerationResultRequest();
                    request.setService("videoDetection");
                    request.setServiceParameters(serviceParameters.toJSONString());
                    VideoModerationResultResponse response = client.videoModerationResult(request);
                    if (response.statusCode == 200) {
                        VideoModerationResultResponseBody body = response.getBody();
                        if (body.getCode() == 200) {
                            VideoModerationResultResponseBody.VideoModerationResultResponseBodyData data = body.getData();
                            if (data != null) {
                                String riskLevel = data.getRiskLevel();
                                String resultJson = JSONObject.toJSONString(data.getFrameResult());
                                updateGreenCheck(greenCheckQ, riskLevel, resultJson);
                                if (greenCheckQ.getRiskLevel() > 0) {
                                    String scenario = greenCheckQ.getBizScenario();
                                    if ("社区帖子封面".equals(scenario) || "社区帖子视频".equals(scenario)) {
                                        ForumPostQ forumPostQ = forumPostQMapper.selectById(greenCheckQ.getBizId());
                                        if (forumPostQ != null) {
                                            forumPostQ.setVisibleScope(1); // 仅自己可见
                                            forumPostQ.setMachineAudit(0); // 机审不通过
                                            forumPostQ.setAudit(0); // 总审核状态不通过
                                            forumPostQ.setEtime(LocalDateTime.now());
                                            forumPostQMapper.updateById(forumPostQ);
                                        }
                                    }
                                    emailQService.sendEmail(getEmailSubject(greenCheckQ.getRiskLevel()), getEmailContent(greenCheckQ.getUid(), greenCheckQ.getBizScenario(), greenCheckQ.getOssUrl()));
                                }
                                Thread.currentThread().sleep(1000);
                            }
                        }
                    }
                }
            }
            if (greenCheckQ.getFileType() == 3) {
                // 文本
                String content = "未获取到内容";
                if (greenCheckQ.getBizScenario().contains("帖子标题")) {
                    ForumPostQ forumPostQ = forumPostQMapper.selectById(greenCheckQ.getBizId());
                    if (forumPostQ != null) {
                        content = forumPostQ.getTitle();
                    }
                }
                if (greenCheckQ.getBizScenario().contains("帖子内容")) {
                    ForumPostQ forumPostQ = forumPostQMapper.selectById(greenCheckQ.getBizId());
                    if (forumPostQ != null) {
                        content = forumPostQ.getContent();
                    }
                }
                if (greenCheckQ.getBizScenario().contains("评论内容")) {
                    ForumPostCommentQ forumPostCommentQ = forumPostCommentQMapper.selectById(greenCheckQ.getBizId());
                    if (forumPostCommentQ != null) {
                        content = forumPostCommentQ.getContent();
                    }
                }
                if (greenCheckQ.getBizScenario().contains("回复内容")) {
                    ForumPostCommentReplyQ forumPostCommentReplyQ = forumPostCommentReplyQMapper.selectById(greenCheckQ.getBizId());
                    if (forumPostCommentReplyQ != null) {
                        content = forumPostCommentReplyQ.getContent();
                    }
                }
                emailQService.sendEmail(getEmailSubject(greenCheckQ.getRiskLevel()), getTextEmailContent(greenCheckQ.getUid(), greenCheckQ.getBizScenario(), content));
                greenCheckQ.setChecked(1);
                greenCheckQ.setEtime(LocalDateTime.now());
                greenCheckQMapper.updateById(greenCheckQ);
            }
        }
    }

    private void updateGreenCheck(GreenCheckQ greenCheckQ, String riskLevel, String resultJson) throws Exception {
        greenCheckQ.setChecked(1);
        greenCheckQ.setResult(resultJson);
        if ("low".equalsIgnoreCase(riskLevel)) {
            greenCheckQ.setRiskLevel(1);
        } else if ("medium".equalsIgnoreCase(riskLevel)) {
            greenCheckQ.setRiskLevel(2);
        } else if ("high".equalsIgnoreCase(riskLevel)) {
            greenCheckQ.setRiskLevel(3);
        } else if ("none".equalsIgnoreCase(riskLevel)) {
            greenCheckQ.setProcessed(1);
        } else {
            throw new Exception("未知的审核结果");
        }
        greenCheckQ.setEtime(LocalDateTime.now());
        greenCheckQMapper.updateById(greenCheckQ);
    }

    private static Client createClient(String accessKeyId, String accessKeySecret, String endpoint) throws Exception {
        Config config = new Config();
        config.setAccessKeyId(accessKeyId);
        config.setAccessKeySecret(accessKeySecret);
        config.setEndpoint(endpoint);
        return new Client(config);
    }

    private static String getRiskLevel(Integer riskLevel) {
        if (riskLevel == 0) {
            return "无风险";
        } else if (riskLevel == 1) {
            return "低风险";
        } else if (riskLevel == 2) {
            return "中风险";
        } else if (riskLevel == 3) {
            return "高风险";
        } else {
            return "未知";
        }
    }

    private String getEmailSubject(Integer riskLevel) {
        return String.format("CS%s告警", getRiskLevel(riskLevel));
    }

    private String getEmailContent(String uid, String scenario, String ossUrl) {
        return "<p>" + appUserQMapper.selectById(uid).getNickname() + "&nbsp;&nbsp;&nbsp;&nbsp;" + scenario + "&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"" + ossUrl + "\">🔗</a></p>";
    }

    private String getTextEmailContent(String uid, String scenario, String content) {
        return "<p>" + appUserQMapper.selectById(uid).getNickname() + "&nbsp;&nbsp;&nbsp;&nbsp;" + scenario + "&nbsp;&nbsp;&nbsp;&nbsp;" + content + "</p>";
    }
}
