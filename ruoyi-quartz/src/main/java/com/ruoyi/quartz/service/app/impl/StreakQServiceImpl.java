package com.ruoyi.quartz.service.app.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.quartz.domain.app.StreakQ;
import com.ruoyi.quartz.mapper.app.StreakQMapper;
import com.ruoyi.quartz.service.app.IStreakQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@DataSource(DataSourceType.SLAVE)
public class StreakQServiceImpl implements IStreakQService {

    @Autowired
    private StreakQMapper streakQMapper;

    @Override
    public StreakQ selectById(String uid) {
        return streakQMapper.selectById(uid);
    }

    @Override
    public int updateById(StreakQ streakQ) {
        return streakQMapper.updateById(streakQ);
    }

    @Override
    public int insert(StreakQ streakQ) {
        return streakQMapper.insert(streakQ);
    }
}
