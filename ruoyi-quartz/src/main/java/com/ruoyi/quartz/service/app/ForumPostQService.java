package com.ruoyi.quartz.service.app;

import com.ruoyi.quartz.domain.app.ForumPostQ;

import java.util.List;

public interface ForumPostQService {

    List<ForumPostQ> selectList();

    List<ForumPostQ> selectOldPostList();

}