package com.ruoyi.quartz.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.quartz.domain.app.AppUserExtQ;
import com.ruoyi.quartz.mapper.app.AppUserExtQMapper;
import com.ruoyi.quartz.mapper.app.AppUserQMapper;
import com.ruoyi.quartz.service.app.IAppUserExtQService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
@Slf4j
public class AppUserExtQServiceImpl implements IAppUserExtQService {

    @Autowired
    private AppUserQMapper appUserQMapper;

    @Autowired
    private AppUserExtQMapper appUserExtQMapper;

    @Override
    public List<AppUserExtQ> selectJPushAppUser() {
        return appUserExtQMapper.selectList(new LambdaQueryWrapper<AppUserExtQ>().isNotNull(AppUserExtQ::getIosJpushRegid).or().isNotNull(AppUserExtQ::getApkJpushRegid));
    }

    @Override
    public AppUserExtQ selectById(String id) {
        return appUserExtQMapper.selectById(id);
    }
}
