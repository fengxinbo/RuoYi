package com.ruoyi.quartz.service.app.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.quartz.domain.app.*;
import com.ruoyi.quartz.mapper.app.*;
import com.ruoyi.quartz.service.app.IAppUserIdleQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class AppUserIdleQServiceImpl implements IAppUserIdleQService {

    @Autowired
    private AppUserQMapper userMapper;

    @Autowired
    private DietPlanQMapper planMapper;

    @Autowired
    private DietPlanArchiveQMapper planArchiveMapper;

    @Autowired
    private DietPlanDetailsQMapper planDetailsMapper;

    @Autowired
    private DietPlanDetailsArchiveQMapper planDetailsArchiveMapper;

    @Autowired
    private DietPlanMealsQMapper planMealsMapper;

    @Autowired
    private DietPlanMealsArchiveQMapper planMealsArchiveMapper;

    @Autowired
    private DietLogQMapper dietLogMapper;

    @Autowired
    private DietLogArchiveQMapper dietLogArchiveMapper;

    @Autowired
    private DietLogFoodsQMapper dietLogFoodsMapper;

    @Autowired
    private DietLogFoodsArchiveQMapper dietLogFoodsArchiveMapper;

    @Override
    @Transactional
    public void idle(Integer days) {
        List<AppUserQ> users = userMapper.selectList(new LambdaQueryWrapper<AppUserQ>()
                .eq(AppUserQ::getIdle, 0)
                .lt(AppUserQ::getEtime, LocalDateTime.now().minusDays(days)));
        users.forEach(user -> {
            user.setIdle(1);
            user.setToken("");
            userMapper.updateById(user);

            // 1.删除旧的计划归档数据（若有）
            List<DietPlanArchiveQ> planArchiveList = planArchiveMapper.selectList(new LambdaQueryWrapper<DietPlanArchiveQ>().eq(DietPlanArchiveQ::getUid, user.getUid()));
            planArchiveList.forEach(planArchive -> {
                List<DietPlanDetailsArchiveQ> planDetailsArchiveList = planDetailsArchiveMapper.selectList(new LambdaQueryWrapper<DietPlanDetailsArchiveQ>().eq(DietPlanDetailsArchiveQ::getPid, planArchive.getPid()));
                planDetailsArchiveList.forEach(planDetailsArchive -> {
                    planMealsArchiveMapper.delete(new LambdaQueryWrapper<DietPlanMealsArchiveQ>().eq(DietPlanMealsArchiveQ::getPdid, planDetailsArchive.getSid()));
                    planDetailsArchiveMapper.deleteById(planDetailsArchive);
                });
                planArchiveMapper.deleteById(planArchive);
            });

            // 2.删除旧的日志归档数据（若有）
            List<DietLogArchiveQ> dietLogArchiveList = dietLogArchiveMapper.selectList(new LambdaQueryWrapper<DietLogArchiveQ>().eq(DietLogArchiveQ::getUid, user.getUid()));
            dietLogArchiveList.forEach(dietLogArchive -> {
                dietLogFoodsArchiveMapper.delete(new LambdaQueryWrapper<DietLogFoodsArchiveQ>().eq(DietLogFoodsArchiveQ::getPid, dietLogArchive.getId()));
                dietLogArchiveMapper.deleteById(dietLogArchive);
            });

            // 3.将热计划数据复制到归档数据
            List<DietPlanQ> planList = planMapper.selectList(new LambdaQueryWrapper<DietPlanQ>().eq(DietPlanQ::getUid, user.getUid()));
            planList.forEach(plan -> {
                DietPlanArchiveQ planArchive = new DietPlanArchiveQ();
                BeanUtils.copyProperties(plan, planArchive);
                planArchiveMapper.insert(planArchive);
                List<DietPlanDetailsQ> planDetailsList = planDetailsMapper.selectList(new LambdaQueryWrapper<DietPlanDetailsQ>().eq(DietPlanDetailsQ::getPid, plan.getPid()));
                planDetailsList.forEach(planDetails -> {
                    DietPlanDetailsArchiveQ planDetailsArchive = new DietPlanDetailsArchiveQ();
                    BeanUtils.copyProperties(planDetails, planDetailsArchive);
                    planDetailsArchiveMapper.insert(planDetailsArchive);
                    List<DietPlanMealsQ> planMealsList = planMealsMapper.selectList(new LambdaQueryWrapper<DietPlanMealsQ>().eq(DietPlanMealsQ::getPdid,planDetails.getSid()));
                    planMealsList.forEach(planMeals -> {
                        DietPlanMealsArchiveQ planMealsArchive = new DietPlanMealsArchiveQ();
                        BeanUtils.copyProperties(planMeals, planMealsArchive);
                        planMealsArchiveMapper.insert(planMealsArchive);
                    });
                });
            });

            // 4.将热日志数据复制到归档数据
            List<DietLogQ> dietLogList = dietLogMapper.selectList(new LambdaQueryWrapper<DietLogQ>().eq(DietLogQ::getUid, user.getUid()));
            dietLogList.forEach(dietLog -> {
                DietLogArchiveQ dietLogArchive = new DietLogArchiveQ();
                BeanUtils.copyProperties(dietLog, dietLogArchive);
                dietLogArchiveMapper.insert(dietLogArchive);
                List<DietLogFoodsQ> dietLogFoodsList = dietLogFoodsMapper.selectList(new LambdaQueryWrapper<DietLogFoodsQ>().eq(DietLogFoodsQ::getPid, dietLog.getId()));
                dietLogFoodsList.forEach(dietLogFoods -> {
                    DietLogFoodsArchiveQ dietLogFoodsArchive = new DietLogFoodsArchiveQ();
                    BeanUtils.copyProperties(dietLogFoods, dietLogFoodsArchive);
                    dietLogFoodsArchiveMapper.insert(dietLogFoodsArchive);
                });
            });

            // 5.删除热计划数据
            planList.forEach(plan -> {
                List<DietPlanDetailsQ> planDetailsList = planDetailsMapper.selectList(new LambdaQueryWrapper<DietPlanDetailsQ>().eq(DietPlanDetailsQ::getPid, plan.getPid()));
                planDetailsList.forEach(planDetails -> {
                    planMealsMapper.delete(new LambdaQueryWrapper<DietPlanMealsQ>().eq(DietPlanMealsQ::getPdid, planDetails.getSid()));
                    planDetailsMapper.deleteById(planDetails);
                });
                planMapper.deleteById(plan);
            });

            // 6.删除热日志数据
            dietLogList.forEach(dietLog -> {
                dietLogFoodsMapper.delete(new LambdaQueryWrapper<DietLogFoodsQ>().eq(DietLogFoodsQ::getPid, dietLog.getId()));
                dietLogMapper.deleteById(dietLog);
            });
        });
    }
}
