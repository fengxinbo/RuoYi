package com.ruoyi.quartz.service.app;

public interface IAliyunSmsQService {

    Object sendSms(String phone, String url, String service);

}
