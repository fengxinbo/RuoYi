package com.ruoyi.quartz.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.quartz.domain.app.DietLogFoodsQ;
import com.ruoyi.quartz.mapper.app.DietLogFoodsQMapper;
import com.ruoyi.quartz.mapper.app.DietLogQMapper;
import com.ruoyi.quartz.service.app.IDietLogQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class DietLogQServiceImpl implements IDietLogQService {

    @Autowired
    private DietLogQMapper dietLogQMapper;

    @Autowired
    private DietLogFoodsQMapper dietLogFoodsQMapper;

    @Override
    public void deleteDietLogFoods() {
        List<String> ids = dietLogQMapper.selectDeletedDietLog();
        ids.forEach(id -> {
            dietLogFoodsQMapper.delete(new LambdaQueryWrapper<DietLogFoodsQ>().eq(DietLogFoodsQ::getPid, id));
            try {
                Thread.currentThread().sleep(100);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
    }

}
