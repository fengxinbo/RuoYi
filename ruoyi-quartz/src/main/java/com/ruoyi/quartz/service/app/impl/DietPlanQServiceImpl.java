package com.ruoyi.quartz.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.quartz.domain.app.DietPlanDetailsQ;
import com.ruoyi.quartz.domain.app.DietPlanMealsQ;
import com.ruoyi.quartz.domain.app.DietPlanQ;
import com.ruoyi.quartz.mapper.app.DietPlanDetailsQMapper;
import com.ruoyi.quartz.mapper.app.DietPlanMealsQMapper;
import com.ruoyi.quartz.mapper.app.DietPlanQMapper;
import com.ruoyi.quartz.service.app.IDietPlanQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class DietPlanQServiceImpl implements IDietPlanQService {

    @Autowired
    private DietPlanQMapper dietPlanQMapper;

    @Autowired
    private DietPlanDetailsQMapper dietPlanDetailsQMapper;

    @Autowired
    private DietPlanMealsQMapper dietPlanMealsQMapper;

    private static final Integer DIET_PLAN_DELETED = 1;

    private static final Integer DIET_PLAN_ACTIVE = 1;

    private static final Integer DIET_PLAN_INACTIVE = 0;

    @Override
    public void deletePlan() {
        LambdaQueryWrapper<DietPlanQ> wrapper1 = new LambdaQueryWrapper<>();
        wrapper1.eq(DietPlanQ::getDel, DIET_PLAN_DELETED);
        List<DietPlanQ> dietPlanList = dietPlanQMapper.selectList(wrapper1);
        dietPlanList.forEach(dietPlan -> {
            LambdaQueryWrapper<DietPlanDetailsQ> wrapper2 = new LambdaQueryWrapper<>();
            wrapper2.eq(DietPlanDetailsQ::getPid, dietPlan.getPid());
            List<DietPlanDetailsQ> details = dietPlanDetailsQMapper.selectList(wrapper2);
            details.forEach(dietPlanDetails -> {
                dietPlanMealsQMapper.delete(new LambdaQueryWrapper<DietPlanMealsQ>().eq(DietPlanMealsQ::getPdid, dietPlanDetails.getSid()));
            });
            dietPlanDetailsQMapper.delete(wrapper2);
        });
        dietPlanQMapper.delete(wrapper1);
    }

    @Override
    public void inactivePlan() {
        List<DietPlanQ> dietPlanList = dietPlanQMapper.selectList(new LambdaQueryWrapper<DietPlanQ>().eq(DietPlanQ::getState, DIET_PLAN_ACTIVE));
        dietPlanList.forEach(dietPlan -> {
            DietPlanDetailsQ dietPlanDetails = dietPlanDetailsQMapper.selectOne(new LambdaQueryWrapper<DietPlanDetailsQ>().eq(DietPlanDetailsQ::getPid, dietPlan.getPid()).orderByDesc(DietPlanDetailsQ::getSdate).last("limit 1"));
            if (dietPlanDetails != null && dietPlanDetails.getSdate().isBefore(LocalDate.now())) {
                dietPlan.setState(DIET_PLAN_INACTIVE);
                dietPlan.setEtime(LocalDateTime.now());
                dietPlanQMapper.updateById(dietPlan);
            }
        });
    }

}