package com.ruoyi.quartz.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.quartz.domain.app.UserOperLogQ;
import com.ruoyi.quartz.mapper.app.UserOperLogQMapper;
import com.ruoyi.quartz.service.app.IUserOperLogQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class UserOperLogQServiceImpl implements IUserOperLogQService {

    @Autowired
    private UserOperLogQMapper userOperLogQMapper;

    @Override
    public UserOperLogQ selectLastUserOperLog(String uid) {
        return userOperLogQMapper.selectOne(new LambdaQueryWrapper<UserOperLogQ>().eq(UserOperLogQ::getUid, uid).orderByDesc(UserOperLogQ::getCtime).last("limit 1"));
    }

    @Override
    public Integer selectOnlineCount(Integer minutes) {
        return userOperLogQMapper.selectOnlineCount(minutes);
    }

    @Override
    public void deleteHistoryData(Integer days) {
        userOperLogQMapper.delete(new LambdaQueryWrapper<UserOperLogQ>().lt(UserOperLogQ::getCtime, LocalDateTime.now().minusDays(days)));
    }

    @Override
    public List<UserOperLogQ> selectYesterdayUserOperLog() {
        // 获取今天的日期
        LocalDate today = LocalDate.now();
        // 计算昨天的日期
        LocalDate yesterday = today.minusDays(1);
        // 获取昨天0时0分0秒的时间
        LocalDateTime startOfDayYesterday = LocalDateTime.of(yesterday, LocalTime.MIDNIGHT);
        // 获取昨天23时59分59秒的时间
        LocalDateTime endOfDayYesterday = LocalDateTime.of(yesterday, LocalTime.MAX);
        List<UserOperLogQ> userOperLogQList = userOperLogQMapper.selectList(new LambdaQueryWrapper<UserOperLogQ>().ge(UserOperLogQ::getCtime, startOfDayYesterday).le(UserOperLogQ::getCtime, endOfDayYesterday).isNotNull(UserOperLogQ::getUid).select(UserOperLogQ::getUid).groupBy(UserOperLogQ::getUid));
        List<UserOperLogQ> result = new ArrayList<>();
        userOperLogQList.stream().forEach(userOperLogQ -> {
            List<UserOperLogQ> tempList = userOperLogQMapper.selectList(new LambdaQueryWrapper<UserOperLogQ>().eq(UserOperLogQ::getUid, userOperLogQ.getUid()).ge(UserOperLogQ::getCtime, startOfDayYesterday).le(UserOperLogQ::getCtime, endOfDayYesterday).like(UserOperLogQ::getInputparams, "phonetype").last("limit 1"));
            if (!tempList.isEmpty()) {
                result.add(tempList.get(0));
            }
        });
        return result;
    }

}
