package com.ruoyi.quartz.service.app.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.quartz.domain.SysConfigQ;
import com.ruoyi.quartz.mapper.SysConfigQMapper;
import com.ruoyi.quartz.service.app.SysConfigQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
@DataSource(DataSourceType.MASTER)
public class SysConfigQServiceImpl implements SysConfigQService {

    @Autowired
    private SysConfigQMapper sysConfigQMapper;

    @Override
    public SysConfigQ selectById(Serializable id) {
        return sysConfigQMapper.selectById(id);
    }
}
