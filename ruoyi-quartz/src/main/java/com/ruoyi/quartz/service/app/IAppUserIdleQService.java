package com.ruoyi.quartz.service.app;

public interface IAppUserIdleQService {
    void idle(Integer days);
}
