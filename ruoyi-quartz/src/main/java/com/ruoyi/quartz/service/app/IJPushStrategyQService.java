package com.ruoyi.quartz.service.app;

import com.ruoyi.quartz.domain.app.JPushAlertQ;
import com.ruoyi.quartz.domain.app.UserOperLogQ;

public interface IJPushStrategyQService {

    JPushAlertQ getAlert(UserOperLogQ userOperLogQ);

    JPushAlertQ selectById(Integer id);

}
