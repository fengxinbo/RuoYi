package com.ruoyi.quartz.service.app.impl;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.quartz.domain.app.ForumPostPollDataQ;
import com.ruoyi.quartz.domain.app.ForumPostQ;
import com.ruoyi.quartz.domain.app.PollJsonQ;
import com.ruoyi.quartz.domain.app.PollOptionQ;
import com.ruoyi.quartz.mapper.app.ForumPostPollDataQMapper;
import com.ruoyi.quartz.mapper.app.ForumPostQMapper;
import com.ruoyi.quartz.service.app.ForumPostPollQService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@DataSource(DataSourceType.SLAVE)
public class ForumPostPollQServiceImpl implements ForumPostPollQService {

    @Autowired
    private ForumPostQMapper forumPostQMapper;

    @Autowired
    private ForumPostPollDataQMapper forumPostPollDataMapper;

    public static void main(String[] args) {
        String jsonString = "[{\"sn\":1,\"rate\":0.25},{\"sn\":2,\"rate\":0.25},{\"sn\":3,\"rate\":0.25},{\"sn\":4,\"rate\":0.25}]";
        try {
            JSONArray jsonArray = JSON.parseArray(jsonString);
            // 进一步处理 jsonArray
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dummyPoll(String postId, Integer pollCount, String jsonStr) {
        log.info("dummyPoll: postId={}, pollCount={}, jsonStr={}", postId, pollCount, jsonStr);
        ForumPostQ post = forumPostQMapper.selectById(postId);
        if (post == null)
            throw new ServiceException("帖子不存在");
        if (post.getType() != 2)
            throw new ServiceException("帖子不是投票帖");
        PollJsonQ pollJson = post.getPollJson();
        if (pollJson == null)
            throw new ServiceException("帖子投票配置不存在");
        List<PollOptionQ> pollOptions = pollJson.getPollOption();
        if (pollOptions == null)
            throw new ServiceException("帖子投票配置选项不存在");
        int optionsSize = pollOptions.size();
        if (pollCount < optionsSize)
            throw new ServiceException("虚拟票数小于投票选项数量，无法分配");
        JSONArray jsonArray = JSON.parseArray(jsonStr);
        if (jsonArray.size() != optionsSize)
            throw new ServiceException("虚拟投票选项数量不匹配");

        // 构建 sn 到 rate 和 content 的映射
        Map<Integer, Double> rateMap = new HashMap<>();
        Map<Integer, String> contentMap = new HashMap<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            if (jsonObject.containsKey("rate") && jsonObject.containsKey("sn")) {
                int sn = jsonObject.getInteger("sn");
                double rate = jsonObject.getDouble("rate");
                rateMap.put(sn, rate);
            } else {
                throw new ServiceException("JSON 对象缺少 rate 或 sn 字段");
            }
        }

        // 验证 rate 总和是否等于 1
        double totalRate = 0.0;
        for (double rate : rateMap.values()) {
            totalRate += rate;
        }
        if (Math.abs(totalRate - 1.0) > 1e-9) {
            throw new ServiceException("JSON 对象数组中的 rate 字段总和不等于 1");
        }

        // 构建 sn 到 content 的映射
        for (PollOptionQ pollOption : pollOptions) {
            int sn = pollOption.getSn();
            String content = pollOption.getContent();
            contentMap.put(sn, content);
        }

        // 计算每个选项的投票数量
        Map<String, Integer> pollOptionCountMap = new HashMap<>();
        for (Map.Entry<Integer, Double> entry : rateMap.entrySet()) {
            int sn = entry.getKey();
            double rate = entry.getValue();
            String content = contentMap.get(sn);
            if (content == null) {
                throw new ServiceException("投票选项 sn 不匹配");
            }
            int count = (int) Math.round(rate * pollCount);
            pollOptionCountMap.put(content, count);
        }

        // 调整投票数量以确保总和为 pollCount
        int totalAssigned = pollOptionCountMap.values().stream().mapToInt(Integer::intValue).sum();
        if (totalAssigned < pollCount) {
            // 分配剩余的投票数量
            for (Map.Entry<String, Integer> entry : pollOptionCountMap.entrySet()) {
                if (totalAssigned >= pollCount) break;
                pollOptionCountMap.put(entry.getKey(), entry.getValue() + 1);
                totalAssigned++;
            }
        }

        for (Map.Entry<String, Integer> entry : pollOptionCountMap.entrySet()) {
            String content = entry.getKey();
            int count = entry.getValue();
            for (int i = 0; i < count; i++) {
                ForumPostPollDataQ pollData = new ForumPostPollDataQ();
                pollData.setPostId(postId);
                pollData.setUid("SU" + RandomUtil.randomString(30).toLowerCase());
                pollData.setPollType(pollJson.getPollType());
                List<String> pollOption = new java.util.ArrayList<>();
                pollOption.add(content);
                pollData.setPollOption(pollOption);
                forumPostPollDataMapper.insert(pollData);
            }
        }
    }
}