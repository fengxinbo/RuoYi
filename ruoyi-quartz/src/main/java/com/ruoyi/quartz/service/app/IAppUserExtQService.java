package com.ruoyi.quartz.service.app;


import com.ruoyi.quartz.domain.app.AppUserExtQ;

import java.util.List;

public interface IAppUserExtQService {

    List<AppUserExtQ> selectJPushAppUser();

    AppUserExtQ selectById(String id);

}
