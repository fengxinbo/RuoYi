package com.ruoyi.quartz.service.app;

public interface ForumPostRsService {

    void updateScore(String postId);

    void updateOldPostScore(String postId);

}