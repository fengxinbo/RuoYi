package com.ruoyi.quartz.service.app;

import com.ruoyi.quartz.domain.SysConfigQ;

import java.io.Serializable;

public interface SysConfigQService {
    SysConfigQ selectById(Serializable id);
}
