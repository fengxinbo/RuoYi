package com.ruoyi.quartz.service.app;

import com.ruoyi.quartz.domain.app.StreakQ;

public interface IStreakQService {
    StreakQ selectById(String uid);

    int updateById(StreakQ streakQ);

    int insert(StreakQ streakQ);
}
