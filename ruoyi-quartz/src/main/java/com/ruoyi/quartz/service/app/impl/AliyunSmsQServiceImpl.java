package com.ruoyi.quartz.service.app.impl;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.tea.TeaException;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.teautil.models.RuntimeOptions;
import com.ruoyi.quartz.config.OssConfigQ;
import com.ruoyi.quartz.service.app.IAliyunSmsQService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AliyunSmsQServiceImpl implements IAliyunSmsQService {

    private Client createClient() {
        Config config = new Config().setAccessKeyId(OssConfigQ.getAccessKeyId()).setAccessKeySecret(OssConfigQ.getAccessKeySecret());
        config.endpoint = "dysmsapi.aliyuncs.com";
        try {
            return new Client(config);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public Object sendSms(String phone, String url, String service) {
        String templateCode = "SMS_473470022";
        SendSmsResponse response = null;
        Client client = createClient();
        SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setPhoneNumbers(phone)
                .setSignName("Elavatine软件")
                .setTemplateCode(templateCode)
                .setTemplateParam("{\"service\":\"" + service + "\"}");
        RuntimeOptions runtime = new RuntimeOptions();
        try {
            response = client.sendSmsWithOptions(sendSmsRequest, runtime);
        } catch (TeaException error) {
            log.error(error.getMessage());
        } catch (Exception _error) {
            log.error(_error.getMessage());
        }
        return response;
    }
}
