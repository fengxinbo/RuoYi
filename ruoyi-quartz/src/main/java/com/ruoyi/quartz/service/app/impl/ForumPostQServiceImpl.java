package com.ruoyi.quartz.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.quartz.domain.app.ForumPostQ;
import com.ruoyi.quartz.mapper.app.ForumPostQMapper;
import com.ruoyi.quartz.service.app.ForumPostQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class ForumPostQServiceImpl implements ForumPostQService {

    @Autowired
    private ForumPostQMapper forumPostQMapper;

    @Override
    public List<ForumPostQ> selectList() {
        return forumPostQMapper.selectList(null);
    }

    @Override
    public List<ForumPostQ> selectOldPostList() {
        return forumPostQMapper.selectList(new LambdaQueryWrapper<ForumPostQ>().eq(ForumPostQ::getPhase, 2));
    }
}
