package com.ruoyi.quartz.service.app.impl;

import cn.hutool.core.util.RandomUtil;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.quartz.domain.app.ForumPostLikeQ;
import com.ruoyi.quartz.mapper.app.ForumPostLikeQMapper;
import com.ruoyi.quartz.service.app.ForumPostLikeQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@DataSource(DataSourceType.SLAVE)
public class ForumPostLikeQServiceImpl implements ForumPostLikeQService {

    @Autowired
    private ForumPostLikeQMapper forumPostLikeQMapper;

    @Override
    public void dummyLike(Integer bizType, String bizId, Integer likeCount, Long interval) {
        for (int i = 0; i < likeCount; i++) {
            ForumPostLikeQ like = new ForumPostLikeQ();
            like.setBizType(bizType);
            like.setBizId(bizId);
            like.setUid("SU" + RandomUtil.randomString(30).toLowerCase()); // 确保总长度为32
            forumPostLikeQMapper.insert(like);
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
