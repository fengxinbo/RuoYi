package com.ruoyi.quartz.service.app;


import com.ruoyi.quartz.domain.app.AppUserQ;

import java.util.List;

public interface AppUserQService {

    List<AppUserQ> selectEnabledAppUser();

}
