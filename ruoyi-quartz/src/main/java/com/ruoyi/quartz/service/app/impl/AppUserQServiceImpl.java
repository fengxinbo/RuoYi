package com.ruoyi.quartz.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.quartz.domain.app.AppUserQ;
import com.ruoyi.quartz.mapper.app.AppUserQMapper;
import com.ruoyi.quartz.service.app.AppUserQService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@DataSource(DataSourceType.SLAVE)
public class AppUserQServiceImpl implements AppUserQService {

    @Autowired
    private AppUserQMapper appUserQMapper;

    @Override
    public List<AppUserQ> selectEnabledAppUser() {
        return appUserQMapper.selectList(new LambdaQueryWrapper<AppUserQ>().eq(AppUserQ::getState, 1));
    }
}
