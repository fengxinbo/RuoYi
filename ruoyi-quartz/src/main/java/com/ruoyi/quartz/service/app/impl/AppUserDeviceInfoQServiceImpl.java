package com.ruoyi.quartz.service.app.impl;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.quartz.domain.app.AppUserDeviceInfoQ;
import com.ruoyi.quartz.domain.app.UserOperLogQ;
import com.ruoyi.quartz.mapper.app.AppUserDeviceInfoQMapper;
import com.ruoyi.quartz.service.app.IAppUserDeviceInfoQService;
import com.ruoyi.quartz.service.app.IUserOperLogQService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Slf4j
@Service
public class AppUserDeviceInfoQServiceImpl implements IAppUserDeviceInfoQService {

    @Autowired
    private IUserOperLogQService userOperLogQService;

    @Autowired
    private AppUserDeviceInfoQMapper appUserDeviceInfoQMapper;

    @Override
    public void captureAppUserDeviceInfo() {
        List<UserOperLogQ> userOperLogQList = userOperLogQService.selectYesterdayUserOperLog();
        userOperLogQList.stream().forEach(userOperLogQ -> {
            String uid = userOperLogQ.getUid();
            String inputParams = userOperLogQ.getInputparams();
            if (StringUtils.isNotBlank(inputParams)) {
                JSONObject jsonObject = JSONObject.parseObject(inputParams);
                String phoneType = null, flavor = null, phoneOs = null, phoneModel = null;
                if (jsonObject.containsKey("phonetype"))
                    phoneType = jsonObject.getString("phonetype");
                if (jsonObject.containsKey("flavor"))
                    flavor = jsonObject.getString("flavor");
                if (jsonObject.containsKey("phoneos"))
                    phoneOs = jsonObject.getString("phoneos");
                if (jsonObject.containsKey("phonemodel"))
                    phoneModel = jsonObject.getString("phonemodel");
                AppUserDeviceInfoQ appUserDeviceInfoQ = appUserDeviceInfoQMapper.selectById(uid);
                boolean create = false;
                if (appUserDeviceInfoQ == null) {
                    create = true;
                    appUserDeviceInfoQ = new AppUserDeviceInfoQ();
                }

                appUserDeviceInfoQ.setUid(uid);
                if (StringUtils.isNotBlank(phoneType))
                    appUserDeviceInfoQ.setPtype(phoneType.toLowerCase());
                if ("ios".equalsIgnoreCase(phoneType)) {
                    appUserDeviceInfoQ.setFlavor("apple");
                    appUserDeviceInfoQ.setManufacturer("APPLE");
                } else {
                    appUserDeviceInfoQ.setFlavor(flavor);
                    if (StringUtils.isNotBlank(phoneModel)) {
                        String[] parts = phoneModel.split(" ");
                        if (parts.length > 0) {
                            String manufacturer = parts[0];
                            if (StringUtils.isNotBlank(manufacturer))
                                appUserDeviceInfoQ.setManufacturer(manufacturer.toUpperCase());
                        }
                    }
                }
                appUserDeviceInfoQ.setOs(phoneOs);
                appUserDeviceInfoQ.setModel(phoneModel);
                appUserDeviceInfoQ.setEtime(LocalDate.now());

                if (create)
                    appUserDeviceInfoQMapper.insert(appUserDeviceInfoQ);
                else appUserDeviceInfoQMapper.updateById(appUserDeviceInfoQ);
            }
        });
    }

}
