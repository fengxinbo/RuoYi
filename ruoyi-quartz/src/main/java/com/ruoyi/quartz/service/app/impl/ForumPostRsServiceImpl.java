package com.ruoyi.quartz.service.app.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.config.datasource.DynamicDataSourceContextHolder;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.quartz.domain.SysConfigQ;
import com.ruoyi.quartz.domain.app.*;
import com.ruoyi.quartz.mapper.SysConfigQMapper;
import com.ruoyi.quartz.mapper.app.*;
import com.ruoyi.quartz.service.app.ForumPostRsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;

@Slf4j
@Service
public class ForumPostRsServiceImpl implements ForumPostRsService {

    @Autowired
    private SysConfigQMapper sysConfigQMapper;

    @Autowired
    private ForumPostQMapper forumPostQMapper;

    @Autowired
    private ForumPostRsQMapper forumPostRsQMapper;

    @Autowired
    private ForumPostLikeQMapper forumPostLikeQMapper;

    @Autowired
    private ForumPostCommentQMapper forumPostCommentQMapper;

    @Autowired
    private ForumPostCommentReplyQMapper forumPostCommentReplyQMapper;

    @Autowired
    private ForumPostImpressionQMapper forumPostImpressionQMapper;

    @Autowired
    private ForumPostClickQMapper forumPostClickQMapper;

    @Autowired
    private ForumPostShareQMapper forumPostShareQMapper;

    @Autowired
    private AppUserExtQMapper appUserExtQMapper;

    @Autowired
    private ForumPostRankingQMapper forumPostRankingQMapper;

    // ====================== 参数配置 ======================
    // 用户打分权重
    private static final double ALPHA = 1;
    // 点击率、点赞率、评论率、转发率的权重
    private static final double W_CLICK = 1.0;
    private static final double W_LIKE = 2.0;
    private static final double W_COMMENT = 3.0;
    private static final double W_SHARE = 4.0;
    // 为了防止曝光量过小做平滑
    private static final double K = 10.0;

    private Double getTimeScore(LocalDateTime ctime, Double t) {
        Duration duration = Duration.between(ctime, LocalDateTime.now());
        long ageInHours = duration.toHours();
        return Math.exp(-ageInHours / t);
    }

    private Double getLogTransform(Long num) {
        return Math.log(1 + num);
    }

    @Override
    public void updateScore(String postId) {
        DynamicDataSourceContextHolder.setDataSourceType(DataSourceType.MASTER.name());
        SysConfigQ sysConfigQ = sysConfigQMapper.selectById(120);
        if (sysConfigQ == null)
            throw new IllegalArgumentException("系统参数<论坛帖子推荐系统权重设置>不存在");
        String configValue = sysConfigQ.getConfigValue();
        if (StringUtils.isBlank(configValue))
            throw new IllegalArgumentException("系统参数<论坛帖子推荐系统权重设置>不存在");
        JSONObject jsonObject = JSONObject.parseObject(configValue);
        if (!jsonObject.containsKey("t"))
            throw new IllegalArgumentException("系统参数<t>不存在");
        if (!jsonObject.containsKey("wt"))
            throw new IllegalArgumentException("系统参数<wt>不存在");
        if (!jsonObject.containsKey("wu"))
            throw new IllegalArgumentException("系统参数<wu>不存在");
        if (!jsonObject.containsKey("wc"))
            throw new IllegalArgumentException("系统参数<wc>不存在");
        Double t = jsonObject.getDouble("t");
        Double wt = jsonObject.getDouble("wt");
        Double wu = jsonObject.getDouble("wu");
        Double wc = jsonObject.getDouble("wc");

        DynamicDataSourceContextHolder.setDataSourceType(DataSourceType.SLAVE.name());
        ForumPostQ forumPostQ = forumPostQMapper.selectById(postId);
        if (forumPostQ != null) {
            Double timeScore = getTimeScore(forumPostQ.getCtime(), t);

            Long likeCount = forumPostLikeQMapper.selectCount(new LambdaQueryWrapper<ForumPostLikeQ>().eq(ForumPostLikeQ::getBizType, 1).eq(ForumPostLikeQ::getBizId, postId).eq(ForumPostLikeQ::getLikeStatus, 1));
            Double likeScore = getLogTransform(likeCount);

            Long commentCount = forumPostCommentQMapper.selectCount(new LambdaQueryWrapper<ForumPostCommentQ>().eq(ForumPostCommentQ::getPostId, postId)) + forumPostCommentReplyQMapper.selectCount(new LambdaQueryWrapper<ForumPostCommentReplyQ>().eq(ForumPostCommentReplyQ::getPostId, postId));
            Double commentScore = getLogTransform(commentCount);

            Double score = wt * timeScore + wu * likeScore + wc * commentScore;

            ForumPostRsQ forumPostRsQ = forumPostRsQMapper.selectById(postId);
            if (forumPostRsQ == null) {
                // 新增
                forumPostRsQ = new ForumPostRsQ();
                forumPostRsQ.setId(postId);
                forumPostRsQ.setWt(wt);
                forumPostRsQ.setTimeScore(timeScore);
                forumPostRsQ.setWu(wu);
                forumPostRsQ.setLikeScore(likeScore);
                forumPostRsQ.setWc(wc);
                forumPostRsQ.setCommentScore(commentScore);
                forumPostRsQ.setScore(score);
                forumPostRsQMapper.insert(forumPostRsQ);
            } else {
                // 更新
                forumPostRsQ.setWt(wt);
                forumPostRsQ.setTimeScore(timeScore);
                forumPostRsQ.setWu(wu);
                forumPostRsQ.setLikeScore(likeScore);
                forumPostRsQ.setWc(wc);
                forumPostRsQ.setCommentScore(commentScore);
                forumPostRsQ.setScore(score);
                forumPostRsQ.setEtime(LocalDateTime.now());
                forumPostRsQMapper.updateById(forumPostRsQ);
            }
        }
        DynamicDataSourceContextHolder.clearDataSourceType();
    }

    @Override
    public void updateOldPostScore(String postId) {
        DynamicDataSourceContextHolder.setDataSourceType(DataSourceType.SLAVE.name());
        // 查询出发帖人的forumAuthorRating
        ForumPostQ post = forumPostQMapper.selectById(postId);
        if (post == null)
            throw new IllegalArgumentException("帖子不存在");
        AppUserExtQ userExt = appUserExtQMapper.selectById(post.getUid());
        if (userExt == null)
            throw new IllegalArgumentException("用户不存在");
        double forumAuthorRating = userExt.getForumAuthorRating();
        Long impressionCount = forumPostImpressionQMapper.selectCount(new LambdaQueryWrapper<ForumPostImpressionQ>().eq(ForumPostImpressionQ::getPostId, postId));
        Long clickCount = forumPostClickQMapper.selectCount(new LambdaQueryWrapper<ForumPostClickQ>().eq(ForumPostClickQ::getPostId, postId));
        Long likeCount = forumPostLikeQMapper.selectCount(new LambdaQueryWrapper<ForumPostLikeQ>().eq(ForumPostLikeQ::getBizType, 1).eq(ForumPostLikeQ::getBizId, postId).eq(ForumPostLikeQ::getLikeStatus, 1));
        Long commentCount = forumPostCommentQMapper.selectCount(new LambdaQueryWrapper<ForumPostCommentQ>().eq(ForumPostCommentQ::getPostId, postId)) + forumPostCommentReplyQMapper.selectCount(new LambdaQueryWrapper<ForumPostCommentReplyQ>().eq(ForumPostCommentReplyQ::getPostId, postId));
        Long shareCount = forumPostShareQMapper.selectCount(new LambdaQueryWrapper<ForumPostShareQ>().eq(ForumPostShareQ::getPostId, postId));
        double baseScore = computeOldPostScore(forumAuthorRating, impressionCount, clickCount, likeCount, commentCount, shareCount);
        log.debug("帖子 {} 旧帖综合打分: {}", postId, baseScore);
        ForumPostRankingQ forumPostRankingQ = forumPostRankingQMapper.selectById(postId);
        if (forumPostRankingQ == null) {
            // 新增
            forumPostRankingQ = new ForumPostRankingQ();
            forumPostRankingQ.setId(postId);
            forumPostRankingQ.setScore(baseScore);
            forumPostRankingQMapper.insert(forumPostRankingQ);
        } else {
            // 更新
            forumPostRankingQ.setScore(baseScore);
            forumPostRankingQ.setEtime(LocalDateTime.now());
            forumPostRankingQMapper.updateById(forumPostRankingQ);
        }
        DynamicDataSourceContextHolder.clearDataSourceType();
    }

    // ====================== 打分计算逻辑 ======================

    /**
     * 计算旧帖的综合打分:
     * old_post_score = ALPHA * forumAuthorRating
     * + W_CLICK   * clickRate
     * + W_LIKE    * likeRate
     * + W_COMMENT * commentRate
     * + W_SHARE   * shareRate
     * <p>
     * 其中:
     * clickRate    = (clickCount + 1) / (impressionCount + K)
     * likeRate     = (likeCount + 1) / (impressionCount + K)
     * commentRate  = (commentCount + 1) / (impressionCount + K)
     * shareRate    = (shareCount + 1) / (impressionCount + K)
     */
    private static double computeOldPostScore(double forumAuthorRating, long impressionCount, long clickCount, long likeCount, long commentCount, long shareCount) {
        double denominator = impressionCount + K; // 防止除0或曝光量太小不稳定
        double clickRate = (clickCount + 1.0) / denominator;
        double likeRate = (likeCount + 1.0) / denominator;
        double commentRate = (commentCount + 1.0) / denominator;
        double shareRate = (shareCount + 1.0) / denominator;

        // 线性加权
        double baseScore =
                ALPHA * forumAuthorRating +
                        W_CLICK * clickRate +
                        W_LIKE * likeRate +
                        W_COMMENT * commentRate +
                        W_SHARE * shareRate;
        return baseScore;
    }
}
