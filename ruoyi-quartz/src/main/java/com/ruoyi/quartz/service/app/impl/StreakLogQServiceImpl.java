package com.ruoyi.quartz.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.quartz.domain.app.StreakLogQ;
import com.ruoyi.quartz.mapper.app.StreakLogQMapper;
import com.ruoyi.quartz.service.app.IStreakLogQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@DataSource(DataSourceType.SLAVE)
public class StreakLogQServiceImpl implements IStreakLogQService {

    @Autowired
    private StreakLogQMapper streakLogQMapper;

    @Override
    public StreakLogQ selectByUidCtime(String uid, LocalDate ctime) {
        return streakLogQMapper.selectOne(new LambdaQueryWrapper<StreakLogQ>().eq(StreakLogQ::getUid, uid).eq(StreakLogQ::getCtime, ctime));
    }

    @Override
    public int insert(StreakLogQ streakLogQ) {
        return streakLogQMapper.insert(streakLogQ);
    }
}
