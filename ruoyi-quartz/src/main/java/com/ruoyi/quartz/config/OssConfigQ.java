package com.ruoyi.quartz.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 极光推送配置类
 *
 * @author fengxinbo
 */
@Component
@ConfigurationProperties(prefix = "oss")
public class OssConfigQ {

    private static String endpoint;

    private static String accessKeyId;

    private static String accessKeySecret;

    private static String bucketName;

    private static String domain;

    public static String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        OssConfigQ.endpoint = endpoint;
    }

    public static String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        OssConfigQ.accessKeyId = accessKeyId;
    }

    public static String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        OssConfigQ.accessKeySecret = accessKeySecret;
    }

    public static String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        OssConfigQ.bucketName = bucketName;
    }

    public static String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        OssConfigQ.domain = domain;
    }
}
