package com.ruoyi.quartz.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 邮件配置类
 *
 * @author fengxinbo
 */
@Component
@ConfigurationProperties(prefix = "email")
public class EmailConfigQ {

    private static String host;

    private static Integer port;

    private static String from;

    private static String user;

    private static String pass;

    private static String to;

    public static String getHost() {
        return host;
    }

    public void setHost(String host) {
        EmailConfigQ.host = host;
    }

    public static Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        EmailConfigQ.port = port;
    }

    public static String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        EmailConfigQ.from = from;
    }

    public static String getUser() {
        return user;
    }

    public void setUser(String user) {
        EmailConfigQ.user = user;
    }

    public static String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        EmailConfigQ.pass = pass;
    }

    public static String getTo() {
        return to;
    }

    public void setTo(String to) {
        EmailConfigQ.to = to;
    }
}
