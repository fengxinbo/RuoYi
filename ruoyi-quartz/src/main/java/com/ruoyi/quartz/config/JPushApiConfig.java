package com.ruoyi.quartz.config;

import cn.jiguang.sdk.api.PushApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JPushApiConfig {

    @Bean
    public PushApi pushApi() {
        return new PushApi.Builder()
                .setAppKey(JPushConfig.getAppKey())
                .setMasterSecret(JPushConfig.getMasterSecret())
                .build();
    }

}