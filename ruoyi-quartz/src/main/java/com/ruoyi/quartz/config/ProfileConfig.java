package com.ruoyi.quartz.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class ProfileConfig {

    @Value("${spring.profiles.active}")
    private String activeProfile;

}
