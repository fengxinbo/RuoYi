package com.ruoyi.quartz.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 极光推送配置类
 *
 * @author fengxinbo
 */
@Component
@ConfigurationProperties(prefix = "jpush")
public class JPushConfig {

    /**
     * 应用key
     */
    private static String appKey;

    /**
     * 应用secret
     */
    private static String masterSecret;

    public static String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        JPushConfig.appKey = appKey;
    }

    public static String getMasterSecret() {
        return masterSecret;
    }

    public void setMasterSecret(String masterSecret) {
        JPushConfig.masterSecret = masterSecret;
    }
}
