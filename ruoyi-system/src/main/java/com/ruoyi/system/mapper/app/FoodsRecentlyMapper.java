package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.FoodsRecently;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FoodsRecentlyMapper extends BaseMapper<FoodsRecently> {
}
