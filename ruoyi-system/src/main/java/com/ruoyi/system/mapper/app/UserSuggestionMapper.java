package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.UserSuggestion;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserSuggestionMapper extends BaseMapper<UserSuggestion> {
    List<UserSuggestion> selectUserSuggestion(UserSuggestion userSuggestion);
}
