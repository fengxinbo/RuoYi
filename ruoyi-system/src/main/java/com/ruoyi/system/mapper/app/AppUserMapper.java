package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.AppUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface AppUserMapper extends BaseMapper<AppUser> {

    List<Map<String, Object>> selectIndexAge();

    List<Map<String, Object>> selectIndexDNU();

    List<Map<String, Object>> selectIndexGenderData();

    List<AppUser> selectUserList(AppUser appUser);

    AppUser selectDummyUserByUid(String uid);

    List<AppUser> selectOfficialUserList();

}
