package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.ForumTutorialMenu2;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ForumTutorialMenu2Mapper extends BaseMapper<ForumTutorialMenu2> {}