package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.UserFission;
import com.ruoyi.system.domain.app.UserFissionStatistics;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserFissionMapper extends BaseMapper<UserFission> {

    List<UserFission> selectUserFission(UserFission userFission);

    List<UserFissionStatistics> selectStatistics(UserFissionStatistics userFissionStatistics);

}
