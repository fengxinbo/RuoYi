package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.AccountDeletionLog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AccountDeletionLogMapper extends BaseMapper<AccountDeletionLog> {

    List<AccountDeletionLog> selectAccountDeletionLogList(AccountDeletionLog accountDeletionLog);

}
