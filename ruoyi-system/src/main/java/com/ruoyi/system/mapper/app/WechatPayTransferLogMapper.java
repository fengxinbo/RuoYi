package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.WechatPayTransferLog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface WechatPayTransferLogMapper extends BaseMapper<WechatPayTransferLog> {
    List<WechatPayTransferLog> selectTransferLog(WechatPayTransferLog wechatPayTransferLog);
}
