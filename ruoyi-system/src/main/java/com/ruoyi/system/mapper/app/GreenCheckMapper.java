package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.GreenCheck;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GreenCheckMapper extends BaseMapper<GreenCheck> {
    List<GreenCheck> selectGreenCheckList(GreenCheck greenCheck);
}