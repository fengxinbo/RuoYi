package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.ForumPost;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ForumPostMapper extends BaseMapper<ForumPost> {
    List<ForumPost> selectPostList(ForumPost forumPost);
}