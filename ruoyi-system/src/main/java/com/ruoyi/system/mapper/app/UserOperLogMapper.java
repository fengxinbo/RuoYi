package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.UserOperLog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserOperLogMapper extends BaseMapper<UserOperLog> {

    List<UserOperLog> selectUserOperLog(UserOperLog userOperLog);

}
