package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.AppUserOnline;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface AppUserOnlineMapper extends BaseMapper<AppUserOnline> {
    List<Map<String, Object>> selectIndexOL();
}