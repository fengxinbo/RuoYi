package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.Foods;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FoodsMapper extends BaseMapper<Foods> {

    List<Foods> selectFoods(Foods foods);

}
