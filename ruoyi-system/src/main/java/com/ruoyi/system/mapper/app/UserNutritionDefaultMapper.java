package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.UserNutritionDefault;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserNutritionDefaultMapper extends BaseMapper<UserNutritionDefault> {}