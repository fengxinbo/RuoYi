package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.DietPlanMeals;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DietPlanMealsMapper extends BaseMapper<DietPlanMeals> {
}
