package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.UserInvitationLog;
import com.ruoyi.system.domain.app.UserInvitationLogStatistics;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserInvitationLogMapper extends BaseMapper<UserInvitationLog> {

    List<UserInvitationLog> selectUserInvitationLog(UserInvitationLog userInvitationLog);

    List<UserInvitationLogStatistics> selectStatistics(UserInvitationLogStatistics userInvitationLogStatistics);

}
