package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.ForumPostClick;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ForumPostClickMapper extends BaseMapper<ForumPostClick> {}