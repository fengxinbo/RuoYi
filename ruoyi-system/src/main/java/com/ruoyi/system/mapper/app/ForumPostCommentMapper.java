package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.ForumPostComment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ForumPostCommentMapper extends BaseMapper<ForumPostComment> {
    List<ForumPostComment> selectCommentList(@Param("criteria") Map<String, Object> criteria);
}