package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.ForumTutorialMenu1;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ForumTutorialMenu1Mapper extends BaseMapper<ForumTutorialMenu1> {}