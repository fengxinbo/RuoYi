package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.SportLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SportLogMapper extends BaseMapper<SportLog> {}