package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.ForumPostCommentReply;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ForumPostCommentReplyMapper extends BaseMapper<ForumPostCommentReply> {
    List<ForumPostCommentReply> selectReplyList(@Param("criteria") Map<String, Object> criteria);
}