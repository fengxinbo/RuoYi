package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.ForumPostReport;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ForumPostReportMapper extends BaseMapper<ForumPostReport> {
    List<ForumPostReport> selectReportList(ForumPostReport forumPostReport);
}