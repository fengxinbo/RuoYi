package com.ruoyi.system.mapper.app;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.app.JPushAlert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface JPushAlertMapper extends BaseMapper<JPushAlert> {
}
