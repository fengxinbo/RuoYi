package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.UserFission;
import com.ruoyi.system.domain.app.UserFissionStatistics;

import java.util.List;

public interface IUserFissionService {

    List<UserFission> selectUserFission(UserFission userFission);

    List<UserFissionStatistics> selectStatistics(UserFissionStatistics userFissionStatistics);

}
