package com.ruoyi.system.service.app.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.system.mapper.app.AppUserDeviceInfoMapper;
import com.ruoyi.system.service.app.IAppUserDeviceInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppUserDeviceInfoServiceImpl implements IAppUserDeviceInfoService {

    @Autowired
    private AppUserDeviceInfoMapper appUserDeviceInfoMapper;

    @Override
    public String getPhoneBrandPie() {
        JSONObject seriesObj = new JSONObject();
        seriesObj.put("type", "pie");
        seriesObj.put("radius", "55%");
        seriesObj.put("data", appUserDeviceInfoMapper.selectIndexPhoneBrandData());

        JSONObject option = new JSONObject();
        option.put("title", new JSONObject() {
            {
                put("text", "用户手机品牌占比");
            }
        });
        option.put("series", new JSONArray() {
            {
                add(seriesObj);
            }
        });
        option.put("tooltip", new JSONObject() {
            {
                put("trigger", "item");
            }
        });

        return option.toJSONString();
    }
}
