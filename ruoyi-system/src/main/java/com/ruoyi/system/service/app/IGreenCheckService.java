package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.GreenCheck;

import java.util.List;

public interface IGreenCheckService {
    List<GreenCheck> selectGreenCheckList(GreenCheck greenCheck);

    int disposalRisk(String id, String ossUrl);

    int ignoreRisk(String id);
}
