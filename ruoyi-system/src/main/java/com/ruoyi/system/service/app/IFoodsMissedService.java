package com.ruoyi.system.service.app;

import java.util.List;
import java.util.Map;

public interface IFoodsMissedService {
    List<Map<String, Object>> selectIndexMissedFoods();

    int resolveMissedFood(String fname);
}
