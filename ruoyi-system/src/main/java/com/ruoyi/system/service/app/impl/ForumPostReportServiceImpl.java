package com.ruoyi.system.service.app.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.config.datasource.DynamicDataSourceContextHolder;
import com.ruoyi.common.constant.AppConstants;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.system.domain.app.*;
import com.ruoyi.system.mapper.app.*;
import com.ruoyi.system.service.app.ForumPostMsgService;
import com.ruoyi.system.service.app.ForumPostReportService;
import com.ruoyi.system.service.app.ForumPostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@DataSource(DataSourceType.SLAVE)
public class ForumPostReportServiceImpl implements ForumPostReportService {

    @Autowired
    private ForumPostReportMapper forumPostReportMapper;

    @Autowired
    private ForumPostService forumPostService;

    @Autowired
    private ForumPostMapper forumPostMapper;

    @Autowired
    private ForumPostCommentMapper forumPostCommentMapper;

    @Autowired
    private ForumPostCommentReplyMapper forumPostCommentReplyMapper;

    @Autowired
    private ForumPostMsgService forumPostMsgService;

    @Autowired
    private AppUserExtMapper appUserExtMapper;

    @Autowired
    private AppUserMapper appUserMapper;

    @Override
    public List<ForumPostReport> list(ForumPostReport forumPostReport) {
        return forumPostReportMapper.selectReportList(forumPostReport);
    }

    @Override
    public int ignore(String id) {
        ForumPostReport report = forumPostReportMapper.selectById(id);
        if (report == null)
            return 0;
        report.setStatus(AppConstants.APP_FORUM_POST_REPORT_STATUS_PROCESSED);
        return forumPostReportMapper.updateById(report);
    }

    @Override
    public ForumPostReport selectById(String id) {
        return forumPostReportMapper.selectById(id);
    }

    @Override
    @Transactional
    public int process(ForumPostReportProcess process) {
        Integer contentProcessType = process.getContentProcessType();
        Integer bizType = process.getBizType();
        String bizTypeStr = process.getBizTypeStr();
        String violations = null;
        String toViolationUid = null;
        if (bizType == 1) {
            // 帖子
            ForumPost post = forumPostMapper.selectById(process.getBizId());
            if (post == null)
                throw new ServiceException("被举报帖子不存在");
            violations = post.getTitle();
            toViolationUid = post.getUid();
            if (contentProcessType == 1) {
                // 帖子可见范围降级
                post.setVisibleScope(1);
                forumPostMapper.updateById(post);
            } else if (contentProcessType == 2) {
                // 帖子删除
                forumPostService.delete(process.getBizId());
            } else throw new ServiceException("未知的处理方式");
        } else if (bizType == 2) {
            // 评论
            ForumPostComment comment = forumPostCommentMapper.selectById(process.getBizId());
            if (comment == null)
                throw new ServiceException("被举报评论不存在");
            violations = comment.getContent();
            toViolationUid = comment.getFromUid();
            if (contentProcessType == 1) {
                // 评论可见范围降级
                comment.setVisibleScope(1);
                forumPostCommentMapper.updateById(comment);
            } else if (contentProcessType == 2) {
                // 评论删除
                forumPostService.deleteComment(process.getBizId());
            } else throw new ServiceException("未知的处理方式");
        } else if (bizType == 3) {
            // 回复
            ForumPostCommentReply reply = forumPostCommentReplyMapper.selectById(process.getBizId());
            if (reply == null)
                throw new ServiceException("被举报回复不存在");
            violations = reply.getContent();
            toViolationUid = reply.getFromUid();
            if (contentProcessType == 1) {
                // 回复可见范围降级
                reply.setVisibleScope(1);
                forumPostCommentReplyMapper.updateById(reply);
            } else if (contentProcessType == 2) {
                // 回复删除
                forumPostService.deleteReplyOnAccountDeletion(process.getBizId());
            } else throw new ServiceException("未知的处理方式");
        } else throw new ServiceException("未知的业务类型");

        Integer userProcessType = process.getUserProcessType();
        if (userProcessType == 1) {
            // 给违规用户发送站内信警告通知
            ForumPostMsg msg = new ForumPostMsg();
            msg.setType(4);
            msg.setFromUid("1");
            msg.setToUid(toViolationUid);
            msg.setTitle("系统通知");
            msg.setContent(String.format("经核实，你发布的%s“%s”存在违规，多次违规可能会影响你未来正常使用社区功能。如有疑问请参考社区“用户守则”，或是联系Elavatine工作人员。", bizTypeStr, violations));
            forumPostMsgService.insert(msg);
        } else if (userProcessType == 2) {
            // 用户禁言
            AppUserExt userExt = appUserExtMapper.selectById(toViolationUid);
            if (userExt == null)
                throw new ServiceException("被举报用户不存在");
            userExt.setForumPostVisibleScope(1);
            userExt.setForumCommentVisibleScope(1);
            appUserExtMapper.updateById(userExt);
        } else if (userProcessType == 3) {
            // 用户封禁
            AppUser user = appUserMapper.selectById(toViolationUid);
            if (user == null)
                throw new ServiceException("被举报用户不存在");
            user.setState(0);
            user.setToken(null);
            appUserMapper.updateById(user);
        } else log.info("后台管理员没有对违规用户采取任何处理方案");

        DynamicDataSourceContextHolder.setDataSourceType(DataSourceType.SLAVE.name());
        ForumPostReport report = forumPostReportMapper.selectById(process.getId());
        if (report == null)
            throw new ServiceException("举报记录不存在");
        // 通知举报用户处理结果
        ForumPostMsg msg = new ForumPostMsg();
        msg.setType(4);
        msg.setFromUid("1");
        msg.setToUid(report.getUid());
        msg.setTitle("举报结果通知");
        msg.setContent(process.getContent());
        forumPostMsgService.insert(msg);
        // 更新举报记录状态
        report.setStatus(1);
        DynamicDataSourceContextHolder.setDataSourceType(DataSourceType.SLAVE.name());
        int affectedRows = forumPostReportMapper.updateById(report);
        DynamicDataSourceContextHolder.clearDataSourceType();
        return affectedRows;
    }
}
