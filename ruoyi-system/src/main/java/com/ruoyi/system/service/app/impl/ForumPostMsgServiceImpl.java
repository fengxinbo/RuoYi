package com.ruoyi.system.service.app.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.app.ForumPostMsg;
import com.ruoyi.system.mapper.app.ForumPostMsgMapper;
import com.ruoyi.system.service.app.ForumPostMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@DataSource(DataSourceType.SLAVE)
public class ForumPostMsgServiceImpl implements ForumPostMsgService {

    @Autowired
    private ForumPostMsgMapper forumPostMsgMapper;

    @Override
    public int insert(ForumPostMsg msg) {
        return forumPostMsgMapper.insert(msg);
    }
}
