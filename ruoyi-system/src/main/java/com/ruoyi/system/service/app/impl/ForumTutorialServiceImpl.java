package com.ruoyi.system.service.app.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.config.OssConfig;
import com.ruoyi.system.domain.app.ForumPostClick;
import com.ruoyi.system.domain.app.ForumTutorial;
import com.ruoyi.system.domain.app.ForumTutorialMenu2;
import com.ruoyi.system.domain.app.MaterialItem;
import com.ruoyi.system.mapper.app.ForumPostClickMapper;
import com.ruoyi.system.mapper.app.ForumTutorialMapper;
import com.ruoyi.system.mapper.app.ForumTutorialMenu2Mapper;
import com.ruoyi.system.service.app.ForumTutorialService;
import com.ruoyi.system.service.app.IOssService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@DataSource(DataSourceType.SLAVE)
public class ForumTutorialServiceImpl implements ForumTutorialService {

    @Autowired
    private ForumTutorialMapper forumTutorialMapper;

    @Autowired
    private ForumTutorialMenu2Mapper forumTutorialMenu2Mapper;

    @Autowired
    private ForumPostClickMapper forumPostClickMapper;

    @Autowired
    private IOssService ossService;

    private static final String FORUM_POST_HTML_FILE_PATH = "forum/post/html/";

    @Override
    public List<ForumTutorial> list(String parentId) {
        List<ForumTutorial> list = forumTutorialMapper.selectList(new LambdaQueryWrapper<ForumTutorial>().eq(ForumTutorial::getParentId, parentId).orderByAsc(ForumTutorial::getSn));
        list.stream().forEach(tutorial -> {
            tutorial.setClickCount(forumPostClickMapper.selectCount(new LambdaQueryWrapper<ForumPostClick>().eq(ForumPostClick::getPostId, tutorial.getId())));
            if (tutorial.getMaterial() != null)
                if (tutorial.getMaterial().getVideo() != null)
                    if (tutorial.getMaterial().getVideo().size() > 0)
                        tutorial.setOssUrl(tutorial.getMaterial().getVideo().get(0).getOssUrl());
        });
        return list;
    }

    @Override
    @Transactional
    public int add(ForumTutorial tutorial) {
        int affectedRows = forumTutorialMapper.insert(tutorial);
        createHtmlFile(tutorial);
        return affectedRows;
    }

    private void createHtmlFile(ForumTutorial tutorial) {
        ForumTutorialMenu2 menu2 = forumTutorialMenu2Mapper.selectById(tutorial.getParentId());

        List<ForumTutorialMenu2> menuList = forumTutorialMenu2Mapper.selectList(new LambdaQueryWrapper<ForumTutorialMenu2>().eq(ForumTutorialMenu2::getParentId, menu2.getParentId()).orderByAsc(ForumTutorialMenu2::getSn).select(ForumTutorialMenu2::getId, ForumTutorialMenu2::getTitle).last("limit 4"));
        List<ForumTutorial> tutorialList = null;
        if (!menuList.isEmpty()) {
            tutorialList = forumTutorialMapper.selectList(new LambdaQueryWrapper<ForumTutorial>().eq(ForumTutorial::getParentId, menuList.get(0).getId()).select(ForumTutorial::getTitle, ForumTutorial::getSubtitle).orderByAsc(ForumTutorial::getSn));
        }

        Map<String, Object> data = new HashMap<>();
        data.put("tutorialTitle", tutorial.getTitle());
        data.put("tutorialSubtitle", tutorial.getSubtitle());
        data.put("material", JSON.toJSONString(tutorial.getMaterial()));
        data.put("menuList", JSON.toJSONString(menuList));
        data.put("tutorialList", JSON.toJSONString(tutorialList));
        data.put("postId", tutorial.getId());

        try {
            // 读取模板文件
            File templateFile = new File(RuoYiConfig.getUploadPath() + "/tutorial_share_template.html");
            Document doc = Jsoup.parse(templateFile, "UTF-8");

            // 替换占位符
            for (Map.Entry<String, Object> entry : data.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                // 替换文本内容
                Elements elementsWithText = doc.select("[id=" + key + "]");
                for (Element element : elementsWithText) {
                    element.text(value.toString());
                }

                // 替换属性内容
                Elements elementsWithAttr = doc.select("[src={{" + key + "}}]");
                for (Element element : elementsWithAttr) {
                    element.attr("src", value.toString());
                }

                // 替换 JavaScript 中的占位符
                Elements scriptElements = doc.select("script");
                for (Element scriptElement : scriptElements) {
                    String scriptContent = scriptElement.html();
                    scriptContent = scriptContent.replace("{{" + key + "}}", value.toString());
                    scriptElement.html(scriptContent);
                }
            }
            doc.outputSettings().prettyPrint(true);

            String filePath = RuoYiConfig.getUploadPath() + "/" + tutorial.getId() + ".html";
            try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), "UTF-8"))) {
                writer.write(doc.html());
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (ossService.putForumPostHtmlFile(FORUM_POST_HTML_FILE_PATH, filePath)) {
                tutorial.setWeblink(OssConfig.getDomain() + FORUM_POST_HTML_FILE_PATH + tutorial.getId() + ".html");
                tutorial.setEtime(LocalDateTime.now());
                forumTutorialMapper.updateById(tutorial);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int update(ForumTutorial forumTutorial) {
        createHtmlFile(forumTutorial);
        return forumTutorialMapper.updateById(forumTutorial);
    }

    @Override
    @Transactional
    public int delete(String id) {
        ForumTutorial tutorial = selectById(id);
        if (tutorial.getMaterial() != null) {
            List<MaterialItem> image = tutorial.getMaterial().getImage();
            for (MaterialItem item : image) {
                ossService.deleteObject(item.getOssUrl());
            }
            List<MaterialItem> video = tutorial.getMaterial().getVideo();
            for (MaterialItem item : video) {
                ossService.deleteObject(item.getOssUrl());
            }
        }
        if (StringUtils.isNotBlank(tutorial.getWeblink()))
            ossService.deleteObject(tutorial.getWeblink());
        return forumTutorialMapper.deleteById(tutorial);
    }

    @Override
    public ForumTutorial selectById(String id) {
        return forumTutorialMapper.selectById(id);
    }

}