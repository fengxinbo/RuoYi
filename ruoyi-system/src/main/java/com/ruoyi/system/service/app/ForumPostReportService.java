package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.ForumPostReport;
import com.ruoyi.system.domain.app.ForumPostReportProcess;

import java.util.List;

public interface ForumPostReportService {

    List<ForumPostReport> list(ForumPostReport forumPostReport);

    int ignore(String id);

    ForumPostReport selectById(String id);

    int process(ForumPostReportProcess process);

}