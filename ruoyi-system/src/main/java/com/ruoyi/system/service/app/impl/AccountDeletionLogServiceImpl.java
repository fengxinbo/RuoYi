package com.ruoyi.system.service.app.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.config.datasource.DynamicDataSourceContextHolder;
import com.ruoyi.common.constant.AppConstants;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.app.JsonValidator;
import com.ruoyi.system.domain.app.*;
import com.ruoyi.system.mapper.app.*;
import com.ruoyi.system.service.app.IAccountDeletionLogService;
import com.ruoyi.system.service.app.ForumPostService;
import com.ruoyi.system.service.app.IOssService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@DataSource(DataSourceType.SLAVE)
public class AccountDeletionLogServiceImpl implements IAccountDeletionLogService {

    @Autowired
    private IOssService ossService;
    @Autowired
    private AccountDeletionLogMapper accountDeletionLogMapper;
    @Autowired
    private AiIdentifyImageLogMapper aiIdentifyImageLogMapper;
    @Autowired
    private DAUMapper dauMapper;
    @Autowired
    private DietLogMapper dietLogMapper;
    @Autowired
    private DietLogArchiveMapper dietLogArchiveMapper;
    @Autowired
    private DietLogFoodsMapper dietLogFoodsMapper;
    @Autowired
    private DietLogFoodsArchiveMapper dietLogFoodsArchiveMapper;
    @Autowired
    private DietLogWaterMapper dietLogWaterMapper;
    @Autowired
    private DietPlanMapper dietPlanMapper;
    @Autowired
    private DietPlanArchiveMapper dietPlanArchiveMapper;
    @Autowired
    private DietPlanDetailsMapper dietPlanDetailsMapper;
    @Autowired
    private DietPlanDetailsArchiveMapper dietPlanDetailsArchiveMapper;
    @Autowired
    private DietPlanMealsMapper dietPlanMealsMapper;
    @Autowired
    private DietPlanMealsArchiveMapper dietPlanMealsArchiveMapper;
    @Autowired
    private FoodsMapper foodsMapper;
    @Autowired
    private FoodsFavoriteMapper foodsFavoriteMapper;
    @Autowired
    private FoodsRecentlyMapper foodsRecentlyMapper;
    @Autowired
    private ForumPostService forumPostService;
    @Autowired
    private ForumPostMapper forumPostMapper;
    @Autowired
    private ForumPostAuthorDislikeMapper forumPostAuthorDislikeMapper;
    @Autowired
    private ForumPostClickMapper forumPostClickMapper;
    @Autowired
    private ForumPostCollectMapper forumPostCollectMapper;
    @Autowired
    private ForumPostCommentMapper forumPostCommentMapper;
    @Autowired
    private ForumPostCommentReplyMapper forumPostCommentReplyMapper;
    @Autowired
    private ForumPostDislikeMapper forumPostDislikeMapper;
    @Autowired
    private ForumPostImpressionMapper forumPostImpressionMapper;
    @Autowired
    private ForumPostLikeMapper forumPostLikeMapper;
    @Autowired
    private ForumPostMsgMapper forumPostMsgMapper;
    @Autowired
    private ForumPostPollDataMapper forumPostPollDataMapper;
    @Autowired
    private ForumPostReportMapper forumPostReportMapper;
    @Autowired
    private ForumPostShareMapper forumPostShareMapper;
    @Autowired
    private ForumUserBlockMapper forumUserBlockMapper;
    @Autowired
    private ForumUserFollowMapper forumUserFollowMapper;
    @Autowired
    private GreenCheckMapper greenCheckMapper;
    @Autowired
    private MealsMapper mealsMapper;
    @Autowired
    private MealsFoodsMapper mealsFoodsMapper;
    @Autowired
    private SmsCodeTempMapper smsCodeTempMapper;
    @Autowired
    private SportLogMapper sportLogMapper;
    @Autowired
    private SportRecentlyMapper sportRecentlyMapper;
    @Autowired
    private SportUserMapper sportUserMapper;
    @Autowired
    private StreakMapper streakMapper;
    @Autowired
    private StreakLogMapper streakLogMapper;
    @Autowired
    private SurveyMapper surveyMapper;
    @Autowired
    private UserBodyStatLogMapper userBodyStatLogMapper;
    @Autowired
    private UserNutGoalCcMapper userNutGoalCcMapper;
    @Autowired
    private UserNutritionDefaultMapper userNutritionDefaultMapper;
    @Autowired
    private UserOperLogMapper userOperLogMapper;
    @Autowired
    private UserSuggestionMapper userSuggestionMapper;
    @Autowired
    private UserWxInfoMapper userWxInfoMapper;
    @Autowired
    private AppUserMapper appUserMapper;
    @Autowired
    private AppUserExtMapper appUserExtMapper;

    @Override
    public List<AccountDeletionLog> selectAccountDeletionLogList(AccountDeletionLog accountDeletionLog) {
        return accountDeletionLogMapper.selectAccountDeletionLogList(accountDeletionLog);
    }

    @Override
    @Transactional
    public int purgeData(String id) {
        if (StringUtils.isNotBlank(id)) {
            AccountDeletionLog log = accountDeletionLogMapper.selectById(id);
            if (log != null) {
                String uid = log.getUid();
                AppUser user = appUserMapper.selectById(uid);
                if (user.getState() == AppConstants.APP_USER_STATE_DISABLED) {
                    new Thread(() -> {
                        DynamicDataSourceContextHolder.setDataSourceType(DataSourceType.SLAVE.name());

                        // 清除用户AI识图数据
                        aiIdentifyImageLogMapper.delete(new LambdaQueryWrapper<AiIdentifyImageLog>().eq(AiIdentifyImageLog::getUid, uid));

                        // 清除用户日活数据
                        dauMapper.delete(new LambdaQueryWrapper<DAU>().eq(DAU::getUid, uid));

                        // 清除用户日志数据
                        List<DietLog> dietLogs = dietLogMapper.selectList(new LambdaQueryWrapper<DietLog>().eq(DietLog::getUid, uid));
                        dietLogs.stream().forEach(dietLog -> {
                            String pid = dietLog.getId();
                            // 清除用户饮食日志食物数据
                            dietLogFoodsMapper.delete(new LambdaQueryWrapper<DietLogFoods>().eq(DietLogFoods::getPid, pid));
                            // 清除用户饮食日志水数据
                            dietLogWaterMapper.delete(new LambdaQueryWrapper<DietLogWater>().eq(DietLogWater::getPid, pid));
                            dietLogMapper.deleteById(pid);
                        });

                        // 清除用户归档日志数据
                        List<DietLogArchive> dietLogArchives = dietLogArchiveMapper.selectList(new LambdaQueryWrapper<DietLogArchive>().eq(DietLogArchive::getUid, uid));
                        dietLogArchives.stream().forEach(dietLogArchive -> {
                            String pid = dietLogArchive.getId();
                            // 清除用户饮食日志食物数据
                            dietLogFoodsArchiveMapper.delete(new LambdaQueryWrapper<DietLogFoodsArchive>().eq(DietLogFoodsArchive::getPid, pid));
                            dietLogArchiveMapper.deleteById(pid);
                        });

                        // 清除用户计划数据
                        List<DietPlan> dietPlans = dietPlanMapper.selectList(new LambdaQueryWrapper<DietPlan>().eq(DietPlan::getUid, uid));
                        dietPlans.stream().forEach(dietPlan -> {
                            String pid = dietPlan.getPid();
                            // 清除用户饮食计划详情数据
                            List<DietPlanDetails> dietPlanDetails = dietPlanDetailsMapper.selectList(new LambdaQueryWrapper<DietPlanDetails>().eq(DietPlanDetails::getPid, pid));
                            dietPlanDetails.stream().forEach(dietPlanDetail -> {
                                String sid = dietPlanDetail.getSid();
                                // 清除用户饮食计划 meals 数据
                                dietPlanMealsMapper.delete(new LambdaQueryWrapper<DietPlanMeals>().eq(DietPlanMeals::getPdid, sid));
                                dietPlanDetailsMapper.deleteById(sid);
                            });
                            dietPlanMapper.deleteById(pid);
                        });

                        // 清除用户归档计划数据
                        List<DietPlanArchive> dietPlanArchives = dietPlanArchiveMapper.selectList(new LambdaQueryWrapper<DietPlanArchive>().eq(DietPlanArchive::getUid, uid));
                        dietPlanArchives.stream().forEach(dietPlanArchive -> {
                            String pid = dietPlanArchive.getPid();
                            // 清除用户饮食计划详情数据
                            List<DietPlanDetailsArchive> dietPlanDetailsArchives = dietPlanDetailsArchiveMapper.selectList(new LambdaQueryWrapper<DietPlanDetailsArchive>().eq(DietPlanDetailsArchive::getPid, pid));
                            dietPlanDetailsArchives.stream().forEach(dietPlanDetailArchive -> {
                                String sid = dietPlanDetailArchive.getSid();
                                // 清除用户饮食计划 meals 数据
                                dietPlanMealsArchiveMapper.delete(new LambdaQueryWrapper<DietPlanMealsArchive>().eq(DietPlanMealsArchive::getPdid, sid));
                                dietPlanDetailsArchiveMapper.deleteById(sid);
                            });
                            dietPlanArchiveMapper.deleteById(pid);
                        });

                        // 清除用户专属食物数据
                        foodsMapper.delete(new LambdaQueryWrapper<Foods>().eq(Foods::getUid, uid));

                        // 清除用户最喜欢的食物数据
                        foodsFavoriteMapper.delete(new LambdaQueryWrapper<FoodsFavorite>().eq(FoodsFavorite::getUid, uid));

                        // 清除用户最近添加的食物数据
                        foodsRecentlyMapper.delete(new LambdaQueryWrapper<FoodsRecently>().eq(FoodsRecently::getUid, uid));

                        // 清除用户帖子数据
                        forumPostMapper.delete(new LambdaQueryWrapper<ForumPost>().eq(ForumPost::getUid, uid));

                        // 清除用户不喜欢的帖子作者数据
                        forumPostAuthorDislikeMapper.delete(new LambdaQueryWrapper<ForumPostAuthorDislike>().eq(ForumPostAuthorDislike::getUid, uid).or().eq(ForumPostAuthorDislike::getAuthorUid, uid));

                        // 清除用户帖子点击数据
                        forumPostClickMapper.delete(new LambdaQueryWrapper<ForumPostClick>().eq(ForumPostClick::getUid, uid));

                        // 清除用户帖子收藏数据
                        forumPostCollectMapper.delete(new LambdaQueryWrapper<ForumPostCollect>().eq(ForumPostCollect::getUid, uid));

                        // 清除用户帖子评论数据
                        List<ForumPostComment> commentList = forumPostCommentMapper.selectList(new LambdaQueryWrapper<ForumPostComment>().eq(ForumPostComment::getFromUid, uid));
                        commentList.stream().forEach(comment -> forumPostService.deleteComment(comment.getId()));

                        // 清除用户帖子评论回复数据
                        List<ForumPostCommentReply> replyList = forumPostCommentReplyMapper.selectList(new LambdaQueryWrapper<ForumPostCommentReply>().eq(ForumPostCommentReply::getFromUid, uid));
                        replyList.stream().forEach(reply -> forumPostService.deleteReplyOnAccountDeletion(reply.getId()));

                        // 清除用户不喜欢的帖子数据
                        forumPostDislikeMapper.delete(new LambdaQueryWrapper<ForumPostDislike>().eq(ForumPostDislike::getUid, uid));

                        // 清除用户相关的帖子曝光数据
                        forumPostImpressionMapper.delete(new LambdaQueryWrapper<ForumPostImpression>().eq(ForumPostImpression::getUid, uid));

                        // 清除用户点赞帖子数据
                        forumPostLikeMapper.delete(new LambdaQueryWrapper<ForumPostLike>().eq(ForumPostLike::getUid, uid));

                        // 清除用户站内信消息
                        forumPostMsgMapper.delete(new LambdaQueryWrapper<ForumPostMsg>().eq(ForumPostMsg::getToUid, uid));

                        // 清除用户关联的投票数据
                        forumPostPollDataMapper.delete(new LambdaQueryWrapper<ForumPostPollData>().eq(ForumPostPollData::getUid, uid));

                        // 清除用户帖子举报数据
                        forumPostReportMapper.delete(new LambdaQueryWrapper<ForumPostReport>().eq(ForumPostReport::getUid, uid));

                        // 清除用户帖子分享数据
                        forumPostShareMapper.delete(new LambdaQueryWrapper<ForumPostShare>().eq(ForumPostShare::getUid, uid));

                        // 清除用户屏蔽数据
                        forumUserBlockMapper.delete(new LambdaQueryWrapper<ForumUserBlock>().eq(ForumUserBlock::getBlockerId, uid).or().eq(ForumUserBlock::getBlockedId, uid));

                        // 清除用户关注数据
                        forumUserFollowMapper.delete(new LambdaQueryWrapper<ForumUserFollow>().eq(ForumUserFollow::getFollowerId, uid).or().eq(ForumUserFollow::getFollowedId, uid));

                        // 清除用户关联的内容安全检查数据
                        greenCheckMapper.delete(new LambdaQueryWrapper<GreenCheck>().eq(GreenCheck::getUid, uid));

                        // 清除餐食数据
                        List<Meals> mealsList = mealsMapper.selectList(new LambdaQueryWrapper<Meals>().eq(Meals::getUid, uid));
                        mealsList.stream().forEach(meals -> {
                            if (StringUtils.isNotBlank(meals.getImage()))
                                ossService.deleteObject(meals.getImage());
                            String pid = meals.getId();
                            // 清除餐食食物数据
                            mealsFoodsMapper.delete(new LambdaQueryWrapper<MealsFoods>().eq(MealsFoods::getPid, pid));
                            mealsMapper.deleteById(pid);
                        });

                        // 清除用户短信验证码数据
                        smsCodeTempMapper.delete(new LambdaQueryWrapper<SmsCodeTemp>().eq(SmsCodeTemp::getIdc, user.getIdc()).eq(SmsCodeTemp::getPhone, user.getPhone()));

                        // 清除用户运动记录数据
                        sportLogMapper.delete(new LambdaQueryWrapper<SportLog>().eq(SportLog::getUid, uid));

                        // 清除用户最近添加的运动数据
                        sportRecentlyMapper.delete(new LambdaQueryWrapper<SportRecently>().eq(SportRecently::getUid, uid));

                        // 清除用户自定义运动数据
                        sportUserMapper.delete(new LambdaQueryWrapper<SportUser>().eq(SportUser::getUid, uid));

                        // 清除连胜数据
                        streakMapper.delete(new LambdaQueryWrapper<Streak>().eq(Streak::getUid, uid));
                        streakLogMapper.delete(new LambdaQueryWrapper<StreakLog>().eq(StreakLog::getUid, uid));

                        // 清除用户调研数据
                        surveyMapper.delete(new LambdaQueryWrapper<Survey>().eq(Survey::getUid, uid));

                        // 清除用户体测数据
                        List<UserBodyStatLog> userBodyStatLogs = userBodyStatLogMapper.selectList(new LambdaQueryWrapper<UserBodyStatLog>().eq(UserBodyStatLog::getUid, uid));
                        userBodyStatLogs.stream().forEach(userBodyStatLog -> {
                            if (StringUtils.isNotBlank(userBodyStatLog.getImgurl()))
                                ossService.deleteObject(userBodyStatLog.getImgurl());
                            userBodyStatLogMapper.deleteById(userBodyStatLog.getRid());
                        });

                        // 清除用户碳循环数据
                        userNutGoalCcMapper.delete(new LambdaQueryWrapper<UserNutGoalCc>().eq(UserNutGoalCc::getUid, uid));

                        // 清除用户默认营养数据
                        userNutritionDefaultMapper.delete(new LambdaQueryWrapper<UserNutritionDefault>().eq(UserNutritionDefault::getUid, uid));

                        // 清除用户操作日志
                        userOperLogMapper.delete(new LambdaQueryWrapper<UserOperLog>().eq(UserOperLog::getUid, uid));

                        // 清除用户建议数据
                        List<UserSuggestion> userSuggestions = userSuggestionMapper.selectList(new LambdaQueryWrapper<UserSuggestion>().eq(UserSuggestion::getUid, uid));
                        userSuggestions.stream().forEach(userSuggestion -> {
                            if (StringUtils.isNotBlank(userSuggestion.getImages())) {
                                String images = userSuggestion.getImages();
                                if (JsonValidator.isValidJsonString(images)) {
                                    JSONArray jsonArray = JSON.parseArray(images);
                                    jsonArray.stream().forEach(image -> {
                                        ossService.deleteObject((String) image);
                                    });
                                }
                            }
                            userSuggestionMapper.deleteById(userSuggestion.getId());
                        });

                        // 清除用户微信基本信息
                        userWxInfoMapper.delete(new LambdaQueryWrapper<UserWxInfo>().eq(UserWxInfo::getOpenid, user.getWxopenid()));

                        // 最后清除用户数据
                        if (StringUtils.isNotBlank(user.getHeadimgurl()) && !user.getHeadimgurl().contains("default_avatar"))
                            ossService.deleteObject(user.getHeadimgurl());
                        appUserExtMapper.deleteById(uid);
                        appUserMapper.deleteById(uid);
                    }).start();
                }
                log.setState(AppConstants.APP_USER_ACCOUNT_DELETED);
                accountDeletionLogMapper.updateById(log);
            }
        }
        DynamicDataSourceContextHolder.clearDataSourceType();
        return 1;
    }

}