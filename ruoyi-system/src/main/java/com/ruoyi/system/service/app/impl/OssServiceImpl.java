package com.ruoyi.system.service.app.impl;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.common.comm.ResponseMessage;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.VoidResult;
import com.ruoyi.system.config.OssConfig;
import com.ruoyi.system.service.app.IOssService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.List;

@Slf4j
@Service
public class OssServiceImpl implements IOssService {

    private String getObjectName(String url) {
        // 使用Hutool的split方法分割字符串，返回一个数组
        List<String> parts = StrUtil.split(url, '/', 4); // 分割并只保留前4个部分

        // 第四个元素就是第三个斜杠后面的字符串
        Assert.notNull(parts.get(3), "The fourth part of the URL should not be null.");

        return parts.get(3);
    }

    @Override
    public String putObject(MultipartFile file, String path) {
        // 获取文件名
        String fileName = file.getOriginalFilename();
        log.debug("文件名: {}", fileName);
        // 获取文件后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));

        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType(file.getContentType());

        // 最后上传生成的文件名
        String finalFileName = path + System.currentTimeMillis() + "" + new SecureRandom().nextInt(0x0400) + suffixName;
        log.debug("文件上传路径: {}", finalFileName);
        OSS ossClient = new OSSClientBuilder().build(
                OssConfig.getEndpoint(),
                OssConfig.getAccessKeyId(),
                OssConfig.getAccessKeySecret()
        );
        try {
            ossClient.putObject(OssConfig.getBucketName(), finalFileName, new ByteArrayInputStream(file.getBytes()), objectMetadata);
            return OssConfig.getDomain() + finalFileName;
        } catch (OSSException oe) {
            oe.printStackTrace();
        } catch (ClientException ce) {
            ce.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        return null;
    }

    @Override
    public boolean putForumPostHtmlFile(String path, String localFilePath) {
        boolean result = false;
        OSS ossClient = new OSSClientBuilder().build(
                OssConfig.getEndpoint(),
                OssConfig.getAccessKeyId(),
                OssConfig.getAccessKeySecret()
        );
        File file = new File(localFilePath);
        try {
            ossClient.putObject(OssConfig.getBucketName(), path + file.getName(), file);
            result = true;
        } catch (OSSException oe) {
            oe.printStackTrace();
        } catch (ClientException ce) {
            ce.printStackTrace();
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        file.delete();
        return result;
    }

    @Override
    public ResponseMessage deleteObject(String url) {
        OSS ossClient = new OSSClientBuilder().build(
                OssConfig.getEndpoint(),
                OssConfig.getAccessKeyId(),
                OssConfig.getAccessKeySecret()
        );
        VoidResult voidResult = null;
        try {
            String objectName = getObjectName(url);
            if (StrUtil.isNotBlank(objectName))
                voidResult = ossClient.deleteObject(OssConfig.getBucketName(), objectName);
        } catch (OSSException oe) {
            oe.printStackTrace();
        } catch (ClientException ce) {
            ce.printStackTrace();
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        return voidResult.getResponse();
    }

}
