package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.ForumTutorialMenu1;

import java.util.List;

public interface ForumTutorialMenu1Service {

    List<ForumTutorialMenu1> list();

    int add(ForumTutorialMenu1 forumTutorialMenu1);

    int update(ForumTutorialMenu1 forumTutorialMenu1);

    int delete(String id);

    ForumTutorialMenu1 selectById(String id);

}