package com.ruoyi.system.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.constant.AppConstants;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.bean.BeanValidators;
import com.ruoyi.system.domain.app.Foods;
import com.ruoyi.system.domain.app.FoodsRecently;
import com.ruoyi.system.mapper.app.FoodsMapper;
import com.ruoyi.system.mapper.app.FoodsRecentlyMapper;
import com.ruoyi.system.service.app.IFoodService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.validation.Validator;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
@DataSource(DataSourceType.SLAVE)
public class FoodServiceImpl implements IFoodService {

    @Resource
    private FoodsMapper foodsMapper;

    @Resource
    private Validator validator;

    @Resource
    private FoodsRecentlyMapper foodsRecentlyMapper;

    @Override
    public List<Foods> selectFoods(Foods foods) {
        return foodsMapper.selectFoods(foods);
    }

    @Override
    public boolean checkFoodsNameUnique(Foods foods) {
        return foodsMapper.selectList(new LambdaQueryWrapper<Foods>().eq(Foods::getFname, foods.getFname()).eq(Foods::getVerified, foods.getVerified())).isEmpty();
    }

    @Override
    public int insertOrUpdateFoods(Foods foods) {
        foods.setAlias(StringUtils.isBlank(foods.getAlias()) ? null : foods.getAlias());
        foods.setProtein(foods.getProtein() == null ? 0 : foods.getProtein());
        foods.setCarbohydrate(foods.getCarbohydrate() == null ? 0 : foods.getCarbohydrate());
        foods.setFat(foods.getFat() == null ? 0 : foods.getFat());
        foods.setCalories(foods.getCalories() == null ? 0 : foods.getCalories());
        return foods.getFid() == null ? foodsMapper.insert(foods) : foodsMapper.updateById(foods);
    }

    @Override
    @Transactional
    public int deleteFoodsByIds(String ids) {
        List<Integer> fids = Arrays.asList(Convert.toIntArray(ids));
        foodsRecentlyMapper.delete(new LambdaQueryWrapper<FoodsRecently>().in(FoodsRecently::getFid, fids));
        return foodsMapper.deleteBatchIds(Arrays.asList(Convert.toIntArray(ids)));
    }

    @Override
    public Foods selectFoodsById(Integer fid) {
        return foodsMapper.selectById(fid);
    }

    /**
     * 导入食物数据
     *
     * @param foodsList       食物数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName        操作用户
     * @return 结果
     */
    @Override
    public String importFoods(List<Foods> foodsList, Boolean isUpdateSupport, String operName) {
        if (StringUtils.isNull(foodsList) || foodsList.size() == 0) {
            throw new ServiceException("导入食物数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (Foods foods : foodsList) {
            try {
                // 验证是否存在这个食物
                LambdaQueryWrapper<Foods> wrapper = new LambdaQueryWrapper<>();
                wrapper.eq(Foods::getFname, foods.getFname()).eq(Foods::getVerified, foods.getVerified());
                Foods foodsInDb = foodsMapper.selectOne(wrapper);
                if (StringUtils.isNull(foodsInDb)) {
                    BeanValidators.validateWithException(validator, foods);
                    foods.setCreatedby(operName);
                    foodsMapper.insert(foods);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、食物 " + foods.getFname() + " 导入成功");
                } else if (isUpdateSupport) {
                    BeanValidators.validateWithException(validator, foods);
                    foodsInDb.setAlias(StringUtils.isBlank(foods.getAlias()) ? null : foods.getAlias());
                    foodsInDb.setProtein(foods.getProtein());
                    foodsInDb.setCarbohydrate(foods.getCarbohydrate());
                    foodsInDb.setFat(foods.getFat());
                    foodsInDb.setCalories(foods.getCalories());
                    foodsInDb.setSpec(StringUtils.isBlank(foods.getSpec()) ? null : foods.getSpec());
                    foodsInDb.setVerified(foods.getVerified());
                    foodsInDb.setUpdatedby(operName);
                    foodsInDb.setEtime(LocalDateTime.now());
                    foodsMapper.updateById(foodsInDb);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、食物 " + foodsInDb.getFname() + " 更新成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、食物 " + foods.getFname() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、食物 " + foods.getFname() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

    @Override
    public int promoteFoods(Foods foods, String createdBy) {
        Foods newFoods = new Foods();
        newFoods.setFname(foods.getFname());
        newFoods.setAlias(foods.getAlias());
        newFoods.setFtype(foods.getFtype());
        newFoods.setProtein(foods.getProtein());
        newFoods.setCarbohydrate(foods.getCarbohydrate());
        newFoods.setFat(foods.getFat());
        newFoods.setFibre(foods.getFibre());
        newFoods.setCalories(foods.getCalories());
        newFoods.setVerified(foods.getVerified());
        newFoods.setSpec(foods.getSpec());
        newFoods.setCreatedby(createdBy);
        newFoods.setCtime(LocalDateTime.now());
        newFoods.setUpdatedby(createdBy);
        newFoods.setEtime(LocalDateTime.now());
        return foodsMapper.insert(newFoods);
    }
}
