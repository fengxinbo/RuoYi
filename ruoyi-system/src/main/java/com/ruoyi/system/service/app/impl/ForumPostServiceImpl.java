package com.ruoyi.system.service.app.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.AppConstants;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.config.OssConfig;
import com.ruoyi.system.config.StringRedisService;
import com.ruoyi.system.domain.app.*;
import com.ruoyi.system.mapper.app.*;
import com.ruoyi.system.service.app.ForumPostService;
import com.ruoyi.system.service.app.IOssService;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@DataSource(DataSourceType.SLAVE)
public class ForumPostServiceImpl implements ForumPostService {

    @Autowired
    private ForumPostMapper forumPostMapper;

    @Autowired
    private ForumPostRsMapper forumPostRsMapper;

    @Autowired
    private ForumPostClickMapper forumPostClickMapper;

    @Autowired
    private ForumPostCollectMapper forumPostCollectMapper;

    @Autowired
    private ForumPostShareMapper forumPostShareMapper;

    @Autowired
    private ForumPostLikeMapper forumPostLikeMapper;

    @Autowired
    private ForumPostDislikeMapper forumPostDislikeMapper;

    @Autowired
    private ForumPostReportMapper forumPostReportMapper;

    @Autowired
    private ForumPostCommentMapper forumPostCommentMapper;

    @Autowired
    private ForumPostImpressionMapper forumPostImpressionMapper;

    @Autowired
    private ForumPostCommentReplyMapper forumPostCommentReplyMapper;

    @Autowired
    private ForumPostMsgMapper forumPostMsgMapper;

    @Autowired
    private ForumPostPollDataMapper forumPostPollDataMapper;

    @Autowired
    private ForumPostRankingMapper forumPostRankingMapper;

    @Autowired
    private IOssService ossService;

    @Autowired
    private StringRedisService redisService;

    private static final String FORUM_POST_HTML_FILE_PATH = "forum/post/html/";

    private static final ExecutorService executor = Executors.newFixedThreadPool(5);

    @Override
    public List<ForumPost> query(ForumPost forumPost) {
        return forumPostMapper.selectPostList(forumPost);
    }

    private static String modifyHtmlContent(String pContent) {
        Document doc = Jsoup.parse(pContent);
        // 设置 html 标签的 lang 属性
        Element htmlTag = doc.selectFirst("html");
        if (htmlTag != null) {
            if (!htmlTag.hasAttr("lang"))
                htmlTag.attr("lang", "zh-CN");
        }
        // 在 head 中添加 meta 标签
        Element headTag = doc.selectFirst("head");
        if (headTag != null) {
            if (headTag.select("meta[charset=UTF-8]").isEmpty()) {
                Element metaCharset = headTag.appendElement("meta");
                metaCharset.attr("charset", "UTF-8");
            }
            if (headTag.select("meta[name=viewport][content=width=device-width, initial-scale=1.0]").isEmpty()) {
                Element metaViewport = headTag.appendElement("meta");
                metaViewport.attr("name", "viewport");
                metaViewport.attr("content", "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no");
            }
            if (headTag.select("style:containsData(.image-container)").isEmpty()) {
                Element style = headTag.appendElement("style");
                style.appendText(".image-container {\n" +
                        "            text-align: center;\n" +
                        "            width: 100%;\n" +
                        "        }\n" +
                        "        .image-container img {\n" +
                        "            max-width: 100%;\n" +
                        "            height: auto;\n" +
                        "            display: block;\n" +
                        "            margin: 0 auto;\n" +
                        "        }");
            }
            if (headTag.select("style:containsData(.video-container)").isEmpty()) {
                Element style = headTag.appendElement("style");
                style.appendText(".video-container {\n" +
                        "            text-align: center;\n" +
                        "            width: 100%;\n" +
                        "        }\n" +
                        "        .video-container video {\n" +
                        "            max-width: 100%;\n" +
                        "            height: auto;\n" +
                        "            display: block;\n" +
                        "            margin: 0 auto;\n" +
                        "        }");
            }
        }

        Elements imgElements = doc.select("img");
        StringBuilder imgOssUrls = new StringBuilder("[");
        for (int i = 0; i < imgElements.size(); i++) {
            Element img = imgElements.get(i);
            if (img.hasAttr("style"))
                img.removeAttr("style");
            Element parent = img.parent();
            if (parent != null && !parent.hasClass("image-container")) {
                Element div = new Element("div").addClass("image-container");
                img.replaceWith(div);
                div.appendChild(img);
            }
            String src = img.attr("src");
            imgOssUrls.append("\"").append(src).append("\"");
            if (i < imgElements.size() - 1) {
                imgOssUrls.append(", ");
            }
            // 为每个 img 元素添加单击事件
            String onclick = "hitImgCallback(" + i + ", imgOssUrls);";
            img.attr("onclick", onclick);
        }
        imgOssUrls.append("]");

        Elements videoElements = doc.select("video");
        for (int i = 0; i < videoElements.size(); i++) {
            Element video = videoElements.get(i);
            Element parent = video.parent();
            if (parent != null && !parent.hasClass("video-container")) {
                // 创建一个新的 div 元素
                Element div = new Element("div").addClass("video-container");

                // 将 video 元素移动到新的 div 中
                video.replaceWith(div);
                div.appendChild(video);
            }
            video.attr("playsinline", "true");
            if (!video.hasAttr("poster")) {
                video.attr("poster", video.attr("src") + "?x-oss-process=video/snapshot,t_6000,f_jpg,w_0,h_0,m_fast");
            }
        }

        Element head = doc.head();
        boolean hitImgCallbackScriptExists = false;

        // 检查是否已经存在特定的脚本
        Elements scripts = head.select("script");
        for (Element script : scripts) {
            if (script.data().contains("function hitImgCallback")) {
                hitImgCallbackScriptExists = true;
                break;
            }
        }

        if (!hitImgCallbackScriptExists) {
            head.append("<script type='text/javascript'>\n" +
                    "var imgOssUrls = " + imgOssUrls + ";\n" +
                    "function isIOS() {\n" +
                    "    return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;\n" +
                    "}\n" +
                    "function isAndroid() {\n" +
                    "    return /Android/.test(navigator.userAgent);\n" +
                    "}\n" +
                    "function hitImgCallback(index, urls) {\n" +
                    "    if (isIOS()) {\n" +
                    "        navigateWithParams(index, urls);\n" +
                    "    } else if (isAndroid()) {\n" +
                    "        window.lnsJsInterface.showImgIndex(urls, index);\n" +
                    "    } else {\n" +
                    "        console.log('不支持的客户端');\n" +
                    "        return;\n" +
                    "    }\n" +
                    "}\n" +
                    "function navigateWithParams(index, urls) {\n" +
                    "    window.location.href = 'elaForum://forumDetail?imgIndex=' + index + '&imgArray=' + JSON.stringify(urls);\n" +
                    "}\n" +
                    "</script>");
        }
        return doc.html();
    }

    @Override
    public int create(ForumPost forumPost) {
        forumPost.setContent(modifyHtmlContent(forumPost.getContent()));
        String id = IdUtil.fastSimpleUUID();
        String filePath = RuoYiConfig.getUploadPath() + "/" + id + ".html";
        createHtmlFile(forumPost.getContent(), filePath);
        int affectedRows = 0;
        if (ossService.putForumPostHtmlFile(FORUM_POST_HTML_FILE_PATH, filePath)) {
            forumPost.setId(id);
            forumPost.setWeblink(OssConfig.getDomain() + FORUM_POST_HTML_FILE_PATH + id + ".html");
            affectedRows = forumPostMapper.insert(forumPost);
        }
        return affectedRows;
    }

    @Override
    public int createDummy(ForumPost forumPost) {
        forumPost.setPoster(2);// 模拟前端发帖
        Integer contentType = forumPost.getContentType();
        List<String> cover = forumPost.getCover();
        if ((contentType == 1 || contentType == 2) && (cover == null || cover.isEmpty()))
            throw new ServiceException("物料不能为空");
        if (cover != null && !cover.isEmpty()) {
            Material material = new Material();
            if (contentType == 1) {
                List<MaterialItem> images = new ArrayList<>();
                for (int i = 0; i < cover.size(); i++) {
                    String ossUrl = cover.get(i);
                    if (StrUtil.isNotBlank(ossUrl)) {
                        String extName = FileUtil.extName(ossUrl);
                        if (isImage(extName)) {
                            MaterialItem item = new MaterialItem();
                            item.setSn(i + 1);
                            item.setOssUrl(ossUrl);
                            images.add(item);
                        }
                    }
                }
                material.setImage(images);
            }
            if (contentType == 2) {
                List<MaterialItem> videos = new ArrayList<>();
                for (int i = 0; i < cover.size(); i++) {
                    String ossUrl = cover.get(i);
                    if (StrUtil.isNotBlank(ossUrl)) {
                        String extName = FileUtil.extName(ossUrl);
                        if (isVideo(extName)) {
                            MaterialItem item = new MaterialItem();
                            item.setSn(i + 1);
                            item.setOssUrl(ossUrl);
                            videos.add(item);
                        }
                    }
                }
                material.setVideo(videos);
            }
            forumPost.setMaterial(material);
            cover.subList(1, cover.size()).clear();
        }
        return forumPostMapper.insert(forumPost);
    }

    private static boolean isImage(String extName) {
        return extName != null && (extName.equalsIgnoreCase("jpg") || extName.equalsIgnoreCase("png") || extName.equalsIgnoreCase("gif"));
    }

    private static boolean isVideo(String extName) {
        return extName != null && (extName.equalsIgnoreCase("mp4") || extName.equalsIgnoreCase("avi") || extName.equalsIgnoreCase("mov"));
    }

    private static void createHtmlFile(String content, String filePath) {
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), "UTF-8"))) {
            writer.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int update(ForumPost forumPost) {
        Integer impressionThreshold = forumPost.getImpressionThreshold();
        if (impressionThreshold != null && impressionThreshold > 0)
            redisService.setValueWithExpiration(AppConstants.POST_IMPRESSION_THRESHOLD_PREFIX + forumPost.getId(), impressionThreshold.toString(), 1, TimeUnit.HOURS);
        if (forumPost.getPoster() == AppConstants.APP_FORUM_POSTER_BACKEND) {
            forumPost.setContent(modifyHtmlContent(forumPost.getContent()));
            String filePath = RuoYiConfig.getUploadPath() + "/" + forumPost.getId() + ".html";
            createHtmlFile(forumPost.getContent(), filePath);
            int affectedRows = 0;
            if (ossService.putForumPostHtmlFile(FORUM_POST_HTML_FILE_PATH, filePath)) {
                affectedRows = forumPostMapper.updateById(forumPost);
            }
            return affectedRows;
        } else {
            return forumPostMapper.updateById(forumPost);
        }
    }

    @Override
    @Transactional
    public int delete(String id) {
        ForumPost post = forumPostMapper.selectById(id);
        if (post != null) {
            if (post.getPoster() == AppConstants.APP_FORUM_POSTER_BACKEND) {
                List<String> urls = extractOssLinks(post.getContent());
                for (String url : urls) {
                    ossService.deleteObject(url);
                }
            }
            List<String> cover = post.getCover();
            if (cover != null)
                cover.stream().forEach(image -> ossService.deleteObject(image));
            if (StringUtils.isNotBlank(post.getWeblink()))
                ossService.deleteObject(post.getWeblink());
            Material material = post.getMaterial();
            if (material != null) {
                if (material.getImage() != null)
                    material.getImage().stream().forEach(image -> ossService.deleteObject(image.getOssUrl()));
                if (material.getVideo() != null)
                    material.getVideo().stream().forEach(video -> ossService.deleteObject(video.getOssUrl()));
            }
            PollJson pollJson = post.getPollJson();
            if (pollJson != null) {
                List<PollOption> pollOption = pollJson.getPollOption();
                if (pollOption != null)
                    pollOption.stream().forEach(item -> {
                        if (item.getImage() != null)
                            item.getImage().stream().forEach(image -> ossService.deleteObject(image));
                    });
            }
            // 删除帖子对应的浏览记录
            forumPostClickMapper.delete(new LambdaQueryWrapper<ForumPostClick>().eq(ForumPostClick::getPostId, id));
            // 删除帖子对应的收藏记录
            forumPostCollectMapper.delete(new LambdaQueryWrapper<ForumPostCollect>().eq(ForumPostCollect::getBizId, id));
            // 迭代删除帖子对应的评论和回复记录
            List<ForumPostComment> commentList = forumPostCommentMapper.selectList(new LambdaQueryWrapper<ForumPostComment>().eq(ForumPostComment::getPostId, id));
            for (ForumPostComment comment : commentList) {
                List<ForumPostCommentReply> replyList = forumPostCommentReplyMapper.selectList(new LambdaQueryWrapper<ForumPostCommentReply>().eq(ForumPostCommentReply::getCommentId, comment.getId()));
                replyList.forEach(reply -> {
                    deleteImagesIfPresent(reply.getImage());
                    deleteReply(reply.getId());
                    forumPostCommentReplyMapper.deleteById(reply.getId());
                });
                forumPostCommentMapper.deleteById(comment.getId());
            }
            // 删除帖子对应的屏蔽记录
            forumPostDislikeMapper.delete(new LambdaQueryWrapper<ForumPostDislike>().eq(ForumPostDislike::getPostId, id));
            // 删除帖子对应的曝光记录
            forumPostImpressionMapper.delete(new LambdaQueryWrapper<ForumPostImpression>().eq(ForumPostImpression::getPostId, id));
            // 删除帖子对应的点赞记录
            forumPostLikeMapper.delete(new LambdaQueryWrapper<ForumPostLike>().eq(ForumPostLike::getBizId, id));
            // 删除帖子相关的所有站内信
            forumPostMsgMapper.delete(new LambdaQueryWrapper<ForumPostMsg>().eq(ForumPostMsg::getPostId, id));
            // 删除帖子对应的投票记录
            forumPostPollDataMapper.delete(new LambdaQueryWrapper<ForumPostPollData>().eq(ForumPostPollData::getPostId, id));
            // 删除帖子对应的排名记录
            forumPostRankingMapper.delete(new LambdaQueryWrapper<ForumPostRanking>().eq(ForumPostRanking::getId, id));
            // 删除帖子对应的举报记录
            forumPostReportMapper.delete(new LambdaQueryWrapper<ForumPostReport>().eq(ForumPostReport::getBizId, id));
            // 删除帖子对应的推荐数据
            forumPostRsMapper.deleteById(id);
            // 删除帖子对应的分享记录
            forumPostShareMapper.delete(new LambdaQueryWrapper<ForumPostShare>().eq(ForumPostShare::getPostId, id));
        }
        return forumPostMapper.deleteById(id);
    }

    @Override
    public int deleteComment(String id) {
        List<ForumPostCommentReply> replyList = forumPostCommentReplyMapper.selectList(new LambdaQueryWrapper<ForumPostCommentReply>().eq(ForumPostCommentReply::getCommentId, id));
        replyList.forEach(reply -> {
            deleteImagesIfPresent(reply.getImage());
            deleteReply(reply.getId());
            forumPostCommentReplyMapper.deleteById(reply.getId());
        });
        // 删除帖子评论相关的所有站内信
        forumPostMsgMapper.delete(new LambdaQueryWrapper<ForumPostMsg>().eq(ForumPostMsg::getCommentId, id));
        return forumPostCommentMapper.deleteById(id);
    }

    @Override
    public int deleteReplyOnAccountDeletion(String id) {
        int result = 0;
        ForumPostCommentReply reply = forumPostCommentReplyMapper.selectById(id);
        if (reply != null) {
            // 删除帖子回复相关的所有站内信
            forumPostMsgMapper.delete(new LambdaQueryWrapper<ForumPostMsg>().eq(ForumPostMsg::getReplyId, reply.getId()));
            deleteImagesIfPresent(reply.getImage());
            result = forumPostCommentReplyMapper.deleteById(id);
        }
        return result;
    }

    private void deleteReply(String parentId) {
        List<ForumPostCommentReply> replyList = forumPostCommentReplyMapper.selectList(new LambdaQueryWrapper<ForumPostCommentReply>().eq(ForumPostCommentReply::getParentId, parentId));
        for (ForumPostCommentReply reply : replyList) {
            // 删除图片
            deleteImagesIfPresent(reply.getImage());
            // 删除帖子回复相关的所有站内信
            forumPostMsgMapper.delete(new LambdaQueryWrapper<ForumPostMsg>().eq(ForumPostMsg::getReplyId, reply.getId()));
            deleteReply(reply.getId());
            forumPostCommentReplyMapper.deleteById(reply.getId());
        }
    }

    private void deleteImagesIfPresent(List<String> images) {
        if (images == null || images.isEmpty()) {
            return;
        }
        for (String imageUrl : images) {
            executor.submit(() -> {
                try {
                    ossService.deleteObject(imageUrl);
                } catch (Exception e) {
                    // 记录异常日志
                    log.error("Failed to delete image: " + imageUrl, e);
                }
            });
        }
    }

    private static List<String> extractOssLinks(String htmlContent) {
        List<String> links = new ArrayList<>();

        // 解析HTML内容
        Document doc = Jsoup.parse(htmlContent);

        // 选择所有的 <img> 和 <video> 标签
        Elements imgElements = doc.select("img[src]");
        Elements videoElements = doc.select("video[src]");

        // 提取 <img> 标签中的 src 属性
        for (Element img : imgElements) {
            String src = img.attr("src");
            if (src != null && !src.isEmpty()) {
                links.add(src);
            }
        }

        // 提取 <video> 标签中的 src 属性
        for (Element video : videoElements) {
            String src = video.attr("src");
            if (src != null && !src.isEmpty()) {
                links.add(src);
            }
        }

        return links;
    }

    @Override
    public ForumPost selectById(String id) {
        return forumPostMapper.selectById(id);
    }

    @Override
    public JSONObject showPostCover(String id) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", "相册");
        jsonObject.put("id", 123);
        jsonObject.put("start", 0);
        JSONArray data = new JSONArray();
        ForumPost post = forumPostMapper.selectById(id);
        List<String> cover = post.getCover();
        for (int i = 0; i < cover.size(); i++) {
            JSONObject json = new JSONObject();
            json.put("alt", "pic");
            json.put("pid", i + 1);
            json.put("src", cover.get(i));
            json.put("thumb", cover.get(i));
            data.add(json);
        }
        jsonObject.put("data", data);
        return jsonObject;
    }

    @Override
    public int replyPost(ForumPost post, String fromUid, String location, String content, String img) {
        ForumPostComment comment = new ForumPostComment();
        comment.setPostId(post.getId());
        comment.setFromUid(fromUid);
        comment.setToUid(post.getUid());
        comment.setContent(content);
        comment.setLocation(location);
        if (StrUtil.isNotBlank(img))
            comment.setImage(Arrays.asList(img));
        int affectedRows = forumPostCommentMapper.insert(comment);
        if (affectedRows > 0) {
            if (!fromUid.equalsIgnoreCase(post.getUid())) {
                // 自己评论自己的帖子不给自己发通知
                ForumPostMsg msg = new ForumPostMsg();
                msg.setPostId(post.getId());
                msg.setCommentId(comment.getId());
                msg.setParentId(post.getId());
                msg.setType(1);// 评论
                msg.setFromUid(fromUid);
                msg.setToUid(post.getUid());
                msg.setParentUid(post.getUid());
                msg.setTitle("评论了你的帖子");
                forumPostMsgMapper.insert(msg);
            }
        }
        return affectedRows;
    }

}