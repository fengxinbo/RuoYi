package com.ruoyi.system.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.config.datasource.DynamicDataSourceContextHolder;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.app.ForumTutorial;
import com.ruoyi.system.domain.app.ForumTutorialMenu2;
import com.ruoyi.system.mapper.app.ForumTutorialMenu2Mapper;
import com.ruoyi.system.service.app.ForumTutorialMenu2Service;
import com.ruoyi.system.service.app.ForumTutorialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class ForumTutorialMenu2ServiceImpl implements ForumTutorialMenu2Service {

    @Autowired
    private ForumTutorialMenu2Mapper forumTutorialMenu2Mapper;

    @Autowired
    private ForumTutorialService forumTutorialService;

    @Override
    public List<ForumTutorialMenu2> list(String parentId) {
        return forumTutorialMenu2Mapper.selectList(new LambdaQueryWrapper<ForumTutorialMenu2>().eq(ForumTutorialMenu2::getParentId, parentId).orderByAsc(ForumTutorialMenu2::getSn));
    }

    @Override
    public int add(ForumTutorialMenu2 forumTutorialMenu2) {
        return forumTutorialMenu2Mapper.insert(forumTutorialMenu2);
    }

    @Override
    public int update(ForumTutorialMenu2 forumTutorialMenu2) {
        return forumTutorialMenu2Mapper.updateById(forumTutorialMenu2);
    }

    @Override
    @Transactional
    public int delete(String id) {
        List<ForumTutorial> list = forumTutorialService.list(id);
        list.stream().forEach(tutorial -> forumTutorialService.delete(tutorial.getId()));
        DynamicDataSourceContextHolder.setDataSourceType(DataSourceType.SLAVE.name());
        int affectedRows = forumTutorialMenu2Mapper.deleteById(id);
        DynamicDataSourceContextHolder.clearDataSourceType();
        return affectedRows;
    }

    @Override
    public ForumTutorialMenu2 selectById(String id) {
        return forumTutorialMenu2Mapper.selectById(id);
    }

}