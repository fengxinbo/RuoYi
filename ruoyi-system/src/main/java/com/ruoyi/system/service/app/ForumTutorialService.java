package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.ForumTutorial;

import java.util.List;

public interface ForumTutorialService {

    List<ForumTutorial> list(String parentId);

    int add(ForumTutorial forumTutorial);

    int update(ForumTutorial forumTutorial);

    int delete(String id);

    ForumTutorial selectById(String id);

}