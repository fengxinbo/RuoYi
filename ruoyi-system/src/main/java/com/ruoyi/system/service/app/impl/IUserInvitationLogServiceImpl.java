package com.ruoyi.system.service.app.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.app.UserInvitationLog;
import com.ruoyi.system.domain.app.UserInvitationLogStatistics;
import com.ruoyi.system.mapper.app.UserInvitationLogMapper;
import com.ruoyi.system.service.app.IUserInvitationLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class IUserInvitationLogServiceImpl implements IUserInvitationLogService {

    @Resource
    private UserInvitationLogMapper userInvitationLogMapper;

    @Override
    public List<UserInvitationLog> selectUserInvitationLog(UserInvitationLog userInvitationLog) {
        return userInvitationLogMapper.selectUserInvitationLog(userInvitationLog);
    }

    @Override
    public List<UserInvitationLogStatistics> selectStatistics(UserInvitationLogStatistics userInvitationLogStatistics) {
        return userInvitationLogMapper.selectStatistics(userInvitationLogStatistics);
    }
}
