package com.ruoyi.system.service.app.impl;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.app.*;
import com.ruoyi.system.mapper.app.*;
import com.ruoyi.system.service.app.AppUserService;
import com.ruoyi.system.service.app.IOssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
@DataSource(DataSourceType.SLAVE)
public class AppUserServiceImpl implements AppUserService {

    @Autowired
    private AppUserMapper appUserMapper;

    @Autowired
    private AppUserExtMapper appUserExtMapper;

    @Autowired
    private UserFissionMapper userFissionMapper;

    @Autowired
    private UserSuggestionMapper userSuggestionMapper;

    @Autowired
    private DAUMapper dauMapper;

    @Autowired
    private UserInvitationLogMapper userInvitationLogMapper;

    @Autowired
    private FoodsMissedMapper foodsMissedMapper;

    @Autowired
    private IOssService ossService;

    @Autowired
    private ForumPostMapper forumPostMapper;

    @Autowired
    private ForumPostCommentMapper forumPostCommentMapper;

    @Autowired
    private ForumPostCommentReplyMapper forumPostCommentReplyMapper;

    @Autowired
    private ForumUserFollowMapper forumUserFollowMapper;

    @Override
    public List<AppUser> selectAppUserList(AppUser user) {
        return appUserMapper.selectUserList(user);
    }

    private Double getUserBalance(String uid, String balColName, String uidColName) {
        QueryWrapper<UserFission> wrapper = new QueryWrapper<>();
        wrapper.select(String.format("IFNULL(sum(%s),0) as totalbalance", balColName)).eq(uidColName, uid);
        List<Map<String, Object>> balance = userFissionMapper.selectMaps(wrapper);
        BigDecimal totalBalance = BigDecimal.ZERO;
        if (balance != null && !balance.isEmpty()) {
            totalBalance = (BigDecimal) balance.get(0).get("totalbalance");
        }
        return totalBalance.doubleValue();
    }

    private Double getUserBalance(String uid) {
        QueryWrapper<UserInvitationLog> wrapper = new QueryWrapper<>();
        wrapper.select("IFNULL(sum(money),0) as totalbalance").eq("inviter", uid);
        List<Map<String, Object>> balance = userInvitationLogMapper.selectMaps(wrapper);
        BigDecimal totalBalance = BigDecimal.ZERO;
        if (balance != null && !balance.isEmpty()) {
            totalBalance = (BigDecimal) balance.get(0).get("totalbalance");
        }
        return totalBalance.doubleValue();
    }

    @Override
    public int changeStatus(AppUser user) {
        return appUserMapper.updateById(user);
    }

    @Override
    public int t1switch(String uid, Integer ulevel) {
        int result = 0;
        AppUser user = appUserMapper.selectById(uid);
        if (user != null) {
            LambdaUpdateWrapper<AppUser> wrapper = new LambdaUpdateWrapper<>();
            wrapper.eq(AppUser::getId, uid).set(AppUser::getUlevel, ulevel == 0 ? null : ulevel).set(AppUser::getEtime, LocalDateTime.now());
            result = appUserMapper.update(user, wrapper);
        }
        return result;
    }

    @Override
    public List<UserSuggestion> selectUserSuggestion(UserSuggestion userSuggestion) {
        return userSuggestionMapper.selectUserSuggestion(userSuggestion);
    }

    @Override
    public UserSuggestion selectUserSuggestionById(String id) {
        return userSuggestionMapper.selectById(id);
    }

    @Override
    public JSONObject showSuggestionPics(String id) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", "相册");
        jsonObject.put("id", 123);
        jsonObject.put("start", 0);
        JSONArray data = new JSONArray();
        UserSuggestion suggestion = userSuggestionMapper.selectById(id);
        if (StringUtils.isNotBlank(suggestion.getImages())) {
            JSONArray jsonArray = JSONArray.parseArray(suggestion.getImages());
            for (int i = 0; i < jsonArray.size(); i++) {
                String url = (String) jsonArray.get(i);
                if (StringUtils.isNotBlank(url)) {
                    JSONObject json = new JSONObject();
                    json.put("alt", "pic");
                    json.put("pid", i + 1);
                    json.put("src", url);
                    json.put("thumb", url);
                    data.add(json);
                }
            }
        }
        jsonObject.put("data", data);
        return jsonObject;
    }

    @Override
    public int processSuggestion(String id) {
        int result = 0;
        if (StringUtils.isNotBlank(id)) {
            LambdaUpdateWrapper<UserSuggestion> wrapper = new LambdaUpdateWrapper<>();
            wrapper.eq(UserSuggestion::getId, id).set(UserSuggestion::getState, 1);
            result = userSuggestionMapper.update(null, wrapper);
        }
        return result;
    }

    @Override
    public int replySuggestion(UserSuggestion userSuggestion) {
        UserSuggestion req = userSuggestionMapper.selectById(userSuggestion.getId());
        req.setState(1);
        userSuggestionMapper.updateById(req);

        UserSuggestion reply = new UserSuggestion();
        reply.setUid(userSuggestion.getUid());
        reply.setSuggestion(userSuggestion.getSuggestion());
        reply.setCreatedby(userSuggestion.getCreatedby());
        return userSuggestionMapper.insert(reply);
    }

    @Override
    public int deleteMsgById(String id) {
        return userSuggestionMapper.deleteById(id);
    }

    @Override
    public String getIndexAge() {
        Object[] xAxisArray = new Object[]{};
        Object[] yAxisArray = new Object[]{};
        List<Map<String, Object>> mapList = appUserMapper.selectIndexAge();
        if (mapList != null && !mapList.isEmpty()) {
            xAxisArray = mapList.stream().map(m -> m.get("age")).toArray();
            yAxisArray = mapList.stream().map(m -> m.get("c")).toArray();
        }
        JSONArray colorArray = new JSONArray();
        colorArray.add("#68bfff");

        JSONArray dataArray = new JSONArray();
        dataArray.add("用户年龄分布");
        JSONObject legend = new JSONObject();
        legend.put("data", dataArray);

        JSONObject xAxisObj = new JSONObject();
        xAxisObj.put("type", "category");
        xAxisObj.put("data", xAxisArray);
        JSONObject axisLabel = new JSONObject();
        axisLabel.put("show", true);
        axisLabel.put("rotate", 45);
        axisLabel.put("margin", 12);
        xAxisObj.put("axisLabel", axisLabel);
        JSONArray xAxisJSONArray = new JSONArray();
        xAxisJSONArray.add(xAxisObj);

        JSONObject seriesObj = new JSONObject();
        seriesObj.put("name", "用户年龄分布");
        seriesObj.put("type", "bar");
        seriesObj.put("data", yAxisArray);
        JSONArray yAxisJSONArray = new JSONArray();
        yAxisJSONArray.add(seriesObj);

        JSONObject tooltip = new JSONObject();
        tooltip.put("trigger", "axis");

        JSONObject option = new JSONObject();
        option.put("legend", legend);
        option.put("xAxis", xAxisJSONArray);
        option.put("yAxis", new JSONObject());
        option.put("series", yAxisJSONArray);
        option.put("color", colorArray);
        option.put("tooltip", tooltip);

        return option.toJSONString();
    }

    @Override
    public String getIndexDNU() {
        Object[] xAxisArray = new Object[]{};
        Object[] yAxisArray = new Object[]{};
        List<Map<String, Object>> mapList = appUserMapper.selectIndexDNU();
        if (mapList != null && !mapList.isEmpty()) {
            xAxisArray = mapList.stream().map(m -> m.get("ct")).toArray();
            yAxisArray = mapList.stream().map(m -> m.get("c")).toArray();
        }
        JSONArray colorArray = new JSONArray();
        colorArray.add("#4eabef");

        JSONArray dataArray = new JSONArray();
        dataArray.add("日新增");
        JSONObject legend = new JSONObject();
        legend.put("data", dataArray);

        JSONObject xAxisObj = new JSONObject();
        xAxisObj.put("type", "category");
        xAxisObj.put("data", xAxisArray);
        JSONObject axisLabel = new JSONObject();
        axisLabel.put("show", true);
        axisLabel.put("rotate", 45);
        axisLabel.put("margin", 12);
        xAxisObj.put("axisLabel", axisLabel);
        JSONArray xAxisJSONArray = new JSONArray();
        xAxisJSONArray.add(xAxisObj);

        JSONObject seriesObj = new JSONObject();
        seriesObj.put("name", "日新增");
        seriesObj.put("type", "bar");
        seriesObj.put("data", yAxisArray);
        JSONArray yAxisJSONArray = new JSONArray();
        yAxisJSONArray.add(seriesObj);

        JSONObject tooltip = new JSONObject();
        tooltip.put("trigger", "axis");

        JSONObject option = new JSONObject();
        option.put("legend", legend);
        option.put("xAxis", xAxisJSONArray);
        option.put("yAxis", new JSONObject());
        option.put("series", yAxisJSONArray);
        option.put("color", colorArray);
        option.put("tooltip", tooltip);

        return option.toJSONString();
    }

    @Override
    public String getIndexDAU() {
        Object[] xAxisArray = new Object[]{};
        Object[] yAxisArray = new Object[]{};
        List<Map<String, Object>> mapList = dauMapper.selectIndexDAU();
        if (mapList != null && !mapList.isEmpty()) {
            xAxisArray = mapList.stream().map(m -> m.get("ctime")).toArray();
            yAxisArray = mapList.stream().map(m -> m.get("c")).toArray();
        }
        JSONArray colorArray = new JSONArray();
        colorArray.add("#5ab4f9");

        JSONArray dataArray = new JSONArray();
        dataArray.add("日活");
        JSONObject legend = new JSONObject();
        legend.put("data", dataArray);

        JSONObject xAxisObj = new JSONObject();
        xAxisObj.put("type", "category");
        xAxisObj.put("data", xAxisArray);
        JSONObject axisLabel = new JSONObject();
        axisLabel.put("show", true);
        axisLabel.put("rotate", 45);
        axisLabel.put("margin", 12);
        xAxisObj.put("axisLabel", axisLabel);
        JSONArray xAxisJSONArray = new JSONArray();
        xAxisJSONArray.add(xAxisObj);

        JSONObject seriesObj = new JSONObject();
        seriesObj.put("name", "日活");
        seriesObj.put("type", "bar");
        seriesObj.put("data", yAxisArray);
        JSONArray yAxisJSONArray = new JSONArray();
        yAxisJSONArray.add(seriesObj);

        JSONObject tooltip = new JSONObject();
        tooltip.put("trigger", "axis");

        JSONObject option = new JSONObject();
        option.put("legend", legend);
        option.put("xAxis", xAxisJSONArray);
        option.put("yAxis", new JSONObject());
        option.put("series", yAxisJSONArray);
        option.put("color", colorArray);
        option.put("tooltip", tooltip);

        return option.toJSONString();
    }

    @Override
    public String getGenderPie() {
        JSONObject seriesObj = new JSONObject();
        seriesObj.put("type", "pie");
        seriesObj.put("radius", "55%");
        seriesObj.put("data", appUserMapper.selectIndexGenderData());
        seriesObj.put("color", new JSONArray(Arrays.asList("#4eabef", "#ffa5ff", "#d3d3d3")));

        JSONObject option = new JSONObject();
        option.put("title", new JSONObject() {
            {
                put("text", "用户性别分布");
            }
        });
        option.put("series", new JSONArray() {
            {
                add(seriesObj);
            }
        });
        option.put("tooltip", new JSONObject() {
            {
                put("trigger", "item");
            }
        });

        return option.toJSONString();
    }

    @Override
    public String getIndexMissedFoods() {
        Object[] xAxisArray = new Object[]{};
        Object[] yAxisArray = new Object[]{};
        List<Map<String, Object>> mapList = foodsMissedMapper.selectIndexMissedFoods();
        if (mapList != null && !mapList.isEmpty()) {
            xAxisArray = mapList.stream().map(m -> m.get("fname")).toArray();
            yAxisArray = mapList.stream().map(m -> m.get("hit")).toArray();
        }
        JSONArray colorArray = new JSONArray();
        colorArray.add("#68bfff");

        JSONArray dataArray = new JSONArray();
        dataArray.add("搜索未命中食物TOP10");
        JSONObject legend = new JSONObject();
        legend.put("data", dataArray);

        JSONObject xAxisObj = new JSONObject();
        xAxisObj.put("type", "category");
        xAxisObj.put("data", xAxisArray);
        JSONObject axisLabel = new JSONObject();
        axisLabel.put("show", true);
        axisLabel.put("rotate", 45);
        axisLabel.put("margin", 12);
        xAxisObj.put("axisLabel", axisLabel);
        JSONArray xAxisJSONArray = new JSONArray();
        xAxisJSONArray.add(xAxisObj);

        JSONObject seriesObj = new JSONObject();
        seriesObj.put("name", "搜索未命中食物TOP10");
        seriesObj.put("type", "bar");
        seriesObj.put("data", yAxisArray);
        JSONArray yAxisJSONArray = new JSONArray();
        yAxisJSONArray.add(seriesObj);

        JSONObject tooltip = new JSONObject();
        tooltip.put("trigger", "axis");

        JSONObject option = new JSONObject();
        option.put("legend", legend);
        option.put("xAxis", xAxisJSONArray);
        option.put("yAxis", new JSONObject());
        option.put("series", yAxisJSONArray);
        option.put("color", colorArray);
        option.put("tooltip", tooltip);

        return option.toJSONString();
    }

    @Override
    public AppUser selectUserByUid(String uid) {
        return appUserMapper.selectById(uid);
    }

    @Override
    public AppUserExt selectUserExtByUid(String uid) {
        return appUserExtMapper.selectById(uid);
    }

    @Override
    @Transactional
    public int insertDummyUser(AppUser user) {
        int result = 0;
        if (appUserMapper.insert(user) > 0) {
            adjustDummyFollowers(user.getId(), user.getFollowers());
            adjustDummyFollowing(user.getId(), user.getFollowing());
            AppUserExt ext = new AppUserExt();
            ext.setUid(user.getId());
            ext.setDummy(user.getDummy());
            result = appUserExtMapper.insert(ext);
        }
        return result;
    }

    private void adjustDummyFollowers(String uid, Long targetCount) {
        Long currentCount = forumUserFollowMapper.selectCount(new LambdaQueryWrapper<ForumUserFollow>().eq(ForumUserFollow::getFollowedId, uid));
        currentCount = currentCount == null ? 0 : currentCount;
        targetCount = targetCount == null ? 0 : targetCount;
        if (currentCount < targetCount) {
            // 增加粉丝
            Long addCount = targetCount - currentCount;
            for (int i = 0; i < addCount; i++) {
                ForumUserFollow follow = new ForumUserFollow();
                follow.setFollowerId("SU" + RandomUtil.randomString(30).toLowerCase());
                follow.setFollowedId(uid);
                forumUserFollowMapper.insert(follow);
            }
        }
        if (currentCount > targetCount) {
            // 减少粉丝
            Long removeCount = currentCount - targetCount;
            forumUserFollowMapper.delete(new LambdaQueryWrapper<ForumUserFollow>().eq(ForumUserFollow::getFollowedId, uid).likeRight(ForumUserFollow::getFollowerId, "SU").last("limit " + removeCount));
        }
    }

    private void adjustDummyFollowing(String uid, Long targetCount) {
        Long currentCount = forumUserFollowMapper.selectCount(new LambdaQueryWrapper<ForumUserFollow>().eq(ForumUserFollow::getFollowerId, uid));
        currentCount = currentCount == null ? 0 : currentCount;
        targetCount = targetCount == null ? 0 : targetCount;
        if (currentCount < targetCount) {
            // 增加偶像
            Long addCount = targetCount - currentCount;
            for (int i = 0; i < addCount; i++) {
                ForumUserFollow follow = new ForumUserFollow();
                follow.setFollowerId(uid);
                follow.setFollowedId("SU" + RandomUtil.randomString(30).toLowerCase());
                forumUserFollowMapper.insert(follow);
            }
        }
        if (currentCount > targetCount) {
            // 减少偶像
            Long removeCount = currentCount - targetCount;
            forumUserFollowMapper.delete(new LambdaQueryWrapper<ForumUserFollow>().eq(ForumUserFollow::getFollowerId, uid).likeRight(ForumUserFollow::getFollowedId, "SU").last("limit " + removeCount));
        }
    }

    @Override
    @Transactional
    public int updateDummyUser(AppUser user) {
        user.setEtime(LocalDateTime.now());
        appUserMapper.updateById(user);
        adjustDummyFollowers(user.getId(), user.getFollowers());
        adjustDummyFollowing(user.getId(), user.getFollowing());
        AppUserExt ext = appUserExtMapper.selectById(user.getId());
        ext.setDummy(user.getDummy());
        ext.setEtime(LocalDateTime.now());
        appUserExtMapper.updateById(ext);
        return 1;
    }

    @Override
    public int deleteDummyUserByUid(String uid) {
        int affectedRows = 0;
        AppUser user = appUserMapper.selectById(uid);
        if (user != null) {
            String headimgurl = user.getHeadimgurl();
            if (StringUtils.isNotBlank(headimgurl))
                ossService.deleteObject(headimgurl);
            affectedRows = appUserMapper.deleteById(user);
        }
        AppUserExt ext = appUserExtMapper.selectById(uid);
        if (ext != null)
            affectedRows = appUserExtMapper.deleteById(uid);
        return affectedRows;
    }

    @Override
    public AppUser selectDummyUserByUid(String uid) {
        AppUser user = appUserMapper.selectDummyUserByUid(uid);
        user.setFollowers(forumUserFollowMapper.selectCount(new LambdaQueryWrapper<ForumUserFollow>().eq(ForumUserFollow::getFollowedId, uid)));
        user.setFollowing(forumUserFollowMapper.selectCount(new LambdaQueryWrapper<ForumUserFollow>().eq(ForumUserFollow::getFollowerId, uid)));
        return user;
    }

    @Override
    public List<AppUser> selectOfficialUserList() {
        return appUserMapper.selectOfficialUserList();
    }

    @Override
    public int changeVisibleScope(AppUser user) {
        AppUserExt userExt = appUserExtMapper.selectById(user.getId());
        if (userExt != null) {
            if (user.getForumPostVisibleScope() != null) {
                if (user.getForumPostVisibleScope() == 11) {
                    // 将用户所有的帖子的可见范围修改为1（仅自己可见）
                    forumPostMapper.update(null, new LambdaUpdateWrapper<ForumPost>().eq(ForumPost::getUid, user.getId()).set(ForumPost::getVisibleScope, 1).set(ForumPost::getEtime, LocalDateTime.now()));
                }
                userExt.setForumPostVisibleScope(user.getForumPostVisibleScope() == 11 ? 1 : user.getForumPostVisibleScope());
                userExt.setEtime(LocalDateTime.now());
                appUserExtMapper.updateById(userExt);
            }
            if (user.getForumCommentVisibleScope() != null) {
                if (user.getForumCommentVisibleScope() == 11) {
                    // 将用户所有的评论/回复的可见范围修改为1（仅自己可见）
                    forumPostCommentMapper.update(null, new LambdaUpdateWrapper<ForumPostComment>().eq(ForumPostComment::getFromUid, user.getId()).set(ForumPostComment::getVisibleScope, 1));
                    forumPostCommentReplyMapper.update(null, new LambdaUpdateWrapper<ForumPostCommentReply>().eq(ForumPostCommentReply::getFromUid, user.getId()).set(ForumPostCommentReply::getVisibleScope, 1));
                }
                userExt.setForumCommentVisibleScope(user.getForumCommentVisibleScope() == 11 ? 1 : user.getForumCommentVisibleScope());
                userExt.setEtime(LocalDateTime.now());
                appUserExtMapper.updateById(userExt);
            }
        }
        return 1;
    }

    @Override
    public int changeForumAuthorRating(AppUser user) {
        AppUserExt userExt = appUserExtMapper.selectById(user.getId());
        if (userExt != null) {
            userExt.setForumAuthorRating(user.getForumAuthorRating());
            userExt.setEtime(LocalDateTime.now());
            appUserExtMapper.updateById(userExt);
            return 1;
        }
        return 0;
    }

}