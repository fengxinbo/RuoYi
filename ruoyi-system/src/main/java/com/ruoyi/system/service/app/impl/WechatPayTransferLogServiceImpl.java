package com.ruoyi.system.service.app.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.app.WechatPayTransferLog;
import com.ruoyi.system.mapper.app.WechatPayTransferLogMapper;
import com.ruoyi.system.service.app.IWechatPayTransferLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class WechatPayTransferLogServiceImpl implements IWechatPayTransferLogService {

    @Autowired
    private WechatPayTransferLogMapper wechatPayTransferLogMapper;

    @Override
    public List<WechatPayTransferLog> selectTransferLog(WechatPayTransferLog wechatPayTransferLog) {
        List<WechatPayTransferLog> list = wechatPayTransferLogMapper.selectTransferLog(wechatPayTransferLog);
        list.stream().forEach(item -> {
            if (item.getSuccessNum() != null)
                if ("FINISHED".equalsIgnoreCase(item.getBatchStatus()))
                    item.setBatchStatus(item.getSuccessNum() > 0 ? "SUCCESS" : "FAILED");
        });
        return list;
    }
}
