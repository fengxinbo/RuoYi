package com.ruoyi.system.service.app;

import com.aliyun.oss.common.comm.ResponseMessage;
import org.springframework.web.multipart.MultipartFile;

public interface IOssService {

    String putObject(MultipartFile file, String path);

    boolean putForumPostHtmlFile(String path, String localFilePath);

    ResponseMessage deleteObject(String url);

}
