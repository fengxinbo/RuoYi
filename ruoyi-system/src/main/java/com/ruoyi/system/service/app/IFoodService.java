package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.Foods;

import java.util.List;

public interface IFoodService {

    List<Foods> selectFoods(Foods foods);

    boolean checkFoodsNameUnique(Foods foods);

    int insertOrUpdateFoods(Foods foods);

    int deleteFoodsByIds(String ids);

    Foods selectFoodsById(Integer fid);

    /**
     * 导入食物数据
     *
     * @param foodsList 食物数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    String importFoods(List<Foods> foodsList, Boolean isUpdateSupport, String operName);

    int promoteFoods(Foods foods, String createdBy);

}
