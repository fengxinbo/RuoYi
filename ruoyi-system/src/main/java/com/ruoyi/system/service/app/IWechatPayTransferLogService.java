package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.WechatPayTransferLog;

import java.util.List;

public interface IWechatPayTransferLogService {
    List<WechatPayTransferLog> selectTransferLog(WechatPayTransferLog wechatPayTransferLog);
}
