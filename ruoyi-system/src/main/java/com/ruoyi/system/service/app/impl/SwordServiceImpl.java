package com.ruoyi.system.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.system.domain.app.Sword;
import com.ruoyi.system.mapper.app.SwordMapper;
import com.ruoyi.system.service.app.ISwordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SwordServiceImpl implements ISwordService {

    @Resource
    private SwordMapper swordMapper;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<Sword> selectSword(Sword sword) {
        LambdaQueryWrapper<Sword> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(sword.getSword() != null, Sword::getSword, sword.getSword());
        return swordMapper.selectList(queryWrapper);
    }

    @Override
    public int changeState(Sword sword) {
        return swordMapper.updateById(sword);
    }

    @Override
    public int deleteSword(String id) {
        return swordMapper.deleteById(id);
    }

    @Override
    public int insertSword(Sword sword) {
        return swordMapper.insert(sword);
    }

    @Override
    public int refresh() {
        String url = "http://localhost:8763/tools/sword/refresh";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("X-Is-Local", "true");
        return restTemplate.postForObject(url, new HttpEntity<>(null, headers), int.class);
    }
}
