package com.ruoyi.system.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.app.UserOperLog;
import com.ruoyi.system.mapper.app.UserOperLogMapper;
import com.ruoyi.system.service.app.IUserOperLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class UserOperLogServiceImpl implements IUserOperLogService {

    @Autowired
    private UserOperLogMapper userOperLogMapper;

    @Override
    public List<UserOperLog> selectUserOperLog(UserOperLog userOperLog) {
        if (StringUtils.isNotBlank(userOperLog.getUid()) || StringUtils.isNotBlank(userOperLog.getPhone()) || StringUtils.isNotBlank(userOperLog.getOperaction()) || StringUtils.isNotBlank(userOperLog.getMessage()) || StringUtils.isNotBlank(userOperLog.getPhonetype()) || StringUtils.isNotBlank(userOperLog.getNickname()))
            return userOperLogMapper.selectUserOperLog(userOperLog);
        else return new ArrayList<>();
    }

    @Override
    public List<UserOperLog> selectUserOperLog(String uid) {
        return userOperLogMapper.selectList(new LambdaQueryWrapper<UserOperLog>().eq(UserOperLog::getUid, uid).select(UserOperLog::getCtime, UserOperLog::getOperpath, UserOperLog::getOperaction, UserOperLog::getInputparams, UserOperLog::getOutputparams).orderByAsc(UserOperLog::getCtime));
    }
}
