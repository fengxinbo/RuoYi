package com.ruoyi.system.service.app.impl;

import com.ruoyi.system.domain.app.JPushAlert;
import com.ruoyi.system.mapper.app.JPushAlertMapper;
import com.ruoyi.system.service.app.IJPushAlertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JPushAlertServiceImpl implements IJPushAlertService {

    @Autowired
    private JPushAlertMapper jPushAlertMapper;

    @Override
    public int insertJPushAlert(JPushAlert jPushAlert) {
        return jPushAlertMapper.insert(jPushAlert);
    }

    @Override
    public int updateJPushAlert(JPushAlert jPushAlert) {
        return jPushAlertMapper.updateById(jPushAlert);
    }

    @Override
    public int deleteJPushAlert(Integer id) {
        return jPushAlertMapper.deleteById(id);
    }

    @Override
    public JPushAlert selectJPushAlertById(Integer id) {
        return jPushAlertMapper.selectById(id);
    }

    @Override
    public List<JPushAlert> selectJPushAlert(JPushAlert jPushAlert) {
        return jPushAlertMapper.selectList(null);
    }
}
