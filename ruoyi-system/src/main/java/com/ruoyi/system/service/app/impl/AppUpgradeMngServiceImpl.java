package com.ruoyi.system.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.system.domain.app.AppUpgradeMng;
import com.ruoyi.system.mapper.app.AppUpgradeMngMapper;
import com.ruoyi.system.service.app.IAppUpgradeMngService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppUpgradeMngServiceImpl implements IAppUpgradeMngService {

    @Autowired
    private AppUpgradeMngMapper appUpgradeMngMapper;

    @Override
    public List<AppUpgradeMng> selectAppUpgradeMngList() {
        LambdaQueryWrapper<AppUpgradeMng> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByAsc(AppUpgradeMng::getPhoneType);
        return appUpgradeMngMapper.selectList(queryWrapper);
    }

    @Override
    public AppUpgradeMng selectAppUpgradeMngById(Integer phoneType) {
        return appUpgradeMngMapper.selectById(phoneType);
    }

    @Override
    public int updateAppUpgradeMng(AppUpgradeMng appUpgradeMng) {
        return appUpgradeMngMapper.updateById(appUpgradeMng);
    }
}
