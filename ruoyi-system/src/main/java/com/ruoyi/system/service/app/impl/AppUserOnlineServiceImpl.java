package com.ruoyi.system.service.app.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.system.mapper.app.AppUserOnlineMapper;
import com.ruoyi.system.service.app.IAppUserOnlineService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class AppUserOnlineServiceImpl implements IAppUserOnlineService {

    @Resource
    private AppUserOnlineMapper appUserOnlineMapper;

    @Override
    public String getIndexOL() {
        Object [] xAxisArray = new Object[]{};
        Object [] yAxisArray = new Object[]{};
        List<Map<String, Object>> mapList = appUserOnlineMapper.selectIndexOL();
        if (mapList != null && !mapList.isEmpty()) {
            xAxisArray = mapList.stream().map(m -> m.get("ctime")).toArray();
            yAxisArray = mapList.stream().map(m -> m.get("online")).toArray();
        }
        JSONArray colorArray = new JSONArray();
        colorArray.add("#5AC4F9");

        JSONArray dataArray = new JSONArray();
        dataArray.add("在线人数");
        JSONObject legend = new JSONObject();
        legend.put("data", dataArray);

        JSONObject xAxisObj = new JSONObject();
        xAxisObj.put("type", "category");
        xAxisObj.put("data", xAxisArray);
        JSONObject axisLabel = new JSONObject();
        axisLabel.put("show", true);
        axisLabel.put("rotate", 45);
        axisLabel.put("margin", 12);
        xAxisObj.put("axisLabel", axisLabel);
        JSONArray xAxisJSONArray = new JSONArray();
        xAxisJSONArray.add(xAxisObj);

        JSONObject seriesObj = new JSONObject();
        seriesObj.put("name", "在线人数");
        seriesObj.put("type", "bar");
        seriesObj.put("data", yAxisArray);
        JSONArray yAxisJSONArray = new JSONArray();
        yAxisJSONArray.add(seriesObj);

        JSONObject tooltip = new JSONObject();
        tooltip.put("trigger", "axis");

        JSONObject option = new JSONObject();
        option.put("legend", legend);
        option.put("xAxis", xAxisJSONArray);
        option.put("yAxis", new JSONObject());
        option.put("series", yAxisJSONArray);
        option.put("color", colorArray);
        option.put("tooltip", tooltip);

        return option.toJSONString();
    }
}
