package com.ruoyi.system.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.config.datasource.DynamicDataSourceContextHolder;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.app.ForumTutorialMenu1;
import com.ruoyi.system.domain.app.ForumTutorialMenu2;
import com.ruoyi.system.mapper.app.ForumTutorialMenu1Mapper;
import com.ruoyi.system.service.app.ForumTutorialMenu1Service;
import com.ruoyi.system.service.app.ForumTutorialMenu2Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class ForumTutorialMenu1ServiceImpl implements ForumTutorialMenu1Service {

    @Autowired
    private ForumTutorialMenu1Mapper forumTutorialMenu1Mapper;

    @Autowired
    private ForumTutorialMenu2Service forumTutorialMenu2Service;

    @Override
    public List<ForumTutorialMenu1> list() {
        List<ForumTutorialMenu1> menu1List = forumTutorialMenu1Mapper.selectList(new LambdaQueryWrapper<ForumTutorialMenu1>().orderByAsc(ForumTutorialMenu1::getSn));
        menu1List.stream().forEach(menu1 -> {
            if (menu1.getMaterial() != null)
                if (menu1.getMaterial().getImage() != null)
                    if (menu1.getMaterial().getImage().size() > 0)
                        menu1.setCover(menu1.getMaterial().getImage().get(0).getOssUrl());
        });
        return menu1List;
    }

    @Override
    public int add(ForumTutorialMenu1 forumTutorialMenu1) {
        return forumTutorialMenu1Mapper.insert(forumTutorialMenu1);
    }

    @Override
    public int update(ForumTutorialMenu1 forumTutorialMenu1) {
        return forumTutorialMenu1Mapper.updateById(forumTutorialMenu1);
    }

    @Override
    @Transactional
    public int delete(String id) {
        List<ForumTutorialMenu2> list = forumTutorialMenu2Service.list(id);
        list.stream().forEach(menu2 -> forumTutorialMenu2Service.delete(menu2.getId()));
        DynamicDataSourceContextHolder.setDataSourceType(DataSourceType.SLAVE.name());
        int affectedRows = forumTutorialMenu1Mapper.deleteById(id);
        DynamicDataSourceContextHolder.clearDataSourceType();
        return affectedRows;
    }

    @Override
    public ForumTutorialMenu1 selectById(String id) {
        return forumTutorialMenu1Mapper.selectById(id);
    }

}