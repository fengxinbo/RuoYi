package com.ruoyi.system.service.app;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.system.domain.app.AppUser;
import com.ruoyi.system.domain.app.AppUserExt;
import com.ruoyi.system.domain.app.UserSuggestion;

import java.util.List;

public interface AppUserService {

    List<AppUser> selectAppUserList(AppUser user);

    /**
     * 用户状态修改
     *
     * @param user 用户信息
     * @return 结果
     */
    int changeStatus(AppUser user);

    int t1switch(String uid, Integer ulevel);

    List<UserSuggestion> selectUserSuggestion(UserSuggestion userSuggestion);

    UserSuggestion selectUserSuggestionById(String id);

    JSONObject showSuggestionPics(String id);

    int processSuggestion(String id);

    int replySuggestion(UserSuggestion userSuggestion);

    int deleteMsgById(String id);

    String getIndexAge();

    String getIndexDNU();

    String getIndexDAU();

    String getGenderPie();

    String getIndexMissedFoods();

    AppUser selectUserByUid(String uid);

    AppUserExt selectUserExtByUid(String uid);

    int insertDummyUser(AppUser user);

    int updateDummyUser(AppUser user);

    int deleteDummyUserByUid(String uid);

    AppUser selectDummyUserByUid(String uid);

    List<AppUser> selectOfficialUserList();

    int changeVisibleScope(AppUser user);

    int changeForumAuthorRating(AppUser user);

}
