package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.JPushAlert;

import java.util.List;

public interface IJPushAlertService {

    int insertJPushAlert(JPushAlert jPushAlert);

    int updateJPushAlert(JPushAlert jPushAlert);

    int deleteJPushAlert(Integer id);

    JPushAlert selectJPushAlertById(Integer id);

    List<JPushAlert> selectJPushAlert(JPushAlert jPushAlert);

}
