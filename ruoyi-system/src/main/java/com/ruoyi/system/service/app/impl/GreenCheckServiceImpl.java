package com.ruoyi.system.service.app.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.constant.AppConstants;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.app.GreenCheck;
import com.ruoyi.system.mapper.app.GreenCheckMapper;
import com.ruoyi.system.service.app.IGreenCheckService;
import com.ruoyi.system.service.app.IOssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class GreenCheckServiceImpl implements IGreenCheckService {

    @Autowired
    private IOssService ossService;

    @Autowired
    private GreenCheckMapper greenCheckMapper;

    @Override
    public List<GreenCheck> selectGreenCheckList(GreenCheck greenCheck) {
        return greenCheckMapper.selectGreenCheckList(greenCheck);
    }

    @Override
    public int disposalRisk(String id, String ossUrl) {
        int affectedRows = ignoreRisk(id);
        if (affectedRows > 0 && StringUtils.isNotBlank(ossUrl))
            ossService.deleteObject(ossUrl);
        return affectedRows;
    }

    @Override
    public int ignoreRisk(String id) {
        int affectedRows = 0;
        GreenCheck greenCheck = greenCheckMapper.selectById(id);
        if (greenCheck != null && greenCheck.getProcessed() == 0) {
            greenCheck.setProcessed(AppConstants.APP_FORUM_POST_CS_PROCESSED);
            greenCheck.setChecked(AppConstants.APP_FORUM_POST_CS_CHECKED);
            greenCheck.setEtime(LocalDateTime.now());
            affectedRows = greenCheckMapper.updateById(greenCheck);
        }
        return affectedRows;
    }
}
