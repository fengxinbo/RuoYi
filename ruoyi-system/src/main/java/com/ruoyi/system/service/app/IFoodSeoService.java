package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.FoodSeo;

import java.util.List;

public interface IFoodSeoService {
    List<FoodSeo> selectFoodSeo(FoodSeo foodSeo);

    int insertFoodSeo(FoodSeo foodSeo);

    int updateFoodSeo(FoodSeo foodSeo);

    int deleteFoodSeoById(String id);

    FoodSeo selectFoodSeoById(String id);
}
