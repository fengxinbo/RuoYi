package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.AppUpgradeMng;

import java.util.List;

public interface IAppUpgradeMngService {

    List<AppUpgradeMng> selectAppUpgradeMngList();

    AppUpgradeMng selectAppUpgradeMngById(Integer phoneType);

    int updateAppUpgradeMng(AppUpgradeMng appUpgradeMng);

}
