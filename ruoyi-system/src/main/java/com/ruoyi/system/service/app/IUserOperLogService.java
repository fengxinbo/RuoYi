package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.UserOperLog;

import java.util.List;

public interface IUserOperLogService {

    List<UserOperLog> selectUserOperLog(UserOperLog userOperLog);

    List<UserOperLog> selectUserOperLog(String uid);

}
