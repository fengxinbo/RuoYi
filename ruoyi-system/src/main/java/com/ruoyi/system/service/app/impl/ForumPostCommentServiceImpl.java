package com.ruoyi.system.service.app.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.app.ForumPost;
import com.ruoyi.system.domain.app.ForumPostComment;
import com.ruoyi.system.domain.app.ForumPostCommentReply;
import com.ruoyi.system.domain.app.ForumPostMsg;
import com.ruoyi.system.mapper.app.ForumPostCommentMapper;
import com.ruoyi.system.mapper.app.ForumPostCommentReplyMapper;
import com.ruoyi.system.mapper.app.ForumPostMapper;
import com.ruoyi.system.mapper.app.ForumPostMsgMapper;
import com.ruoyi.system.service.app.ForumPostCommentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@DataSource(DataSourceType.SLAVE)
public class ForumPostCommentServiceImpl implements ForumPostCommentService {

    @Autowired
    private ForumPostCommentMapper forumPostCommentMapper;

    @Autowired
    private ForumPostCommentReplyMapper forumPostCommentReplyMapper;

    @Autowired
    private ForumPostMsgMapper forumPostMsgMapper;

    @Autowired
    private ForumPostMapper forumPostMapper;

    @Override
    public List<ForumPostComment> list(Map<String, Object> criteria) {
        return forumPostCommentMapper.selectCommentList(criteria);
    }

    @Override
    public int onEditableSave(ForumPostComment comment) {
        int affectedRows = forumPostCommentMapper.updateById(comment);
        if (affectedRows > 0 && comment.getVisibleScope() == 1) {
            forumPostMsgMapper.delete(new LambdaQueryWrapper<ForumPostMsg>().eq(ForumPostMsg::getCommentId, comment.getId()));
        }
        return affectedRows;
    }

    @Override
    public ForumPostComment selectById(String id) {
        return forumPostCommentMapper.selectById(id);
    }

    @Override
    public int replyComment(ForumPostComment comment, String fromUid, String location, String content, String img) {
        ForumPostCommentReply reply = new ForumPostCommentReply();
        reply.setPostId(comment.getPostId());
        reply.setCommentId(comment.getId());
        reply.setFromUid(fromUid);
        reply.setToUid(comment.getFromUid());
        reply.setContent(content);
        reply.setLocation(location);
        if (StrUtil.isNotBlank(img))
            reply.setImage(Arrays.asList(img));
        int affectedRows = forumPostCommentReplyMapper.insert(reply);
        if (affectedRows > 0) {
            // 自己回复自己的评论不需要给自己发通知
            if (!fromUid.equalsIgnoreCase(comment.getFromUid())) {
                // 如果被回复的人和评论的人不是同一个人，才发通知
                ForumPostMsg commenterMsg = new ForumPostMsg();
                commenterMsg.setPostId(reply.getPostId());
                commenterMsg.setCommentId(reply.getCommentId());
                commenterMsg.setReplyId(reply.getId());
                commenterMsg.setParentId(reply.getCommentId());
                commenterMsg.setType(2);// 回复
                commenterMsg.setFromUid(fromUid);
                commenterMsg.setToUid(comment.getFromUid());
                commenterMsg.setParentUid(comment.getFromUid());
                commenterMsg.setTitle("回复了你的评论");
                forumPostMsgMapper.insert(commenterMsg);
            }

            ForumPost post = forumPostMapper.selectById(comment.getPostId());
            if (post != null) {
                if (!fromUid.equalsIgnoreCase(post.getUid())) {
                    // 自己回复自己的帖子不需要给自己发通知
                    ForumPostMsg posterMsg = new ForumPostMsg();
                    posterMsg.setPostId(post.getId());
                    posterMsg.setCommentId(comment.getId());
                    posterMsg.setReplyId(reply.getId());
                    posterMsg.setParentId(comment.getId());
                    posterMsg.setType(2);// 回复
                    posterMsg.setFromUid(fromUid);
                    posterMsg.setToUid(post.getUid());
                    posterMsg.setTitle("回复了你的帖子");
                    posterMsg.setParentUid(comment.getFromUid());
                    forumPostMsgMapper.insert(posterMsg);
                }
            }
        }
        return affectedRows;
    }

    @Override
    public int replyReply(ForumPostCommentReply parent, String fromUid, String location, String content, String img) {
        ForumPostCommentReply reply = new ForumPostCommentReply();
        reply.setPostId(parent.getPostId());
        reply.setCommentId(parent.getCommentId());
        reply.setParentId(parent.getId());
        reply.setFromUid(fromUid);
        reply.setToUid(parent.getFromUid());
        reply.setContent(content);
        reply.setLocation(location);
        if (StrUtil.isNotBlank(img))
            reply.setImage(Arrays.asList(img));
        int affectedRows = forumPostCommentReplyMapper.insert(reply);
        if (affectedRows > 0) {
            if (!fromUid.equalsIgnoreCase(parent.getFromUid())) {
                // 自己回复自己不需要给自己发通知
                ForumPostMsg respondentMsg = new ForumPostMsg();
                respondentMsg.setPostId(parent.getPostId());
                respondentMsg.setCommentId(parent.getCommentId());
                respondentMsg.setParentId(parent.getId());
                respondentMsg.setType(2);
                respondentMsg.setFromUid(fromUid);
                respondentMsg.setToUid(parent.getFromUid());
                respondentMsg.setTitle("回复了你的回复");
                respondentMsg.setParentUid(parent.getFromUid());
                forumPostMsgMapper.insert(respondentMsg);
            }

            ForumPostComment comment = forumPostCommentMapper.selectById(parent.getCommentId());
            if (comment != null) {
                // 自己回复自己的评论不需要给自己发通知
                if (!fromUid.equalsIgnoreCase(comment.getFromUid())) {
                    // 如果被回复的人和评论的人不是同一个人，才发通知
                    ForumPostMsg commenterMsg = new ForumPostMsg();
                    commenterMsg.setPostId(reply.getPostId());
                    commenterMsg.setCommentId(reply.getCommentId());
                    commenterMsg.setReplyId(reply.getId());
                    commenterMsg.setParentId(reply.getCommentId());
                    commenterMsg.setType(2);// 回复
                    commenterMsg.setFromUid(fromUid);
                    commenterMsg.setToUid(comment.getFromUid());
                    commenterMsg.setParentUid(comment.getFromUid());
                    commenterMsg.setTitle("回复了你的评论");
                    forumPostMsgMapper.insert(commenterMsg);
                }

            }

            ForumPost post = forumPostMapper.selectById(comment.getPostId());
            if (post != null) {
                if (!fromUid.equalsIgnoreCase(post.getUid())) {
                    // 自己回复自己的帖子不需要给自己发通知
                    ForumPostMsg posterMsg = new ForumPostMsg();
                    posterMsg.setPostId(post.getId());
                    posterMsg.setCommentId(comment.getId());
                    posterMsg.setReplyId(reply.getId());
                    posterMsg.setParentId(comment.getId());
                    posterMsg.setType(2);// 回复
                    posterMsg.setFromUid(fromUid);
                    posterMsg.setToUid(post.getUid());
                    posterMsg.setTitle("回复了你的帖子");
                    posterMsg.setParentUid(comment.getFromUid());
                    forumPostMsgMapper.insert(posterMsg);
                }
            }

        }
        return affectedRows;
    }

}
