package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.ForumPostComment;
import com.ruoyi.system.domain.app.ForumPostCommentReply;

import java.util.List;
import java.util.Map;

public interface ForumPostCommentService {

    List<ForumPostComment> list(Map<String, Object> criteria);

    int onEditableSave(ForumPostComment comment);

    ForumPostComment selectById(String id);

    int replyComment(ForumPostComment comment, String fromUid, String location, String content, String img);

    int replyReply(ForumPostCommentReply reply, String fromUid, String location, String content, String img);

}