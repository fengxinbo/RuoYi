package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.Sword;

import java.util.List;

public interface ISwordService {

    List<Sword> selectSword(Sword sword);

    int changeState(Sword sword);

    int deleteSword(String id);

    int insertSword(Sword sword);

    int refresh();
}
