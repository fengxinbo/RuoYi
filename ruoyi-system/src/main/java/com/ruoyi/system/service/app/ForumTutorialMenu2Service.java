package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.ForumTutorialMenu2;

import java.util.List;

public interface ForumTutorialMenu2Service {

    List<ForumTutorialMenu2> list(String parentId);

    int add(ForumTutorialMenu2 forumTutorialMenu2);

    int update(ForumTutorialMenu2 forumTutorialMenu2);

    int delete(String id);

    ForumTutorialMenu2 selectById(String id);

}