package com.ruoyi.system.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.system.domain.app.*;
import com.ruoyi.system.mapper.app.*;
import com.ruoyi.system.service.app.ForumPostLikeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Slf4j
@Service
@DataSource(DataSourceType.SLAVE)
public class ForumPostLikeServiceImpl implements ForumPostLikeService {

    @Autowired
    private AppUserExtMapper appUserExtMapper;

    @Autowired
    private ForumPostLikeMapper forumPostLikeMapper;

    @Autowired
    private ForumPostMsgMapper forumPostMsgMapper;

    @Autowired
    private ForumPostMapper forumPostMapper;

    @Autowired
    private ForumPostCommentMapper forumPostCommentMapper;

    @Autowired
    private ForumPostCommentReplyMapper forumPostCommentReplyMapper;

    @Override
    @Transactional
    public int add(ForumPostLike like) {
        int affectedRows = 0;
        if (like.getLikeCount() == null) {
            try {
                affectedRows = forumPostLikeMapper.insert(like);
                if (affectedRows > 0)
                    sendLikeMsg(like.getBizType(), like.getBizId(), like.getUid());
            } catch (Exception e) {
                if (e instanceof DuplicateKeyException)
                    throw new ServiceException("重复点赞");
            }
        } else {
            affectedRows = insertLikes(like.getBizType(), like.getBizId(), like.getLikeCount());
        }
        return affectedRows;
    }

    private int insertLikes(Integer bizType, String bizId, Integer likeCount) {
        List<AppUserExt> userExtList = appUserExtMapper.selectList(new LambdaQueryWrapper<AppUserExt>().eq(AppUserExt::getDummy, 2));
        Collections.shuffle(userExtList);

        if (userExtList.size() < likeCount) {
            throw new ServiceException(String.format("特殊帐号不足，需要%s，实际只有%s", likeCount, userExtList.size()));
        }

        int insertedCount = 0;
        for (int i = 0; i < userExtList.size() && insertedCount < likeCount; i++) {
            AppUserExt userExt = userExtList.get(i);
            ForumPostLike like = new ForumPostLike();
            like.setBizType(bizType);
            like.setBizId(bizId);
            like.setUid(userExt.getUid());
            try {
                int affectedRows = forumPostLikeMapper.insert(like);
                if (affectedRows == 1) {
                    insertedCount++;
                    sendLikeMsg(bizType, bizId, userExt.getUid());
                }
            } catch (DuplicateKeyException e) {
                log.debug("重复点赞，继续尝试下一个用户");
            } catch (Exception e) {
                throw new ServiceException("插入点赞记录时发生错误");
            }
        }

        if (insertedCount < likeCount)
            log.debug("特殊帐号不足");

        return insertedCount;
    }

    private void sendLikeMsg(Integer bizType, String bizId, String uid) {
        if (bizType == 1) {
            // 帖子点赞
            ForumPost post = forumPostMapper.selectById(bizId);
            if (post != null) {
                if (forumPostMsgMapper.selectCount(new LambdaQueryWrapper<ForumPostMsg>().eq(ForumPostMsg::getLikeBizType, bizType).eq(ForumPostMsg::getPostId, bizId).eq(ForumPostMsg::getToUid, post.getUid()).eq(ForumPostMsg::getFromUid, uid)) < 1) {
                    ForumPostMsg msg = new ForumPostMsg();
                    msg.setPostId(bizId);
                    msg.setToUid(post.getUid());
                    msg.setTitle("赞了你的帖子");
                    msg.setParentId(bizId);
                    msg.setType(3);// 点赞
                    msg.setLikeBizType(bizType);
                    msg.setFromUid(uid);
                    forumPostMsgMapper.insert(msg);
                }
            }
        } else if (bizType == 2) {
            // 评论点赞
            ForumPostComment comment = forumPostCommentMapper.selectById(bizId);
            if (comment != null) {
                if (forumPostMsgMapper.selectCount(new LambdaQueryWrapper<ForumPostMsg>().eq(ForumPostMsg::getLikeBizType, bizType).eq(ForumPostMsg::getCommentId, bizId).eq(ForumPostMsg::getToUid, comment.getFromUid()).eq(ForumPostMsg::getFromUid, uid)) < 1) {
                    ForumPostMsg msg = new ForumPostMsg();
                    msg.setPostId(comment.getPostId());
                    msg.setCommentId(bizId);
                    msg.setToUid(comment.getFromUid());
                    msg.setTitle("赞了你的评论");
                    msg.setParentId(bizId);
                    msg.setType(3);// 点赞
                    msg.setLikeBizType(bizType);
                    msg.setFromUid(uid);
                    forumPostMsgMapper.insert(msg);
                }
            }
        } else if (bizType == 3) {
            // 回复点赞
            ForumPostCommentReply reply = forumPostCommentReplyMapper.selectById(bizId);
            if (reply != null) {
                if (forumPostMsgMapper.selectCount(new LambdaQueryWrapper<ForumPostMsg>().eq(ForumPostMsg::getLikeBizType, bizType).eq(ForumPostMsg::getReplyId, bizId).eq(ForumPostMsg::getToUid, reply.getFromUid()).eq(ForumPostMsg::getFromUid, uid)) < 1) {
                    ForumPostMsg msg = new ForumPostMsg();
                    msg.setPostId(reply.getPostId());
                    msg.setCommentId(reply.getCommentId());
                    msg.setReplyId(bizId);
                    msg.setToUid(reply.getFromUid());
                    msg.setTitle("赞了你的回复");
                    msg.setParentId(bizId);
                    msg.setType(3);// 点赞
                    msg.setLikeBizType(bizType);
                    msg.setFromUid(uid);
                    forumPostMsgMapper.insert(msg);
                }
            }
        }
    }
}
