package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.UserInvitationLog;
import com.ruoyi.system.domain.app.UserInvitationLogStatistics;

import java.util.List;

public interface IUserInvitationLogService {

    List<UserInvitationLog> selectUserInvitationLog(UserInvitationLog userInvitationLog);

    List<UserInvitationLogStatistics> selectStatistics(UserInvitationLogStatistics userInvitationLogStatistics);

}
