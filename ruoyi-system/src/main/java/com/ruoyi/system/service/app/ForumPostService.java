package com.ruoyi.system.service.app;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.system.domain.app.ForumPost;

import java.util.List;

public interface ForumPostService {

    List<ForumPost> query(ForumPost forumPost);

    int create(ForumPost forumPost);

    int createDummy(ForumPost forumPost);

    int update(ForumPost forumPost);

    int delete(String id);

    int deleteComment(String id);

    int deleteReplyOnAccountDeletion(String id);

    ForumPost selectById(String id);

    JSONObject showPostCover(String id);

    int replyPost(ForumPost post, String fromUid, String location, String content, String img);

}