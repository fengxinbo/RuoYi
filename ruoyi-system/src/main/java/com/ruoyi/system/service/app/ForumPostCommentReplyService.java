package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.ForumPostCommentReply;

import java.util.List;
import java.util.Map;

public interface ForumPostCommentReplyService {

    List<ForumPostCommentReply> list(Map<String, Object> criteria);

    int onEditableSave(ForumPostCommentReply reply);

    ForumPostCommentReply selectById(String id);

}