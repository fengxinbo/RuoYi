package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.AccountDeletionLog;

import java.util.List;

public interface IAccountDeletionLogService {

    List<AccountDeletionLog> selectAccountDeletionLogList(AccountDeletionLog accountDeletionLog);

    int purgeData(String id);

}
