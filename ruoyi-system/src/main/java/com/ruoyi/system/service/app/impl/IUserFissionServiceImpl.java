package com.ruoyi.system.service.app.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.app.UserFission;
import com.ruoyi.system.domain.app.UserFissionStatistics;
import com.ruoyi.system.mapper.app.UserFissionMapper;
import com.ruoyi.system.service.app.IUserFissionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class IUserFissionServiceImpl implements IUserFissionService {

    @Resource
    private UserFissionMapper userFissionMapper;

    @Override
    public List<UserFission> selectUserFission(UserFission userFission) {
        return userFissionMapper.selectUserFission(userFission);
    }

    @Override
    public List<UserFissionStatistics> selectStatistics(UserFissionStatistics userFissionStatistics) {
        return userFissionMapper.selectStatistics(userFissionStatistics);
    }
}
