package com.ruoyi.system.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.app.FoodSeo;
import com.ruoyi.system.domain.app.Foods;
import com.ruoyi.system.mapper.app.FoodSeoMapper;
import com.ruoyi.system.mapper.app.FoodsMapper;
import com.ruoyi.system.service.app.IFoodSeoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class FoodSeoServiceImpl implements IFoodSeoService {

    @Autowired
    private FoodsMapper foodsMapper;

    @Autowired
    private FoodSeoMapper foodSeoMapper;

    @Override
    public List<FoodSeo> selectFoodSeo(FoodSeo foodSeo) {
        LambdaQueryWrapper<FoodSeo> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(foodSeo.getHotword()))
            queryWrapper.eq(FoodSeo::getHotword, foodSeo.getHotword());
        queryWrapper.orderByAsc(FoodSeo::getHotword).orderByAsc(FoodSeo::getSn);
        return foodSeoMapper.selectList(queryWrapper);
    }

    @Override
    public int insertFoodSeo(FoodSeo foodSeo) {
        Foods foods = foodsMapper.selectById(foodSeo.getFid());
        if (foods == null)
            throw new ServiceException(String.format("fid【%s】不存在", foodSeo.getFid()));
        LambdaQueryWrapper<FoodSeo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(FoodSeo::getHotword, foodSeo.getHotword()).eq(FoodSeo::getFid, foodSeo.getFid());
        if (foodSeoMapper.selectCount(queryWrapper) > 0)
            throw new ServiceException(String.format("重复的SEO【%s】", foods.getFname()));
        foodSeo.setFname(foods.getFname());
        return foodSeoMapper.insert(foodSeo);
    }

    @Override
    public int updateFoodSeo(FoodSeo foodSeo) {
        return foodSeoMapper.updateById(foodSeo);
    }

    @Override
    public int deleteFoodSeoById(String id) {
        return foodSeoMapper.deleteById(id);
    }

    @Override
    public FoodSeo selectFoodSeoById(String id) {
        return foodSeoMapper.selectById(id);
    }
}
