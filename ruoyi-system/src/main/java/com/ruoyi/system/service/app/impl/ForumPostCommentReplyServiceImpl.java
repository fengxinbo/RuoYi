package com.ruoyi.system.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.app.ForumPostCommentReply;
import com.ruoyi.system.domain.app.ForumPostMsg;
import com.ruoyi.system.mapper.app.ForumPostCommentReplyMapper;
import com.ruoyi.system.mapper.app.ForumPostMsgMapper;
import com.ruoyi.system.service.app.ForumPostCommentReplyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
@DataSource(DataSourceType.SLAVE)
public class ForumPostCommentReplyServiceImpl implements ForumPostCommentReplyService {

    @Autowired
    private ForumPostCommentReplyMapper forumPostCommentReplyMapper;

    @Autowired
    private ForumPostMsgMapper forumPostMsgMapper;

    @Override
    public List<ForumPostCommentReply> list(Map<String, Object> criteria) {
        return forumPostCommentReplyMapper.selectReplyList(criteria);
    }

    @Override
    public int onEditableSave(ForumPostCommentReply reply) {
        int affectedRows = forumPostCommentReplyMapper.updateById(reply);
        if (affectedRows > 0 && reply.getVisibleScope() == 1) {
            forumPostMsgMapper.delete(new LambdaQueryWrapper<ForumPostMsg>().eq(ForumPostMsg::getReplyId, reply.getId()));
        }
        return affectedRows;
    }

    @Override
    public ForumPostCommentReply selectById(String id) {
        return forumPostCommentReplyMapper.selectById(id);
    }

}
