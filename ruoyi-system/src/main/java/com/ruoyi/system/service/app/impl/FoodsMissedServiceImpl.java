package com.ruoyi.system.service.app.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.app.FoodsMissed;
import com.ruoyi.system.mapper.app.FoodsMissedMapper;
import com.ruoyi.system.service.app.IFoodsMissedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Service
@DataSource(DataSourceType.SLAVE)
public class FoodsMissedServiceImpl implements IFoodsMissedService {

    @Autowired
    private FoodsMissedMapper foodsMissedMapper;

    @Override
    public List<Map<String, Object>> selectIndexMissedFoods() {
        return foodsMissedMapper.selectIndexMissedFoods();
    }

    @Override
    public int resolveMissedFood(String fname) {
        int affectedRows = 0;
        FoodsMissed foodsMissed = foodsMissedMapper.selectById(fname);
        if (foodsMissed != null) {
            foodsMissed.setResolved(1);
            foodsMissed.setEtime(LocalDateTime.now());
            affectedRows = foodsMissedMapper.updateById(foodsMissed);
        }
        return affectedRows;
    }
}
