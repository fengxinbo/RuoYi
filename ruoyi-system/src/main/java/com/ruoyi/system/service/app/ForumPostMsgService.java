package com.ruoyi.system.service.app;

import com.ruoyi.system.domain.app.ForumPostMsg;

public interface ForumPostMsgService {

    int insert(ForumPostMsg msg);

}