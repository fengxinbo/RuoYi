package com.ruoyi.system.domain.app;

import lombok.Data;

@Data
public class UserInvitationLogStatistics {

    private String nickname;

    private Integer invitees;

    private Double totalcommission;

    private Double withdrawal;

    private Double balance;

}
