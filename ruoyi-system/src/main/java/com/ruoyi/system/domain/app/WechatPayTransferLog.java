package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_wechatpay_transfer_log")
public class WechatPayTransferLog extends Model<WechatPayTransferLog> {

    @TableId(type = IdType.INPUT, value = "batch_id")
    private String batchId;

    private String uid;

    private String mchid;

    @TableField(value = "out_batch_no")
    private String outBatchNo;

    private String appid;

    @TableField(value = "batch_status")
    private String batchStatus;

    @TableField(value = "batch_type")
    private String batchType;

    @TableField(value = "batch_name")
    private String batchName;

    @TableField(value = "batch_remark")
    private String batchRemark;

    @TableField(value = "close_reason")
    private String closeReason;

    @TableField(value = "total_amount")
    private Long totalAmount;

    @TableField(value = "total_num")
    private Integer totalNum;

    @TableField(value = "success_amount")
    private Long successAmount;

    @TableField(value = "success_num")
    private Integer successNum;

    @TableField(value = "fail_amount")
    private Long failAmount;

    @TableField(value = "fail_num")
    private Integer failNum;

    @TableField(value = "transfer_scene_id")
    private String transferSceneId;

    private Integer compensate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

}
