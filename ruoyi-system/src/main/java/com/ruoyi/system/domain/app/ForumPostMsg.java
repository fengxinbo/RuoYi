package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@TableName(value = "t_forum_post_msg", autoResultMap = true)
public class ForumPostMsg extends Model<ForumPostMsg> {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    @TableField(value = "post_id")
    private String postId;

    @TableField(value = "comment_id")
    private String commentId;

    @TableField(value = "reply_id")
    private String replyId;

    @TableField(value = "parent_id")
    private String parentId;

    private Integer type;

    @TableField(value = "read_flag")
    private Integer readFlag;

    @TableField(value = "from_uid")
    private String fromUid;

    @TableField(exist = false)
    private String fromNickname;

    @TableField(exist = false)
    private String fromAvatar;

    @TableField(value = "from_role")
    private String fromRole;

    @TableField(value = "to_uid")
    private String toUid;

    @TableField(exist = false, typeHandler = FastjsonTypeHandler.class)
    private List<String> postCover;

    private String title;

    private String content;

    @TableField(exist = false)
    private String parentContent;

    @TableField(exist = false, typeHandler = FastjsonTypeHandler.class)
    private List<String> image;

    @TableField(exist = false, typeHandler = FastjsonTypeHandler.class)
    private List<String> parentImage;

    @TableField(value = "like_biz_type")
    private Integer likeBizType;

    @TableField(exist = false)
    private Integer like;

    @TableField(value = "parent_uid")
    private String parentUid;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

}