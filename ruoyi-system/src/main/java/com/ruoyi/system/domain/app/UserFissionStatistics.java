package com.ruoyi.system.domain.app;

import lombok.Data;

@Data
public class UserFissionStatistics {

    private String uid;

    private String nickname;

    private Integer ulevel;

    private Integer invitees;

    private Double t1commission;

    private Double t2commission;

    private Double totalcommission;

    private Double withdrawal;

    private Double balance;

}
