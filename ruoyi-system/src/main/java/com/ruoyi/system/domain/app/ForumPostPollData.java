package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@TableName(value = "t_forum_post_poll_data", autoResultMap = true)
public class ForumPostPollData extends Model<ForumPostPollData> {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    @TableField(value = "post_id")
    private String postId;

    private String uid;

    @TableField(value = "poll_type")
    private Integer pollType;

    @TableField(value = "poll_option", typeHandler = FastjsonTypeHandler.class)
    private List<String> pollOption;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

}