package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@TableName(value = "t_forum_post_comment", autoResultMap = true)
public class ForumPostComment extends Model<ForumPostComment> {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    @TableField(value = "post_id")
    private String postId;

    @TableField(value = "comment_id")
    private String commentId;

    @TableField(value = "parent_id")
    private String parentId;

    private Integer type;

    @TableField(value = "from_uid")
    private String fromUid;

    @TableField(value = "to_uid")
    private String toUid;

    private String content;

    @TableField(value = "image", typeHandler = FastjsonTypeHandler.class)
    private List<String> image;

    private Integer sn;

    private String ip;

    private String location;

    @TableField(value = "visible_scope")
    private Integer visibleScope;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @TableField(exist = false)
    private String fromUser;

    @TableField(exist = false)
    private Integer dummy;

    @TableField(exist = false)
    private String toUser;

    @TableField(exist = false)
    private Integer likeCount;

    @TableField(exist = false)
    private Integer replyCount;

}