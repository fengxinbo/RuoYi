package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_meals_foods")
public class MealsFoods extends Model<MealsFoods> {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String pid;

    private Integer fid;

    private String fname;

    private Double qty;

    private String spec;

    private Double protein;

    private Double carbohydrate;

    private Double fat;

    private Double calories;

    @TableField(exist = false)
    private Foods foods;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

}
