package com.ruoyi.system.domain.app;

import lombok.Data;

@Data
public class ForumPostReportProcess {

    private String id;

    private String bizId;

    private Integer bizType;

    private String bizTypeStr;

    private Integer contentProcessType;

    private Integer userProcessType;

    private String contentPart1;

    private String contentPart2;

    private String contentPart3;

    private String content;

}