package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName(value = "t_forum_tutorial", autoResultMap = true)
public class ForumTutorial {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    @TableField(value = "parent_id")
    private String parentId;

    private String title;

    private String subtitle;

    @TableField(exist = false)
    private String materialStr;

    @TableField(value = "material", typeHandler = FastjsonTypeHandler.class)
    private Material material;

    @TableField(exist = false)
    private String ossUrl;

    private Integer sn;

    private String weblink;

    @TableField("created_by")
    private String createdBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @TableField("updated_by")
    private String updatedBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

    @TableField(exist = false)
    private Long clickCount;

}