package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_user_invitation_log")
public class UserInvitationLog {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String inviter;

    private String invitee;

    private Double money;

    private String remarks;

    private LocalDateTime ctime;

}
