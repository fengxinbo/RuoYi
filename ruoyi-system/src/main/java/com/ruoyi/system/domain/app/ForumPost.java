package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@TableName(value = "t_forum_post", autoResultMap = true)
public class ForumPost {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private Integer poster;

    private String uid;

    @TableField(exist = false)
    private String nickname;

    private Integer top;

    private Integer sn;

    @TableField(value = "tag", typeHandler = FastjsonTypeHandler.class)
    private List<String> tag;

    private Integer type;

    private String title;

    private String subtitle;

    @TableField(value = "cover", typeHandler = FastjsonTypeHandler.class)
    private List<String> cover;

    @TableField(exist = false)
    private String coverInfoStr;

    @TableField(value = "cover_info", typeHandler = FastjsonTypeHandler.class)
    private ForumPostCoverInfo coverInfo;

    private String content;

    @TableField("content_type")
    private Integer contentType;

    @TableField(value = "material", typeHandler = FastjsonTypeHandler.class)
    private Material material;

    @TableField("visible_scope")
    private Integer visibleScope;

    private Integer commentable;

    private Integer likable;

    @TableField("machine_audit")
    private Integer machineAudit;

    @TableField("manual_audit")
    private Integer manualAudit;

    private Integer audit;

    private String weblink;

    @TableField(value = "poll_json", typeHandler = FastjsonTypeHandler.class)
    private PollJson pollJson;

    private String ip;

    private String location;

    @TableField("impression_threshold")
    private Integer impressionThreshold;

    private Integer phase;

    @TableField("created_by")
    private String createdBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @TableField("updated_by")
    private String updatedBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

    @TableField(exist = false)
    private Integer commentCount;

    @TableField(exist = false)
    private Integer clickCount;

    @TableField(exist = false)
    private String clickRate;

}