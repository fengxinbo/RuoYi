package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_user_nut_goal_cc")
public class UserNutGoalCc {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String uid;

    private Integer sn;

    private Double proteins;

    private Double carbohydrates;
    
    private Double fats;
    
    private Integer calories;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

}
