package com.ruoyi.system.domain.app;

import lombok.Data;

@Data
public class MaterialItem {

    private Integer sn;

    private String ossUrl;

}
