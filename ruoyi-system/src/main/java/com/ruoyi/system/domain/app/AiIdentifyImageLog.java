package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("t_ai_identify_image_log")
public class AiIdentifyImageLog {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String uid;

    private String input;

    private String output;

    private String ctime;

}