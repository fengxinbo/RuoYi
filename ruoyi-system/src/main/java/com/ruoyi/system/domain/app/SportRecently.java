package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_sport_recently")
public class SportRecently {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String uid;

    @TableField(value = "menu_id")
    private String menuId;

    private String name;

    private Integer sn;

    private Double met;

    private Double weight;

    private Double duration;

    private Double calories;

    @TableField(value = "selected_count")
    private Long selectedCount;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

}