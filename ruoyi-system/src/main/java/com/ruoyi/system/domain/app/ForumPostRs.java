package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName(value = "t_forum_post_rs")
public class ForumPostRs {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private Double wt;

    @TableField(value = "time_score")
    private Double timeScore;

    private Double wu;

    @TableField(value = "like_score")
    private Double likeScore;

    private Double wc;

    @TableField(value = "comment_score")
    private Double commentScore;

    private Double score;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

}