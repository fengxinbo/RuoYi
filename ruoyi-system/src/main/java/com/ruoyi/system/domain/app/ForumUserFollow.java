package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "t_forum_user_follow")
public class ForumUserFollow {

    @TableField(value = "follower_id")
    private String followerId;

    @TableField(value = "followed_id")
    private String followedId;

    private String ctime;

}