package com.ruoyi.system.domain.app;

import lombok.Data;

import java.util.List;

@Data
public class PollOption {

    private Integer sn;

    private String content;

    private List<String> image;

}
