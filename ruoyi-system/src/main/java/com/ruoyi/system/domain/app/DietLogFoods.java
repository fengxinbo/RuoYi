package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_diet_log_foods")
public class DietLogFoods {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String pid;

    private Integer sn;

    private Integer fid;

    private String fname;

    private Integer qty;

    private String spec;

    private Double weight;

    private Double protein;

    private Double carbohydrate;

    private Double fat;

    private Integer calories;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

    @TableField(exist = false)
    private Foods foods;

}
