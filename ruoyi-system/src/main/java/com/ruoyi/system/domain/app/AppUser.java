package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@TableName("t_users")
public class AppUser {

    @TableId(value = "uid", type = IdType.ASSIGN_UUID)
    private String id;

    private String nickname;

    private Integer gender;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate birthday;

    private String idc;

    private String phone;

    private String invcode;

    private Integer regfrom;

    private String wxopenid;

    private String wxunionid;

    private String wxaccesstoken;

    private String wxrefreshtoken;

    private String headimgurl;

    private String appleid;

    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String token;

    @TableField(exist = false)
    private Double balance;

    private Integer state;

    private Integer ulevel;

    private Integer idle;

    @TableField(value = "key_user")
    private Integer keyUser;

    @TableField(exist = false)
    private Integer dummy;

    @TableField(exist = false)
    private Integer forumPostVisibleScope;

    @TableField(exist = false)
    private Integer forumCommentVisibleScope;

    @TableField(exist = false)
    private Double forumAuthorRating;

    @TableField(exist = false)
    private Long followers;

    @TableField(exist = false)
    private Long following;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

}
