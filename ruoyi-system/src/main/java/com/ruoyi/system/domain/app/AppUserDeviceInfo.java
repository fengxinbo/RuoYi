package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
@TableName("t_app_device_info")
public class AppUserDeviceInfo {

    @TableId(type = IdType.INPUT)
    private String uid;

    private String ptype;

    private String flavor;

    private String os;

    private String manufacturer;

    private String model;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate etime;

}
