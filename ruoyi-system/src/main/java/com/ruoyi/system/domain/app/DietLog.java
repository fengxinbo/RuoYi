package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@TableName("t_diet_log")
public class DietLog {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String uid;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate sdate;

    private Double protein;

    private Double carbohydrate;

    private Double fat;

    private Integer calories;

    private Double proteinden;

    private Double carbohydrateden;

    private Double fatden;

    private Integer caloriesden;

    private String notes;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

    @TableField(exist = false)
    private List<Object> foods;
}
