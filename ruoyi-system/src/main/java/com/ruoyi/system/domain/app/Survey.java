package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_survey")
public class Survey {

    @TableId(type = IdType.ASSIGN_UUID)
    private String sid;

    private String uid;

    private Integer gender;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDateTime birthday;

    private Integer height;

    private Double weight;

    private Integer goal;

    private Integer dailyact;

    private String bodyfat;

    private Integer dailymeals;

    private Integer planweeks;

    private String proteins;

    private String fats;

    private String carbohydrates;

    private String vegetables;

    private String fruits;

    private Integer dailyfoodsqty;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

}
