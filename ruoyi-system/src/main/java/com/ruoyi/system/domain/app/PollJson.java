package com.ruoyi.system.domain.app;

import lombok.Data;

import java.util.List;

@Data
public class PollJson {

    private Integer pollType;

    private Integer showResult;

    private List<PollOption> pollOption;

    private Integer optionThreshold;

    private Integer haveImage;

}
