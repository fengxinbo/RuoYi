package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
@TableName("t_streak")
public class Streak {

    @TableId(type = IdType.INPUT)
    private String uid;

    private Integer level;

    private Integer gap;

    private Integer streak;

    @TableField(value = "max_streak")
    private Integer maxStreak;

    @TableField(value = "protection_period")
    private Integer protectionPeriod;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate ctime;

}
