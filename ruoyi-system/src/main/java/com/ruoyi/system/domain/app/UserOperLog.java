package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_user_oper_log")
public class UserOperLog {

    @Excel(name = "操作时间", cellType = Excel.ColumnType.STRING)
    private LocalDateTime ctime;

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String uid;

    @TableField(exist = false)
    private String nickname;

    @TableField(exist = false)
    private String phone;

    private String clientip;

    @Excel(name = "接口地址", cellType = Excel.ColumnType.STRING)
    private String operpath;

    @Excel(name = "用户操作", cellType = Excel.ColumnType.STRING)
    private String operaction;

    private Integer affectedrows;

    private Long costtime;

    @Excel(name = "入参", cellType = Excel.ColumnType.STRING)
    private String inputparams;

    @Excel(name = "出参", cellType = Excel.ColumnType.STRING)
    private String outputparams;

    private String phonetype;

    private String phoneos;

    private String phonemodel;

    private String vername;

    private String vercode;

    private String message;

    private String stacktrace;

    private String remark;

}
