package com.ruoyi.system.domain.app;

import lombok.Data;

@Data
public class SpecJson {

    private Double specNum;

    private String specName;

}
