package com.ruoyi.system.domain.app;

import lombok.Data;

import java.util.List;

@Data
public class Material {

    private List<MaterialItem> video;

    private List<MaterialItem> image;

}
