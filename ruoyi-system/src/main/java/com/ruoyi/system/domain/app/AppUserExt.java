package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_users_ext")
public class AppUserExt {

    @TableId(type = IdType.ASSIGN_UUID)
    private String uid;

    @TableField(value = "ios_ver_code")
    private Integer iosVerCode;

    @TableField(value = "ios_ver_name")
    private String iosVerName;

    @TableField(value = "apk_ver_code")
    private Integer apkVerCode;

    @TableField(value = "apk_ver_name")
    private String apkVerName;

    @TableField(value = "ios_jpush_regid")
    private String iosJpushRegid;

    @TableField(value = "apk_jpush_regid")
    private String apkJpushRegid;

    @TableField(value = "survey_button_status")
    private Integer surveyButtonStatus;

    private Integer dummy;

    @TableField(value = "forum_post_visible_scope")
    private Integer forumPostVisibleScope;

    @TableField(value = "forum_comment_visible_scope")
    private Integer forumCommentVisibleScope;

    @TableField(value = "forum_author_rating")
    private Double forumAuthorRating;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

}
