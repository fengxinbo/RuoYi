package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_forum_post_report")
public class ForumPostReport extends Model<ForumPostReport> {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    @TableField(value = "biz_type")
    private Integer bizType;

    @TableField(value = "biz_id")
    private String bizId;

    @TableField(exist = false)
    private String content;

    private String uid;

    private String reason;

    private Integer status;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

}