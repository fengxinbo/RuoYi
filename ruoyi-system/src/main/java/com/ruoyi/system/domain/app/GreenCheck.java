package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName(value = "t_green_check", autoResultMap = true)
public class GreenCheck {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String uid;

    @TableField(value = "biz_id")
    private String bizId;

    @TableField(value = "biz_scenario")
    private String bizScenario;

    @TableField(value = "file_type")
    private Integer fileType;

    @TableField(value = "oss_url")
    private String ossUrl;

    @TableField(value = "req_id")
    private String reqId;

    @TableField(value = "task_id")
    private String taskId;

    private Integer checked;

    private LocalDateTime due;

    private String result;

    @TableField(value = "risk_level")
    private Integer riskLevel;

    private Integer processed;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

}
