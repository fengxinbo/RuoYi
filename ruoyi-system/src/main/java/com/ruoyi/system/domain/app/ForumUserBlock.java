package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "t_forum_user_block")
public class ForumUserBlock {

    @TableField(value = "blocker_id")
    private String blockerId;

    @TableField(value = "blocked_id")
    private String blockedId;

    private String ctime;

}