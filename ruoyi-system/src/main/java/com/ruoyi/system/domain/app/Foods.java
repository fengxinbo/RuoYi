package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@TableName("t_foods")
public class Foods extends Model<Foods> {

    @TableId(type = IdType.AUTO)
    private Integer fid;

    @Excel(name = "食物名称", cellType = Excel.ColumnType.STRING, type = Excel.Type.IMPORT)
    private String fname;

    @Excel(name = "别名", cellType = Excel.ColumnType.STRING, type = Excel.Type.IMPORT)
    private String alias;

    private Integer ftype;

    private String uid;

    @Excel(name = "蛋白质", cellType = Excel.ColumnType.NUMERIC, type = Excel.Type.IMPORT)
    private Double protein;

    @Excel(name = "碳水化物", cellType = Excel.ColumnType.NUMERIC, type = Excel.Type.IMPORT)
    private Double carbohydrate;

    @Excel(name = "脂肪", cellType = Excel.ColumnType.NUMERIC, type = Excel.Type.IMPORT)
    private Double fat;

    private Double fibre;

    @Excel(name = "热量", cellType = Excel.ColumnType.NUMERIC, type = Excel.Type.IMPORT)
    private Integer calories;

    @Excel(name = "验证", cellType = Excel.ColumnType.NUMERIC, type = Excel.Type.IMPORT, readConverterExp = "0=未验证,1=已验证")
    private Integer verified;

    @Excel(name = "规格", cellType = Excel.ColumnType.STRING, type = Excel.Type.IMPORT)
    private String spec;

    private String createdby;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    private String updatedby;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

    public Integer getFid() {
        return fid;
    }

    public void setFid(Integer fid) {
        this.fid = fid;
    }

    @NotBlank(message = "食物名称不能为空")
    @Size(max = 64, message = "食物名称长度不能超过64个字符")
    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getAlias() {
        return alias == null ? alias : alias.trim();
    }

    public void setAlias(String alias) {
        this.alias = alias == null ? alias : alias.trim();
    }

    public Integer getFtype() {
        return ftype;
    }

    public void setFtype(Integer ftype) {
        this.ftype = ftype;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Double getProtein() {
        return protein;
    }

    public void setProtein(Double protein) {
        this.protein = protein;
    }

    public Double getCarbohydrate() {
        return carbohydrate;
    }

    public void setCarbohydrate(Double carbohydrate) {
        this.carbohydrate = carbohydrate;
    }

    public Double getFat() {
        return fat;
    }

    public void setFat(Double fat) {
        this.fat = fat;
    }

    public Double getFibre() {
        return fibre;
    }

    public void setFibre(Double fibre) {
        this.fibre = fibre;
    }

    public Integer getCalories() {
        return calories;
    }

    public void setCalories(Integer calories) {
        this.calories = calories;
    }

    public Integer getVerified() {
        return verified;
    }

    public void setVerified(Integer verified) {
        this.verified = verified;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public LocalDateTime getCtime() {
        return ctime;
    }

    public void setCtime(LocalDateTime ctime) {
        this.ctime = ctime;
    }

    public String getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public LocalDateTime getEtime() {
        return etime;
    }

    public void setEtime(LocalDateTime etime) {
        this.etime = etime;
    }
}
