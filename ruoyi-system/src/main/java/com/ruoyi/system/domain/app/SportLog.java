package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@TableName("t_sport_log")
public class SportLog {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String uid;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate sdate;

    @TableField(value = "sport_item_id")
    private String sportItemId;

    @TableField(value = "sport_item_name")
    private String sportItemName;

    private Double weight;

    private Double duration;

    private Double calories;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

    @TableField(exist = false)
    private Double met;

    @TableField(exist = false)
    private Double durationUser;

    @TableField(exist = false)
    private Double caloriesUser;

}