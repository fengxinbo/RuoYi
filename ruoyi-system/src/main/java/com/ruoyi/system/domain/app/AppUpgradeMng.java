package com.ruoyi.system.domain.app;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_app_upgrade_mng")
public class AppUpgradeMng {

    @TableId(value = "phone_type", type = IdType.INPUT)
    private Integer phoneType;

    @TableField("latest_ver_name")
    private String latestVerName;

    @TableField("latest_ver_code")
    private Long latestVerCode;

    @TableField("release_log")
    private String releaseLog;

    @TableField("force_upgrade_min_ver_code")
    private Long forceUpgradeMinVerCode;

    @TableField("need_pop_min_ver_code")
    private Long needPopMinVerCode;

    @TableField("ios_app_build_ver_code")
    private Long iosAppBuildVerCode;

    @TableField("created_by")
    private String createdBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime ctime;

    @TableField("updated_by")
    private String updatedBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime etime;

}
