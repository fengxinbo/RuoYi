package com.ruoyi.system.domain.app;

import lombok.Data;

@Data
public class ForumPostCoverInfo {

    private String ossUrl;

    private Integer width;

    private Integer height;

}
