package com.ruoyi.common.constant;

public class AppConstants {

    public static final Integer APP_FOODS_NUTRITION_VERIFIED = 1;

    public static final Integer APP_USER_STATE_DISABLED = 0;

    public static final Integer APP_USER_ACCOUNT_DELETED = 2;

    public static final Integer APP_FORUM_POST_CS_PROCESSED = 1;

    public static final Integer APP_FORUM_POST_CS_CHECKED = 1;

    public static final Integer APP_FORUM_POST_CS_MACHINE_AUDIT_PASS = 1;

    public static final Integer APP_FORUM_POST_CS_AUDIT_PASS = 1;

    public static final Integer APP_FORUM_POSTER_BACKEND = 1;

    public static final Integer APP_FORUM_POST_REPORT_STATUS_PROCESSED = 1;

    public static final String POST_IMPRESSION_THRESHOLD_PREFIX = "ela:forum:post:impression:threshold:";

}
