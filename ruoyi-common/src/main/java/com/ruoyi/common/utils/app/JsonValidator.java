package com.ruoyi.common.utils.app;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;

public class JsonValidator {

    public static boolean isValidJsonString(String jsonString) {
        if (jsonString == null) {
            return false;
        }
        try {
            JSON.parse(jsonString);
            return true;
        } catch (JSONException ex) {
            return false;
        }
    }

}
