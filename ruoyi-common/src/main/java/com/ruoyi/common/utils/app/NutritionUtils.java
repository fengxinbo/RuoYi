package com.ruoyi.common.utils.app;

import java.math.BigDecimal;

public class NutritionUtils {

    /**
     * 根据营养元素(蛋白质，碳水化物和脂肪)获取卡路里
     *
     * @param protein
     * @param carbohydrate
     * @param fat
     * @return
     */
    public static Integer getCalories(Double protein, Double carbohydrate, Double fat) {
        protein = protein == null ? 0 : protein;
        carbohydrate = carbohydrate == null ? 0 : carbohydrate;
        fat = fat == null ? 0 : fat;
        return new BigDecimal(((protein + carbohydrate) * 4) + (fat * 9)).intValue();
    }

}
